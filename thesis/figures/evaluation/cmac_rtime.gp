set terminal postscript size 7,4
set output 'cmac_rtime.eps'

#set key horiz invert reverse center bottom outside
set yrange [0:80]
set xrange [37.5:225]
set ylabel "Radio on-time (us)"
set xlabel "IEEE 802.11g interference (KB/s)"
set grid y

plot 'cmac_rtime.dat' using 1:2 w lines t "ContikiMAC P-DCCA" lw 3 lc rgb "red" , \
'' using 1:3 w lines t "ContikiMAC Standard" lw 3 lc rgb "blue" lt 1

