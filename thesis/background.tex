\chapter{WSN Interference and Coexistence}
\label{chap:background}

In this section, the foundations of this thesis are presented.
The history, evolving demands, and typical hardware of \ac{WSN} are discussed in Section \ref{sec:wsn.background}.
\ac{WSN} \ac{MAC} protocols are discussed in Section \ref{sec:mac_protocols.background}.
ContikiMAC, which is used as an example \ac{MAC} protocol in Chapters \ref{chap:estimating} and \ref{chap:dcca}, is described in Section \ref{sec:contikimac}.
The effects of interference on \ac{WSN}, and sources of interference, are discussed in sections
\ref{sec:background.int_effects} and \ref{sec:interference.background} respectively.
The chapter is summarised in Section \ref{sec:summary.background}.

\section{Wireless Sensor Networks}
\label{sec:wsn.background}
\input{wsn.background}

\subsection{WSN Hardware}

The work in this thesis was based around the Moteiv tmote sky \cite{tmote}, which is now described.
The processing, interface, GPIO and radio hardware of the tmote sky are comparable to many other \ac{WSN} hardware examples.
The tmote sky is based around an 8$Mhz$ Texas Instruments MSP430 F1611 microcontroller \cite{msp430}, with 10KB of RAM, and 48KB of flash storage.
The board, measuring 3.2$cm$ by 1.3$cm$, includes an IEEE 802.15.4 2.4$Ghz$ Physical layer-compliant TI CC2420 \cite{cc2420spec} radio transceiver, with integrated \ac{PCB} antenna.
The board also includes various integrated sensors and GPIO pins connecting to the MCU; programming is done over the USB connection by means of a pre-installed bootloader.
The board also includes an FTDI UART-to-USB interface,
which can be used for serial connection to a host computer.

The \ac{MCU} communicates with the CC2420 radio via SPI.
The CC2420 defines registers that configure and direct radio operation, 
such as the \ac{CCA} thresholds, output power, and initiating a transmission.
A single FIFO buffer is used for both sending and receiving packets, with a built-in \ac{CRC} generator.

\subsection{IEEE 802.15.4 (\acs{LR-WPAN})}
\label{sec:lrwpan}
\input{lrwpan.background}

\section{MAC Protocols}
\label{sec:mac_protocols.background}
\input{mac_protocols.background}

\subsection{ContikiMAC}
\label{sec:contikimac}
\input{contikimac.background}

\subsection{Effect of interference}
\label{sec:background.int_effects}.

% interference intro.

The 2.4$Ghz$ ISM band is shared with a plethora of other wireless technologies.
Among which, transmission powers, modulations, bandwidth, and channel arbitration policies are far from homogeneous.
Consequently in environments shared with other devices, link performance in these networks may be degraded.
This issue is referred to as \acf{CTI}, and is an important consideration in the design of any wireless protocol, and prior to any deployment.

Compared to other communication protocols, and standard  802.15.4 devices, the effects of \ac{CTI} are more exaggerated in \ac{WSN} \ac{MAC} protocols, such as ContikiMAC.
This is due to the infrequent nature of rendezvous opportunities, and the need to achieve low power operation.
The effects of \ac{CTI} are twofold on \ac{WSN}: packet loss, and energy inefficiency.

\textbf{Packet Loss}

Packet loss is caused by collisions - simultaneous use of the channel - with other interference;
this degrades network reliability and throughput.
Packet loss may result from two types of interaction: packet-collision, and \ac{CCA}-collision.
Packet-collisions occur when another interference signal coincides with a \ac{WSN} transmission.
The reduced signal quality at the receiving node prevents the packet contents from being received correctly.
This is the most common cause of packet loss in typical communication protocols.

\ac{CCA}-collisions occur in communication protocols that implement a listen-before-send policy - 
including \ac{LR-WPAN}, \ac{WSN} \ac{MAC} protocols, and IEEE 802.11.
Here, nodes check the channel is free before transmitting using a \ac{CCA} check.
If the channel is deemed busy, due to an interference signal on the same frequency - 
the transmission is aborted.
Depending on the retransmission policy, another attempt may be rescheduled.

The occurrence and effect of \ac{CCA}-collisions are amplified in contention-based \ac{WSN} \ac{MAC} protocols.
Unlike other  802.15.4 protocols, the transmission procedure in these protocols requires a lengthy synchronisation phase
with multiple \ac{CCA} checks - any one of which may cause a \ac{CCA} collision.
For example, in the ContikiMAC send sequence, six \ac{CCA} checks are required before transmitting,
followed by further \ac{CCA} checks between each packet strobe.
This is shown in figure \ref{fig:background_contikimac_w_int}.
By contrast, the standard  802.15.4 \ac{MAC} requires only a single \ac{CCA} check in the \ac{CAP}, and no \ac{CCA} checks at all in the \ac{CFP}.
Therefore, as studied in previous literature (\cite{boano2010making} - discussed in Section \ref{sec:related_studies}),
packet loss due to interference is more prevalent in \ac{WSN}-specific \ac{MAC} protocols.

\textbf{Energy inefficiency}

Energy efficiency is an essential feature of \ac{WSN} \ac{MAC} protocols, in order to provide long network lifetime on minimal resources.
Under interference conditions, energy consumption of \ac{WSN} \ac{MAC} protocols is increased.
Therefore, network lifetime may be reduced compared to an idealistic environment.

Energy inefficiency may stem from two sources of energy use: transmitting/receiving, and idle listening.
In the first instance, mechanisms to counter packet loss, such as retransmissions, error correction coding, and routing changes,
consume more energy than in an interference-free environment.
This may be necessary to achieve quality of service requirements in a deployment.

Secondly, the energy consumption of idle listening - when not receiving or transmitting data - is also influenced by interference.
This is due to the listening mechanism during periodic wakeup checks, which use \ac{CCA} to detect if another node may be transmitting.
If this \ac{CCA} detects other interference, a \textit{false-wakeup} results, where the node enters the wakeup procedure, increasing energy consumption \cite{zheng2014zisense}.
For example, this is shown for ContikiMAC in figure \ref{fig:background_contikimac_w_int}.
In typical \ac{WSN} deployments, which have low data rate requirements, idle listening is the predominant source of energy consumption.
Therefore, in interference environments, false wakeups are likely to be the greatest source of energy inefficiency. 

\begin{figure}
 \centering
 \includegraphics[width=.7\textwidth]{figures/background/contikimac_w_int.eps}
 \caption{ContikiMAC under WiFi interference:
          1) Node detects interference during wakeup \ac{CCA}, initiating \textit{false wakeup}. 
          2) \ac{CCA} check during packet strobing halts transmission prematurely.}
 \label{fig:background_contikimac_w_int}
\end{figure}

\section{Interference Sources}
\label{sec:interference.background}

In this section, interference sources that are common in the 2.4$Ghz$ frequency domain are discussed.
This includes \textit{communicable} devices, such as WiFi and Bluetooth, which define a standardised channel access
protocol.
Also, Microwave oven interference is discussed as an example of \textit{non-communicable} interference source.
In both cases,  802.15.4 networks are known to suffer in coexisting deployments.

\subsection{IEEE 802.15.1 (Bluetooth)}
\input{bluetooth.background}

\subsection{IEEE 802.11 (WiFi)}
\input{wifi.background}

\subsection{\acf{MWO} interference}

\acfp{MWO} use a magnetron to emit microwave radiation.
Despite being shielded by a faraday cage, leaked energy is still emitted across the 2.4$Ghz$ frequency range.
This is more prominent in older devices.
The interference generated is observed as a 50\% duty cycle pulse train, 
whose on/off period is determined by the AC mains frequency.
For example, in countries that use 50$Hz$ AC mains frequency, the 
on-duration of microwave interference is
approximately 10$ms$, followed by 10$ms$ off.

Some previous works have observed unique features of \ac{MWO} interference
within  802.15.4 RSSI traces \cite{zheng2014zisense,boano2011jamlab}.
\ac{MWO} interference has been shown to cause RSSI fluctuations below the noise floor 
during the on-period, which is not exhibited by other interference sources.
This is attributed to the saturation of the intermediate
frequency amplifier chain in the CC2420 radio.

Unlike other interference, \ac{MWO} interference is not a by-product of wireless communication.
Likewise, \acp{MWO} have no channel access or arbitration policy, 
compared to  802.11 for example.
Interference is generated regardless of the prior channel state.
This must be accounted for in the design of any interference mitigation approach.

\section{Chapter Summary}
\label{sec:summary.background}

In this chapter, the foundations of \ac{WSN}, \ac{MAC} protocols, and interference have been discussed.
\ac{WSN} \ac{MAC} protocols were reviewed in Section \ref{sec:mac_protocols.background} where \ac{CTI} has been shown to affect packet loss and energy inefficiency.
Typical interference sources in the 2.4$Ghz$ frequency domain - common amongst \ac{WSN} devices - were discussed in Section \ref{sec:interference.background}.
In the next chapter, previous literature on \ac{CTI}, including mitigation solutions, with respect to  802.15.4 and \ac{WSN} \ac{MAC} protocols specifically, is reviewed.

