set terminal postscript size 7,5
set output 'multi_nodes.eps'

#set key horiz center bottom outside
set key title "Num. TX." box
set yrange [20:400]
set ylabel "Average Throughput per node\n(Bytes/Sec)"
set xlabel "ContikiMAC-variation"
unset xlabel
set grid y
set border 3
set style data histograms
set style histogram cluster gap 1
set style fill solid 0.25 border -1
set boxwidth 0.75
set xtics nomirror rotate by -45

plot 'multi_nodes.dat' using 2:xticlabel(1) t "1" lc rgb "red" lt 1, \
'' using 3 t "2" lc rgb "green" lt 1, \
'' using 4 t "4" lc rgb "blue" lt 1, \
'' using 5 t "8" lc rgb "pink" lt 1


