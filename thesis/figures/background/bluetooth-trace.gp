set term postscript
set output 'bluetooth-trace.eps'

set xlabel 'Time (ms)'
set ylabel 'RSSI (dBm)'

unset key
plot 'bluetooth-trace.dat' using ($0*.0325):1 w l lc rgb 'black' lt 1

