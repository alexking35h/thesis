set term epscairo size 5,3

set xlabel "Channel Busy Probability p"

set ylabel "Duty Cycle D(p) in [%]"
set yrange [0:4]
set output "exp3.eps"

#plot "exp3.txt" using ($1/100):(((16*$3)/10000)/108) title '240ppm - Measured' with lines, \
#	"exp3.txt" using ($1/100):(((16*$4)/10000)/108) title '30ppm - Measured' with lines, \
#	"exp3.txt" using ($1/100):(((16*$5)/10000)/108) title '10ppm - Measured' with lines, \
#	"exp3.txt" using ($1/100):(((16*$6)/10000)/108) title '1ppm - Measured' with lines, \
#	"exp3.txt" using ($1/100):(((16*$7)/10000)/108) title '0ppm - Monte Carlo' with lines

plot "exp3.txt" using 1:($3*100) title '240ppm - Measured' with lines, \
	"exp3.txt" using 1:($4*100) title '30ppm - Measured' with lines, \
	"exp3.txt" using 1:($5*100) title '10ppm - Measured' with lines, \
	"exp3.txt" using 1:($6*100) title '1ppm - Measured' with lines, \
	"exp3.txt" using 1:($7*100) title '0ppm - Monte Carlo' with lines



