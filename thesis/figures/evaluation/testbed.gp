set terminal postscript size 7,4
set output 'testbed.eps'

#set key horiz invert reverse center bottom outside
set ylabel "Radio-on time (ms)"
set xlabel "IEEE 802.11g interference (KB/s)"
set grid y

plot 'testbed.dat' using 1:2 w l t "Standard" lw 3 lc rgb "blue", \
'' using 1:3 w l t "P-DCCA" lw 3 lc rgb "red" lt 1

