set term epscairo size 5,3.5 font ",15"
set output 'energy.eps'

set ylabel "Idle listening time per channel check ({/Symbol m}s)
set xlabel "Channel Busy Duration ({/Symbol m}s)"

set key at 1400., 900.

set grid

busy=.25

N_R = 8
T_st = 128
T_RSSI = 32
f = 8

T_A(t) = ( T_RSSI * (N_R-1) ) * ( t - ( (N_R-1) * T_RSSI ) ) / t
T_B(t) = ( T_RSSI / t ) * ( T_RSSI ) * ( (N_R-1)*(N_R-2) / 2)
T(t,p) = T_st + T_RSSI + ( p * ( T_A( t ) + T_B( t ) ) )
T_ALL(t,p) = f * T(t,p)

# The model is only valid if t (which is the duration of the interference)
# is less than the total sampling duration - which is N_R * T_RSSI.
set xrange[ (N_R * T_RSSI) :1500]
set yrange[0:3000]

plot 2900 w l title "ZiSense" ls 1 lc rgb "black" lw 3 dt 1,\
T_ALL(x, 0.1) w l title "P=0.1" ls 1 lc rgb "black" lw 3 dt 2,\
T_ALL(x, 0.25) w l title "P=0.25" ls 1 lc rgb "black" lw 3 dt 3,\
T_ALL(x, 0.5) w l title "P=0.5" ls 1 lc rgb "black" lw 3 dt 4,\
(f * T_st) w l title "Plain CCA" ls 1 lc rgb "black" lw 3 dt 5
