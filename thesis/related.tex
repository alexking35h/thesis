\chapter{Related Work}
\label{chap:related}

This chapter reviews previous literature related to wireless coexistence and interference, with respect to 802.15.4 and WSN communication.
Following an overview in Section \ref{sec:related_overview}, three research areas are explored.

In Section \ref{sec:related_studies}, experimental studies of wireless coexistence are discussed,
where the effects of interference are measured empirically.
Environmental interference is shown to negatively effect 802.15.4 and WSN links, motivating the remaining discussion in this chapter.

In Section \ref{sec:related_models}, theoretical models of coexistence are presented,
where link performance and energy efficiency are estimated from known environmental properties.
It is shown here that no model of energy consumption is currently available that accounts for the idle listening behaviour in interference environments.

In Section \ref{sec:related_solutions}, previous solutions to mitigate wireless interference are presented.
These are classed into avoidance, detection, and resilience approaches.
Exploring detection mechanisms, previous works are shown to either require specialist hardware - beyond the reach of off-the-shelf WSN hardware,
or increase idle listening - at the expense of energy efficiency.

Finally, the chapter is concluded in Section \ref{sec:related_summary}.

\section{Coexistence Overview}
\label{sec:related_overview}

Focusing on 802.15.4 networks, coexistence scenarios with 802.11 are the most commonly researched due to their application overlap.
The consensus here is that 802.15.4 links are more susceptible to the effects of 802.11 interference than vice versa.
The effects on 802.15.4 networks are twofold.
Firstly, collisions and packet loss increase as channel conditions deteriorate, subsequently worsening latency and QoS.
Secondly, the mechanisms to mitigate collisions and packet loss - discussed in Section \ref{sec:related_solutions} - have additional energy demands, which reduce energy efficiency.

The majority of work in this domain has focused on the standard 802.15.4 PHY and MAC layers, or industry standards such as ZigBee.
For the specific case of 802.15.4-based WSN MAC protocols however, the effects of CTI on packet loss and energy inefficiency are amplified.
Packet loss is compounded by the awkward nature of unicast communication in duty-cycled WSN MAC protocols;
latency is further affected due to the infrequent nature of synchronisation and opportunities for retransmission.
 % As well as sensor readings, communication of network maintenance (e.g., routing advertisements, neighbor discovery) and
 % application control packets (e.g., sensor queries, updates) may not be possible, therefore preventing reliable WSN operation.
Low power listening MAC protocols, such as LPL \cite{moss2007low} and ContikiMAC \cite{dunkels2011contikimac}, are susceptible to false wakeups caused by interference - where CTI is mistaken for WSN channel activity.
In the presence of external interference, false wakeups become more frequent, increasing idle listening and reducing energy efficiency.
 % Many WSN applications that require minimum energy consumption, in order to prolong network lifetime, may therefore not be possible in
 % interference conditions.

Yang et al present a thorough survey of 802.11 and 802.15.4 coexistence work in \cite{yang2011wireless}.
The authors begin with measurement-based studies, which evaluate experimentally the effect of distance,
transmission rate, 802.11 variant, and data direction variables on 802.15.4 communication.
These typically take place in controlled environments to ensure reliable results.
Following this, theoretical studies are surveyed.
These are organised based on input, output and behaviour components, which allows for easy comparison between models, and to identify how one model may feed into another.
For example, distance and transmission power may be used as input to a path loss function, whose output is received power;
this may subsequently be fed into a Bit Error Rate (BER) function to estimate packet loss.
Finally, the authors review existing solutions to interference, classed as either inherent - performed permanently during operation,
or on-demand - dynamically employed in response to detecting interference.

The authors review only coexistence amongst standards-based communication, such as ZigBee \cite{alliance2009ieee}, Wireless Hart \cite{wirelesshart}, and ISA100 \cite{isa100}, but do not include less common WSN MAC protocols.
Also, Yang et al only review 802.11/802.15.4 coexistence, and do not consider interference from other sources such as microwave ovens and bluetooth devices.
Interference detection mechanisms are summarised to have appeared in literature throughout the network stack, from the PHY through to the MAC layer.
These are used to drive on-demand, reactive solutions.
The authors find that these detection mechanisms, and the response to their outcome, require further research.
%The same conclusion is reached in this thesis, on the basis that more accurate detection and classification of interference may allow for a more suitable response.

Hayanajneh et al present a survey of \ac{BAN} coexistence issues in \cite{hayajneh2014survey}.
\acp{BAN} are a subset of WSN, sharing the same energy efficiency requirements, radio hardware, and communication protocol design,
and therefore this survey is relevant to the wider issue of WSN coexistence.
The authors consider two types of interference: mutual, originating from inside the same network; and cross-interference, from other sources.
In this thesis, only cross-interference is considered.
Three wireless standards are discussed as candidates for \ac{BAN} communication: 802.15.4, 802.15.6, and Low-power WiFi.
As with \cite{yang2011wireless}, only interference with 802.11 is reviewed, and WSN-specific MAC protocols are not included.
The survey considers theoretical, simulation, and testbed studies, leading the authors to make some important observations.
Firstly, the coexistence parameters that are evaluated most often vary to suit the type of study.
Secondly, most coexistence studies are carried out experimentally, using testbeds with a small number of nodes per network.

%%% White paper (Schneider electric)

In response to concerns governing WiFi/ZigBee coexistence, Thonet et al \cite{thonet2008zigbee}
present a survey of studies into 802.15.4/802.11 coexistence, and measure the impact of coexistence of both networks experimentally.
This work only reviewed industrial studies, and did not include any academic sources.
The findings here that, except under high traffic loads, WiFi does not impede 802.15.4 networks, is contrary to the consensus among work in this field.
Further, the ZigBee standard, designed for Home Automation purposes, is dissimilar to WSN usage, wherein energy efficiency requirements and traffic patterns differ.

\section{Experimental studies}
\label{sec:related_studies}
\input{experimental.related}

\section{Theoretical Models}
\label{sec:related_models}
\input{models.related}

\section{Solutions}
\label{sec:related_solutions}
\input{cti-intro.related}

\subsection{Frequency Avoidance}
\input{avoidance.related}

\subsection{Resilience}
\label{sec:resilience.related}
\input{resilience.related}

\subsection{Detection}
\input{csma.related}

\subsubsection{Interferer-side}
\input{csma.wifi.related}

\subsubsection{WSN-side}
\input{csma.zigbee.related}

\subsection{Section Summary}
\input{cti-summary.related}

\section{Chapter Summary}
\label{sec:related_summary}

This chapter has examined existing work related to wireless coexistence, in 802.15.4 and wireless sensor networks.
Numerous measurement studies have shown that 802.11 interference is detrimental to 802.15.4 links, resulting in increased packet loss and latency.

Following this, theoretical models of link quality and energy consumption with respect to interference were discussed.
It was shown that there currently exists no theoretical model of energy consumption, 
that accurately accounts for idle listening in interference environments.

Existing solutions to interference were classed into avoidance, detection, and resilience approaches.
Among detection approaches, it was shown that no WSN-based approaches current exist that are sufficiently accurate; do not rely on expensive hardware; do not increase idle listening.

% Grrr, I still have this!
% \subsection{Interference Classification}
% \input{classification.related}

 % \section{Energy Consumption}
 % \label{sec:energy.related}
 % \input{energy.related}
 % 
 % \section{Packet Loss}
 % \label{sec:packetloss.related}
 % \input{study-interference.related}
 % 
 % 
 % \section{Chapter Summary}

~
~
~
~
