set terminal postscript size 7,4
set output '24hr_eng.eps'

set key horiz right top
#set key horiz invert reverse center bottom outside
set yrange [9:17]
#set xrange [37.5:225]
set timefmt "%H:%M:%S"
set xdata time

set format x "%H:%M"
#set xtics rotate by 90 out 
set xtics nomirror rotate by -60


set ylabel "Radio On Time (ms)"
set xlabel "Time"
set grid y

plot '24hr_eng.dat' using 1:2 w lines t "P-DCCA" lw 3 lc rgb "red" , \
'' using 1:3 w lines t "Standard" lw 3 lc rgb "blue" lt 1

