\chapter{Estimating Node Lifetime}
\label{chap:estimating}

Predictable energy consumption is an invaluable component of WSN design.
As discussed in Chapter \ref{chap:background}, the receiving function of WSN MAC protocols is based on the 802.15.4 CCA interface, which detects incoming packets without costly-idle listening.
However, the most common CCA implementation, energy-detection, is susceptible to other interference;
energy consumption is therefore dependent on the environmental interference.

Consequently, WSN designers would benefit from a tool to estimate energy consumption of a WSN, based on interference measurements in an environment.
This may be used to assess the feasibility of a deployment; fine tune MAC protocol parameters to meet network lifetime requirements; and compare MAC protocol performance.

In Section \ref{sec:estimating__quantify} methods of measuring interference are discussed.
Then in Sections \ref{sec:estimating__model} and \ref{sec:estimating__solver},
two methods of predicting energy consumption are described, and shown by example for a well known MAC protocol.
Prediction accuracy is then evaluated in Section \ref{sec:estimating__evaluation}.
Finally, the chapter is summarised in Section \ref{sec:estimating__summary}.

\section{Estimating Energy Consumption in WSN}

When designing or installing a WSN, node energy consumption is a key design parameter.
On battery powered devices, node energy consumption dictates network lifetime, and so WSN designers can tailor energy consumption to meet lifetime goals.
A WSN deployment to monitor volcano emissions, for example, may require that readings are recorded for at least three months.
Alternatively, on WSN hardware powered by energy harvesting devices, node energy use must fall within the set budget for the network to operate.
For example, a sensor node powered by a solar cell must not exceed this energy budget, in order for the node to function.

On most WSN hardware, the radio is the greatest source of energy consumption, even when not transmitting or receiving.
Thus, node energy consumption and lifetime is determined more by the radio than any other component.
However, as reviewed in Chapter \ref{chap:related}, radio energy use has been shown in previous works to be influenced by environmental interference.

This is the case in asynchronous low power MAC protocols that infer from channel energy if a packet is being sent.
In protocols such as ContikiMAC, and Tiny OS LPL, Clear Channel Assessments (CCA) are used to sample the channel energy and determine if a packet is being transmitted.
This achieves very low energy usage, as the radio only briefly needs to be listening to the channel to determine if a further wakeup is needed.
Most transceivers provide CCA based on energy detection, whereby the channel energy is sampled, and compared to a predefined threshold.
This can detect 802.15.4 activity, and also any other activity in the 2.4Ghz domain, including other interference devices.
In the context of a WSN MAC protocol wakeup sequence, this is liable to \textit{false-wakeups}:
where a WSN node infers channel activity from other interference.
This increases idle listening and reduces energy efficiency and node lifetime.

Therefore, accurate predictions of WSN energy consumption, for these protocols, cannot be made without factoring the interference in the deployment.
Tools and methods to achieve this can be used to:

\begin{enumerate}
 \item \textbf{Tune MAC parameters} Wakeup frequency, CCA check behaviour, CCA threshold, and energy saving optimisations can be tuned to meet node energy use requirements for a given environment.
 \item \textbf{Assess deployment feasibility} Given known hardware, network, application, and lifetime requirements, the feasibility of a WSN deployment operating in an environment may be determined in advance.
 \item \textbf{Comparing MAC protocols} Different MAC protocols may be compared on an energy efficiency basis, to select the most suitable for a deployment.
\end{enumerate}

\section{Measuring Interference}
\label{sec:estimating__quantify}

In order to estimate the energy consumption of a sensor node in a given environment, a quantification of the interference must be provided.
Methods of sampling and analysing interference have received attention in WSN literature.
Among other applications, this has been used to identify interference sources, and to inform channel selection algorithms.

A prominent approach has been to record the RSSI register value repeatedly into an in-memory buffer, providing a time domain trace of channel energy.
This provides a detailed representation of any interference signals present, from which a wealth of information can be drawn.
For each signal recorded, modulation characteristics, signal strength, and timing features may be calculated from such a trace.
A high sampling frequency here is desirable to capture an RSSI trace Representative of channel activity.

This method however suffers a number of drawbacks.
Firstly, the memory capacity on typical WSN hardware is less than 10$KB$, which limits the maximum sample duration.
While compression techniques can mitigate this restriction, long uninterrupted sample durations are not possible.
Likewise in applications which require this feature alongside other WSN functions, such a substantial memory allocation is not possible.
Although this may not be an issue in a WSN toolset which has only this specific requirement.
This approach is useful in applications which are required to analyse each detected signal or packet, such as interference source classification.

Statistical methods are able to compress the recorded RSSI before it is saved into memory.
Some signal features are inevitably lost in this approach, although this is acceptable in applications which do not analyse each detected signal or packet - as in the case presented here.
The distribution of idle/busy period frequencies in an RSSI trace has been adopted in previous literature.
Here, on detection of each idle/busy period, the corresponding bin counter is incremented.
In this approach, long sampling durations can be afforded at the expense of additional processing in each sampling loop iteration.
The granularity of this approach can be tailored by altering the bin size.
Packet delivery estimations, which are based on the probability of the channel remaining clear for a given packet length, have adopted this approach in previous work.

In this chapter, the channel busy probability, donated $P_C$, was used to measure interference.
This is the probability of the channel being occupied by other interference at any instant, and is calculated as in equation \ref{eq:pc}.

\begin{equation}
\label{eq:pc}
P_C = \frac{\sum_{n=1}^{N}{C_n}}{N}
\end{equation}

Where $C_n$ is the state of the channel as busy (1) or clear (0) as measured on the $n$th sampling iteration, and $N$ is the total number of samples.
A quieter interference environment will be close to zero, while a busy channel will be closer to one.
Depending on the interference source, requirements on the PHY and MAC layer within the standard may restrict this.
For example, under 802.11g interference, $P_C$ cannot exceed 55\%, even under multiple stations transmitting at once.

This approach is sufficient for predicting the behaviour of WSN MAC protocol CCA - which are spaced out sufficiently to be considered temporally independent.
This is the case with, for example, ContikiMAC.
The memory and processing overhead of this approach is the smallest of these approaches, and can easily be incorporated alongside other WSN applications.
As with measuring idle/busy distribution, only the channel state is required.
This can be measured via CCA, which is much faster than RSSI and therefore supports higher sampling frequencies.

$P_C$ is not expected to be static over time.
Rather, it should increase during busier periods as devices generate more 2.4Ghz interference (for example, during working hours in an office), and decrease during quieter hours.
However, the maximum recorded value of $P_C$ can be used to calculate the worst case estimate for the energy use.
Likewise, if the sample window for $P_C$ is large enough, the average value of $P_C$ can be used to estimate the average-case lifetime of the sensor node.


\section{Closed Form Solution}
\label{sec:estimating__model}

A closed form solution, $D(P_C)$, takes as input the channel busy probability and estimates the idle listening time for a given \ac{MAC} protocol.
$D$ expressly ignores traffic within the WSN, assumes that the largest expenditure in energy consumption 
is idle listening.
The idle listening time is a product of the expected radio on time, per wakeup sequence, $E$, 
and the channel check frequency $f$, as in equation \ref{eq:estimating__d(p)}.

\begin{equation}
\label{eq:estimating__d(p)}
D(p) = E(p) \cdot f
\end{equation}

$f$ is the rate that nodes check for incoming traffic, and is configured before the network is deployed;
higher $f$ allows for greater network throughput, but increases idle listening.
$E$ must be calculated for the MAC protocol, accounting for the wakeup procedure.
As an example, a derivation of $E(P_C)$ for the ContikiMAC duty cycling MAC protocol is now described.
The operation of ContikiMAC is similar to other MAC protocols that use CCA to detect incoming traffic, 
to which this approach should be applicable.

% \begin{figure}
%  \centering
%   \includegraphics[height=.9\textheight]{figures/estimating/contikimac_state_diagram.pdf}
%   \caption{State diagram for ContikiMAC channel check sequence}
%   \label{fig:estimating__contikimac_state_machine}
% \end{figure}

\tikzstyle{gdecision} = [diamond, draw, fill=gray!20, text width=5.5em, text badly centered, inner sep=0pt]
\tikzstyle{decision} = [diamond, draw, text width=5.5em, text badly centered, inner sep=0pt]
\tikzstyle{gblock} = [rectangle, draw, fill=gray!20, text width=5.5em, text centered, rounded corners, minimum height=2em]
\tikzstyle{block} = [rectangle, draw, text width=5.5em, text centered, rounded corners, minimum height=2em]
\tikzstyle{line} = [draw, -latex']
\tikzstyle{cloud} = [draw, ellipse, minimum height=2em]

\begin{figure}
\centering
\begin{tikzpicture}[node distance = 1cm, auto]
    % Place nodes
    \node [cloud] (init) {Wake};
    \node [gdecision, below  of=init, node distance=2cm] (cca1)  {CCA1};
    \node [block, below left of=cca1, xshift=-1em, node distance=2cm] (wait1) {Wait $T_w$};
    \node [gdecision, below of=wait1, node distance=2.5cm] (cca2)  {CCA2};
    \node [block, right  of=cca2,  node distance=3.5cm] (initmaxsil) {max=0;sil=0};
    \node [decision, below of=initmaxsil, node distance=2.5cm] (max)  {++max $>$ \\ $N_{max}$?};
    \node [cloud, left  of=max, node distance=3.5cm] (sleep) {Sleep};
    \node [decision, below of=sleep, node distance=3.5cm] (sil)  {++sil $>$ \\ $N_{sil}$?};
    \node [gdecision, below  of=max, node distance=3.5cm] (cca3)  {CCA3};
    \node [decision, below  of=cca3, node distance=3.5cm] (data)  {Data?};
    \node [cloud, left  of=data, xshift=-1em, node distance=5cm] (receive) {Receive};
    \node [block, below of=data,  node distance=2.5cm] (silzero) {sil=0};
    \node [gblock, below right of=silzero,  node distance=2cm] (wait2) {Wait $T_w$};
    % Draw edges
    \path [line] (init) -- (cca1);
    \path [line] (cca1) -| node [near start]  {idle} (wait1);
    \path [line] (wait1) -- (cca2);
    \path [line] (cca1) -| node [near start]  {busy} (initmaxsil);
    \path [line] (cca2) -- node [near start]  {busy} (initmaxsil);
    \path [line] (cca2) -- node [near start]  {idle} (sleep);
    \path [line] (initmaxsil) -- (max);
    \path [line] (max) -- node [near start]  {yes} (sleep);
    \path [line] (sil) -- node [near start]  {yes} (sleep);
    \path [line] (sil) |- node [near start]  {no} (wait2);
    \path [line] (cca3) -- node [near start]  {idle} (sil);
    \path [line] (max) -- node [near start]  {no} (cca3);
    \path [line] (cca3) -- node [near start]  {yes} (data);
    \path [line] (data) -- node [near start]  {no} (silzero);
     \path [line] (data) -- node [near start]  {yes} (receive);
    \path [line] (silzero) |-  (wait2);
    \path [line] (wait2) |- node [near start]  {yes} (max);
\end{tikzpicture}
\caption{State diagram for ContikiMAC channel check sequence. Elements contributing to the idle duty cycle are shaded gray.}
\label{fig:estimating__contikimac_state_machine}
\end{figure}


The ContikiMAC wakeup sequence - as per the current implementation - is shown as a flow chart in 
figure \ref{fig:estimating__contikimac_state_machine}.
It consists of two components: the listen and receive phases.
Firstly, in the listen phase, two CCA checks sample the channel to detect incoming traffic.
The CCA checks are separated sufficiently to ensure that at least one will coincide with 
any ongoing packet transmission.
If either indicate a busy channel, the node enters the receive phase.
Otherwise, the wakeup sequence terminates.

The receive phase is where the expectant packet is received, after which an acknowledgement packet is sent (if required) 
and the radio powered down.
While waiting for a packet, the CCA and incoming data flag are polled continuously every 620$\mu s$ to check the channel state.
If no packet is received, the receive phase times out after ten iterations of this loop, and the radio returns to sleep.
To mitigate the cost of idle listening caused by false wakeups, \textit{fast-sleep}, is described as an optimisation in ContikiMAC.
Here, if the channel is found clear for four uninterrupted CCA checks, a false-wakeup is assumed, and the radio is powered down.

\tP{For ContikiMAC, E(p) is the sum of three terms, which represent the three paths the MAC protocol can take in response to CCA check outcomes.
 \begin{itemize}
  \item $E_{ii}$: the case where both of the periodic CCA checks return false: no packet is detected. In this case, there is no false wakeup and the radio goes back to sleep.
  \item $E_{b}$: the case where the first periodic CCA check falsely indicates a packet. The radio enters the channel check sequence, whose duration is determined by up to six further CCA checks.
  \item $E_{bi}$: the case where the first periodic CCA check returns false, but the second returns true and falsely indicates a packet. The radio enters the channel check sequence, whose duration is determined by up to six further CCA checks.
 \end{itemize}
}

From this, the function $E(p)$ (see equation \ref{eq:estimating__e(p)}) is the sum of three terms,
which are derived from the three paths the wakeup sequence can take in response to each CCA outcome.
Each term is the product of the probability of its occurrence, and the resulting radio-on time in each case.

\begin{equation}
\label{eq:estimating__e(p)}
E(p)=E_{ii} + E_{b}  + E_{ib}
\end{equation}

These terms are collectively exhaustive, and encapsulate all code paths where no packet is received.
$E_{ii}$ represents the case where both CCA find a clear channel. No packet is detected, and there is no following false wakeup 
sequence. This is the optimal case, which achieves the lowest duty cycle.
The probability of this branch executing within the state machine is $P_{ii} = (1 - p)^2$.
The radio-on time in this case is the time required to execute two CCA checks (CCA1 and CCA2), with duration $T_1$ and $T_2$. $E_{ii}$ is thus given
by: 

\begin{equation}
\label{eq:estimating_Ei}
E_{ii}(p)= (1-p)^2 \cdot ((T_{1}+T_{2}))
\end{equation}

$E_{b}$ represents the case where ContikiMAC's first CCA returns busy and ContikiMAC enters the receive phase of the wakeup sequence, wherein the channel is periodically checked via a CCA (CCA3 with duration $T_{3}$).
In this procedure the node evaluates if a detected channel activity is part of an incoming transmission.
A maximum number of $N_{max}+1$ CCA's are carried out, with $N_{max}=10$ for a default ContikiMAC configuration.
The procedure may terminate before $N_{max}+1$ CCA's are carried out if $N_{sil}+1$ consecutive clear CCA's are encountered, with $N_{sil}=5$ for a default ContikiMAC configuration.
Between each CCA a delay of $T_{w}$ is included, which contributes to the radio on time as ContikiMAC keeps the radio active during the entire procedure.
The very first CCA in this procedure returns always busy as there is no time delay between this CCA and the busy CCA leading into this procedure.

The default ContikiMAC configuration sets $N_{MAX} = 10$, $N_{SIL} = 5$, and $T_W = 500\mu s$.
$T_1$ and $T_2$ were both measured as $294\mu s$, which includes radio-startup time. $T_3$ measured $122 \mu s$.
For these settings, seven possibilities exist for the procedure to terminate before the maximum number of 
$N_{max}+1=11$ CCA checks are carried out.
For example, after the first CCA in the procedure -- which always returns busy -- we could encounter a sequence of 6 idle CCA which leads to a termination of the procedure after 7 CCA checks.
The probability of this path is given by $p\cdot(1-p)^6$.
Considering all possible paths through the state machine we can give $E_{b}$ as: 

\begin{eqnarray}
E_{b}(p)& = & p \cdot (1-p)^{N_{sil}} \cdot \Big((N_{sil}\cdot(T_{3}+T_{w})+T_{1}) \nonumber \\
&& + \sum\limits_{m=1}^{(N_{max}-N_{sil}-2)}  \sum\limits_{n=1}^{m} \big[p^n \cdot (1-p)^{(m-n)}  \nonumber \\
&& \cdot \big((N_{sil}+m) \cdot (T_{3}+T_{w}) + T_{1}\big)\big]\Big) \nonumber \\
&& + p \cdot \Big(1 - \big((1-p)^{N_{sil}}  \nonumber \\
&& + \sum\limits_{m=1}^{(N_{max}-N_{sil}-2)}  \sum\limits_{n=1}^{m} \big[p^n \cdot (1-p)^{(m-n)}\big]\big)\Big) \nonumber \\
&&  \cdot  \big((N_{max}-1) \cdot (T_{3}+T_{w}) + T_{1}\big)
\end{eqnarray}

$E_{ib}$ represents the case where ContikiMAC's first CCA (CCA1) returns clear but the second CCA (CCA2) returns busy which then leads to the execution of the same procedure as described for $E_{b}$.
The difference here is the resulting duration of the radio on time as two CCA are executed before entering the procedure of repeated follow-up CCA checks.
$E_{ib}$  can be given as:

\begin{eqnarray}
E_{ib}(p)& = & p \cdot (1-p)^{N_{sil}+1} \cdot \Big((N_{sil}\cdot(T_{3}+T_{w})+ \nonumber \\
&& T_{1} +T_{2})  \nonumber \\
&& + \sum\limits_{m=1}^{(N_{max}-N_{sil}-2)}  \sum\limits_{n=1}^{m} \big[p^n \cdot (1-p)^{(m-n)}  \nonumber \\
&& \cdot \big((N_{sil}+m) \cdot (T_{3}+T_{w})  + T_{1} + T_{2}\big)\big]\Big)  \nonumber \\
&& + p \cdot \Big(1 - \big((1-p)^{(N_{sil}+1)}  \nonumber \\
&& + \sum\limits_{m=1}^{(N_{max}-N_{sil}-2)}  \sum\limits_{n=1}^{m} \big[p^n \cdot (1-p)^{(m-n)}\big]\big)\Big) \nonumber \\
&&  \cdot  \big((N_{max}-1) \cdot (T_{3}+T_{w}) + T_{1} + T_{2}\big)
\end{eqnarray}

The predicted duty cycle function, $D(P_C)$ of ContikiMAC is plotted in figure \ref{fig:estimating__closed_form_contikimac}, for channel check rates 4$Hz$, 8$Hz$, and 16$Hz$.
From this, the duty cycle of ContikiMAC is shown to increase with channel busy probability, 
affirming that node energy consumption is determined by the environmental interference.
This is more exaggerated with higher values of $f$.
As example, for $f=8Hz$, which is a common setting in WSN, the optimal duty cycle is 0.5\% under ideal conditions without interference ($P_C=0$).
Under worst-case conditions ($P_C=1$), the duty cycle increases to approximately 4.5\% - 11 times worse.
Obviously, in such circumstances WSN communication is not possible, but the model shows that interference levels must be considered when estimating network lifetime.

\begin{figure}
 \centering
  \includegraphics[width=.75\textwidth]{figures/estimating/closed_form.pdf}
  \caption{Closed form estimation of ContikiMAC duty cycle under interference conditions}
  \label{fig:estimating__closed_form_contikimac}
\end{figure}

\section{Monte Carlo Solver}
\label{sec:estimating__solver}

For a given value of $P_C$, the closed form solution $D(P_C)$ offers precise estimations of the duty cycle of affected nodes.
However, the difficulty of deriving $D(P_C)$ for a MAC protocol is dependant on the complexity of the wakeup sequence.
The implementation of ContikiMAC is quite simple, however deriving $D(P_C)$ is difficult, due in part to the fast sleep mechanism.
Even small modifications to ContikiMAC, such as changing the number of CCA checks used in the periodic channel check,
or allowing $N_{mac}$ to vary in response to $P_C$, for example, would require significant changes to $E(P_C)$.
Other MAC protocols which use more complex mechanisms introduce more paths and assigned probabilities through the state diagram, which $D(P_C)$ would need to account for.

A more flexible solution, which can be readily adapted to new changes and MAC protocols, is desirable.
To achieve this, a Monte Carlo solver was used.
Here, the MAC protocol wakeup sequence is executed in a simulator, from which the radio-on time is measured.
To determine the expected radio-on time, $E(p)$, the simulator is executed $N$ times.
For each run, the calculated radio-on time, $E_{n}(p)$ is recorded. 
The expected radio-on time is then the average, calculated as:

\begin{equation}
\label{eq:estimating__monte_carlo}
D(p) = \frac{f}{N} \displaystyle \sum_{n=1}^{N} E_{n}(p)
\end{equation}

Following the previous section, the ContikiMAC wakeup sequence was implemented in the simulator.
The simulator, which was written in the scripting language LUA, requires as input the channel busy probability, $p$.
This is incorporated into the simulation as a function call for the CCA, which returns true/false based on the probability $p$.
The solver takes as input also the channel check frequency, $f$, and outputs the expected duty-cycle for the wakeup sequence, $D(p)$.

Algorithm \ref{alg:estimating__mc_contikimac} shows the algorithm for ContikiMAC in the simulator.
This implementation is based on the protocol state machine, shown in figure \ref{fig:estimating__contikimac_state_machine}.

\begin{algorithm}[h!]
	\SetAlgoLined
	\KwIn{busy probability: $p$}
	\KwResult{radio on time: $E$}
	
			
	\vspace{1em}
	
	E = 0; first = 0; silence = 0; max = 0\;
	
	\vspace{1em}
	E += $T_{1}$;
	
	\If{ cca\_clear( p ) == FALSE }{
		return E\;
	}
	E += $T_{2}$;
	
	\If{ cca\_clear( p ) == FALSE }{
		return E\;
	}
		
	\While{ TRUE }{
		\If{ first != 0 }{
			first++\;
			period++\;
			\eIf{ cca\_clear(busy\_prob) == TRUE }{
				silence++\;
			}{
				silence = 0\;
			}
			
			\If{ (silence > $N_{sil}$) $OR$ (max > $N_{max}$) }{ return E\; }
			E += $T_{3}$ + $T_{w}$\;
		}
	}
	
	\caption{The Monte-Carlo solver simulation of the ContikiMAC state machine}
	\label{alg:estimating__mc_contikimac}
\end{algorithm}

This approach is much closer to the actual implementation of ContikiMAC, and so is trivial to implement.
The ContikiMAC protocol builds atop the Contiki OS, which provides a thread-based abstraction for sleeps and delays.
In \acp{OS} that lack this feature, or are event-based, a different approach may be appropriate.

The output of the monte carlo solver was measured for $f=16Hz$, and included in the figure \ref{fig:estimating__closed_form_contikimac}.
Comparison of the result with the result provided by the closed form solution shows little deviation.
The largest deviation is 0.25\%, with a busy probability of $p=0.3\%$.
Hence, the Monte Carlo solution was concluded to offer comparable precision of energy consumption estimation as the closed form solution, while still providing flexibility to handle different MAC protocol parameters.

\section{Evaluation}
\label{sec:estimating__evaluation}

In this section, the prediction accuracy of the monte carlo solver to estimate duty cycle of a MAC protocol in interference environments was evaluated.
Three experiments were carried out.
The first took place under controlled interference, and evaluated the accuracy of energy consumption estimation under various WiFi interference rates.
The second experiment took place in two uncontrolled interference environments, and again measured the prediction accuracy under realistic interference.
The final experiment measured also the effect of WSN traffic on the accuracy of these estimations.

\subsection{Controlled Interference}
\label{sec:estimating__exp1}

In this experiment, two tmote sky sensor nodes and an 802.11g network, which consisted of an access point and station, were
deployed in an unused office.
The ContikiMAC protocol, described previously, ran on one of the nodes using the default parameters, including
channel check frequency, $f=8Hz$.
As the node was running, the cumulative radio-on time was calculated for each experiment run.
No packets were sent from the node, hence the radio-on time is caused only by idle listening.
The second node measured the channel busy probability, $p$, throughout each experiment - without any MAC protocol or duty cycling.

The 802.11 network generated controlled interference using the iperf tool \cite{perf}.
The corresponding value for $p$, for each interference level as measured on the tmote sky node, 
is shown in table \ref{tab:estimating__pwifi}.
802.11g traffic rates were used corresponding to a channel busy probability range from $1.74\%$ to $23.81\%$.
It was not possible to obtain an entirely interference-free environment ($p=0$), due to background interference.
This is inconsequential though, as only $p$ is used as the independent variable, not the specific traffic rate.
Higher interference levels were not used as WSN deployments are unlikely in such environments.

\begin{table}
    \centering
    \begin{tabular}{|r|r|}
        \hline
        Busy Probability $p$ [\%]  & Data Rate [kbit/s]   \\
        \hline
        1.74 & 0      \\
        3.91 & 500     \\
        7.55 & 2000   \\
        13.10 & 4000  \\
        23.81 & 8000  \\
        \hline
    \end{tabular}
    \caption{The mapping of WiFi data rates and measured channel busy probability $p$.}
    \label{tab:estimating__pwifi}
\end{table}

The measured value for $p$ is plotted against the output of the Monte-carlo simulation and the actual measured duty cycle in figure \ref{fig:estimating__eval_1}.
As shown, the recorded duty cycle is very close to the prediction: the largest deviation from the recorded measurement is 7.4\%, for $p=7.55\%$.
Therefore, this approach has been shown to provide high prediction accuracy of estimating the radio duty cycle of ContikiMAC, under known interference conditions.
This experiment also supports earlier models and simulations: that the energy efficiency - and hence node lifetime - of ContikiMAC is severely affected under interference.
In this experiment $p$ was known and could be compared against the actual recorded duty cycle.
In a practical deployment, $p$ may only be measured once, to estimate the duty cycle.

\begin{figure}
    \begin{centering}
        \includegraphics[width=.75\textwidth]{figures/estimating/exp1.eps}
        \caption{ContikiMAC duty cycle under controlled interference settings.}
        \label{fig:estimating__eval_1}
    \end{centering} 
\end{figure}

\subsection{Uncontrolled Interference}
\label{sec:estimating__exp2}

In this experiment, the accuracy of the energy consumption prediction is evaluated in uncontrolled interference environments.
This presents a more realistic setting, which includes sources of interference other than 802.11g, such as Bluetooth.
In two deployments - a meeting room and an office - two sensor nodes were deployed as previously.
One recorded the environmental interference, measured as channel busy probability  $p$, which was reported every minute.
The second node ran the ContikiMAC protocol, reporting the duty cycle of the radio every minute; 
no traffic was generated by either node.
The host computer calculated the six-hour average $p$ and radio duty cycle from both nodes.
Using the monte carlo simulator, the predicted duty cycle was estimated.
The results from both deployments are plotted in figures \ref{fig:estimating__exp2a} and \ref{fig:estimating__exp2b}.

\begin{figure}
    \begin{centering}
        \includegraphics[width=.75\textwidth]{figures/estimating/exp2a.eps}
        \par
    \end{centering}
    \caption{ContikiMAC's duty cycle estimated and measured over time ($f=8$). This experiment was carried out in a meeting room.}
    \label{fig:estimating__exp2a}
\end{figure}

\begin{figure}
    \begin{centering}
        \includegraphics[width=.75\textwidth]{figures/estimating/exp2b.eps}
        \par
    \end{centering}
    \caption{ContikiMAC's duty cycle estimated and measured over time ($f=8$). This experiment was carried out in an office.}
    \label{fig:estimating__exp2b}
\end{figure}

These figures show again that the predicted duty cycle as calculated by the monte carlo solver, closely follows the 
observed duty cycle throughout the experiment.
The interference varies throughout the day according to activity within each room.
The average deviation of the predicted duty cycle from the measured duty cycle is 6.23\% and 2.09\% for the office and meeting room
respectively.
In the office deployment, the worst case deviation was 13.1\%, on day six.
In the meeting room deployment, the worse case was 12.94\%, on the second day.

\subsection{Background WSN Traffic}

Experiments in Sections \ref{sec:estimating__exp1} and \ref{sec:estimating__exp2} have shown that the duty cycle of ContikiMAC
can be accurately predicted based on the monte carlo solver.
These experiments were tested under the premise that idle listening constitutes the dominant use of the radio,
hence why previous experiments did not involve any WSN traffic.
Additional packet transmissions will increase the radio-on time, which is not accounted for in the prediction.

This experiment is similar to the first experiment: two sensor nodes are deployed in an unused office environment, alongside an 
802.11g network.
The same interference rates as in the first experiment were used here (see table \ref{tab:estimating__pwifi}).
One sensor node continuously calculates the channel busy probability, $p$, with no duty cycling or WSN traffic.
The second sensor node executes the ContikiMAC protocol, but also transmits (using the ContikiMAC sending function)
packets to another node, at different rates.
While acknowledgements are required as part of the protocol, retransmissions are not employed, and the 
standard ContikiMAC parameters are again used.
The predicted duty cycle, calculated from $p$, is plotted alongside the recorded duty cycle for packet transmit 
rates 1, 10, 30, and 240 packets per minute.
The results are shown in figure \ref{fig:estimating__eval_3}.

\begin{figure}
    \begin{centering}
        \includegraphics[width=.75\textwidth]{figures/estimating/exp3.eps}
        \caption{ContikiMAC duty cycle under controlled interference, for WSN traffic rates 1, 10, 30, and 240 packets per minute.}
        \label{fig:estimating__eval_3}
    \end{centering} 
\end{figure}

The results show that for very low data rates, the predicted duty cycle closely follows the recorded duty cycle
for all levels of interference.
For high WSN traffic levels however, such as 240ppm, the predicted duty cycle underestimates the measured duty cycle.
This is because idle listening is not the dominant cost

Under high WSN traffic - such as 240ppm, and low interference levels, the predicted duty cycle underestimates the measured
duty cycle.
This is because idle listening is not the dominant cost.
For high interference levels, however, the predictions continue to closely approximate the measured duty cycle,
regardless of WSN traffic rate.

\section{Chapter Summary}
\label{sec:estimating__summary}

In this section, the requirement for accurate energy consumption estimation has been discussed in the context of WSN MAC protocols under interference conditions.
Measurement approaches to quantify interference in an environment were discussed in Section \ref{sec:estimating__quantify}.
Based on the ContikiMAC protocol, a closed form solution was developed in Section \ref{sec:estimating__model}.
Following this, an easier approach was described based on a monte carlo simulation in Section \ref{sec:estimating__solver}.
The evaluation in Section \ref{sec:estimating__evaluation} showed that the latter is able to achieve high prediction 
accuracy under controlled and uncontrolled interference, and under typical WSN traffic conditions.
