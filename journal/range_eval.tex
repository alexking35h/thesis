% !TEX root = article.tex
\section{Range Evaluation}
\label{sec:range_eval}

\ac{P-DCCA} requires transmission power variation which means that a signal cannot be transmitted continuously with full power. Hence, it has to be assumed that the achievable transmission range is reduced. In this evaluation we evaluate the impact of transmit power variation on the achievable communication distance. 

The transmission power variation used by \ac{P-DCCA} reduces the signal strength and \ac{SNR} of the received signal. This reduces the ability of the IEEE 802.15.4 receiver to correctly decode the incoming signal, leading to an increased \ac{PER} for a given link. Since signal strength loss is a function of the distance between sender and receiver, \ac{P-DCCA} links will have a shorter communicable distance, compared to uniform transmission power.

We use two TelosB nodes configured as sender and receiver; the former is programmed to transmit 90 byte packets at a rate of 16 packets/second; the latter records each packet received to a buffer in RAM. These results are then retrieved by a host computer after the experiment. The experiment takes place outside in an empty sports field, and the distance between the nodes is varied from $90m$ to $180m$, in increments of $15m$. \ac{PRR} is recorded for 3 minutes, for each distance. For each increment, the \ac{PRR}  is calculated. The transmitter node does not implement \ac{CCA} checks or retransmissions, and neither node duty cycled the radio. Both nodes are elevated $75cm$ above the ground to ensure line of sight.

Two \ac{P-DCCA} configurations are used: $P_T = 0dB$ which is the uniform transmission power;  $P_T = 5dB$  which is the setting used for the CC2420 implementation. The results are shown in Figure~\ref{fig:range_prr}.


\begin{figure}
 \centering
 \includegraphics[width=.75\textwidth]{figs/range_prr.eps}
 \caption{Link distance for \ac{P-DCCA} and without power variation. \ac{P-DCCA} power variation leads to a reduction of communication distance. \ac{P-DCCA} achieves 100\% \ac{PRR} up to $105m$ compared to $150m$ without \ac{P-DCCA}.}
 \label{fig:range_prr}
\end{figure}

\ac{PRR} follows the same pattern with both transmission powers: 100\% \ac{PRR} up to a cutoff point, beyond which no packets are received. The receiver can compensate bit errors up to a point where the packet cannot be successfully reconstructed. The transitional region between 100\% \ac{PRR} and 0\% is often very small as in this experiment (see Petrova et al. \cite{petrova2006}). 
For $P_T = 5dB$, this distance is $105m$. By contrast, using the maximum uniform transmission power, $P_T = 0dB$, this range is extended to $150m$. Therefore, \ac{P-DCCA} in this implementation incurs a reduction in link range of approximately 30\%.

The reduced link range is the main drawback of using \ac{P-DCCA}. In a large, multi-hop network, more hops may be needed to communicate, increasing energy consumption and offsetting the benefits of \ac{P-DCCA}. Otherwise, if nodes are too sparsely deployed, \ac{P-DCCA} may fragment the network and prevent communication entirely. \ac{P-DCCA} may therefore be better suited to either smaller networks, with few nodes, or large, multi-hop networks, with many possible links.

However, when considering the design of sender-initiated asynchronous \ac{MAC} protocols, it becomes apparent that the link performance is not the only parameter determining maximum link distance. Receivers implementing these \ac{MAC} protocols periodically sample the channel energy using \ac{CCA} checks: if the set \ac{CCA} threshold is exceeded, an incoming packet is inferred, and the radio remains powered on to receive a packet. Otherwise, the radio is powered off, until the next wake-up sequence. Therefore, for a packet to be received it is necessary that the \ac{RSSI} exceeds the \ac{CCA} threshold. Lowering the threshold improves detection of incoming transmissions (and range) while increasing the rate of false wake-ups. For example, Oppermann et al. \cite{oppermann15configuration} have investigated \ac{CCA} threshold selection to balance \ac{PRR} and energy consumption.

Even though it might be generally possible to receive a packet at a specific distance, the receiver may not be able to clearly detect the incoming transmission using \ac{CCA}. Thus, the limiting factor of maximum link distance in sensor networks may be detection of the incoming transmission rather than the ability to decode it successfully. 




