set term epscairo size 5,3
set output 'exp2a.eps'

set ylabel "Duty Cycle D(p) in [%]"
set yrange [0:1.5]

set grid

set xtics rotate

set datafile separator ","
plot 'exp2a.csv' using 2:xtic(1) title "Measured" lw 2 with lines, \
     'exp2a.csv' using 3:xtic(1) title "Monte Carlo" lw 2 with lines

set output 'exp2b.eps'

set ylabel "Duty Cycle D(p) in [%]"
set yrange [0:1.5]

set grid

set xtics rotate

set datafile separator ","
plot 'exp2b.csv' using 2:xtic(1) title "Measured" lw 2 with lines, \
     'exp2b.csv' using 3:xtic(1) title "Monte Carlo" lw 2 with lines
