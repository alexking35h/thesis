% !TEX root = article.tex
\section{\acf{P-DCCA}}\label{sec:dcca}

In this section, \ac{P-DCCA} is described in detail, including an implementation on \ac{WSN} hardware. Our approach is based on varying the output power between two values during packet transmission. Our implementation is based on the common TI CC2420 transceiver, however we believe \ac{P-DCCA} should be implementable on any IEEE 802.15.4-compliant hardware. An implementation of \ac{T-DCCA} based on ZiSense~\cite{zheng2014zisense} is also briefly described. We use this implementation in our evaluation to compare our \ac{P-DCCA}  approach with \ac{T-DCCA}.


\subsection{\ac{P-DCCA} Mechanism}

\ac{P-DCCA} requires transmission of packets with alternating transmission power. A \ac{P-DCCA} check must detect this information if present. Within this process the implementation specifics of the \ac{RSSI} measurement facility on transceiver hardware must be taken into account. The 802.15.4 specification states that \ac{CCA} detection time shall be equal to eight symbol periods. Hence, a reading of the \ac{RSSI} register on currently available  802.15.4 transceivers provides an average \ac{RSSI} measurement over eight symbol periods. Alteration of the transmission power as used for \ac{P-DCCA} and as shown in Figure~\ref{fig:dcca_plot_tx} is perceived as slow increase or decrease of the \ac{RSSI} at the receiver as an average is computed (see Figure~\ref{fig:dcca_plot_rx}).

\begin{figure*}[ht]
 \centering
  \subfigure[P-DCCA Modulation Transmitter] {
   \includegraphics[width=0.5\textwidth]{figs/DCCA_plot_diagram_tx.eps}
   \label{fig:dcca_plot_tx}
  }%
  \subfigure[Detected P-DCCA modulation receiver] {
   \includegraphics[width=0.5\textwidth]{figs/DCCA_plot_diagram_rx.eps}
   \label{fig:dcca_plot_rx}
  }
 \centering
 \caption{\ac{P-DCCA} transmitter power cycle and observed \ac{RSSI} reading at a receiver node.}
 \label{fig:dcca_plot}
\end{figure*}

The timing of transmission power alteration and the algorithm to detect presence of this signature must take this \ac{RSSI} averaging process into account. Next we discuss transmission and detection procedures in detail. Parameters governing these mechanisms are given in Table~\ref{tab:parameters}. Table~\ref{tab:parameters} also shows settings for these parameters as used within our implementation of \ac{P-DCCA} based on a TI~CC2420 transceiver, which we describe later.

\begin{table}[!t]
\tbl{P-DCCA parameters and values for CC2420 802.15.4 Transceiver.}{
   \begin{tabular}{| p{1.8cm} || p{7.4cm} | p{1.4cm} |}
   \hline
   \textbf{Parameter} & \textbf{Description} & \textbf{CC2420} \\
   \hline
   \hline
   $P_T$ & Difference between the two used power levels  & 5$dB$ \\
   $T_T$ & Transmit power variation period & 256$\mu s$ \\
   $T_R$ & Duration of a \ac{P-DCCA} check & 256$\mu s$ \\
   $N_R$ & Number of \ac{RSSI} samples taken during a \ac{P-DCCA} check & 8 \\
   $C$   & Duration of a \ac{RSSI} sample & 32$\mu s$ \\
   \hline
   $P_{min}$ & Minimum power range of the \ac{P-DCCA} sample set & 2$dB$ \\
   $P_{max}$ & Maximum power range of the \ac{P-DCCA} sample set & 7$dB$ \\
   $P_{\delta}$ & Maximum power difference between two consecutive \ac{P-DCCA} samples & 4$dB$ \\
   $N_E$ & Maximum number of  extrema in the \ac{P-DCCA} sample set & 2 \\
   $\tau_{RSSI}$ & The minimum threshold for each RSSI sample & -75$dB$ \\
   \hline
  \end{tabular}}
  \label{tab:parameters}
\end{table}

\subsection{\ac{P-DCCA} Signal Transmission}
$P_T$ and $T_T$ define the power-cycle of \ac{P-DCCA}, used during the transmission of \ac{P-DCCA} modulated packets. Packets are transmitted while alternating between two transmit power settings\footnote{In practical settings the strongest and second strongest available transmission power are used but other power settings are possible too.} resulting in a transmit power range of $P_T$. Each transmit power is used for the same length of time covering the period $T_T$. 


$T_T$ is set to $256\mu s$. This is twice the length of the \ac{RSSI} averaging window, which in most cases is 8 symbol periods ($128\mu s$).
Thus, it is ensured that the maximum and minimum transmission power level used are each fully represented in the \ac{RSSI} register during a power-cycle (see Figure~\ref{fig:dcca_plot_rx}). Aligning the power-cycle duration $T_T$  with the  \ac{RSSI}  averaging time ensures that a node performing \ac{P-DCCA} can observe a signal constantly alternating between minimum and maximum power which simplifies the detection process. 

$P_T$ must be large enough to ensure that signal features are reliably discernible to receivers. $P_T$ must be selected such that power variations can be distinguished clearly from natural signal strength variations. $P_T$ must also be minimised such that transmission power remains sufficiently strong at the intended receiver, as reduced transmit power leads to a reduced transmission range.

\subsection{\ac{P-DCCA} Signal Detection}
A simple algorithm is necessary for signal analysis as a fast execution on resource constraint nodes is necessary. The signal analysis must be carried out during a brief \ac{CCA} check. The wave form as shown in Figure~\ref{fig:dcca_plot_rx} must be detected reliably. This can be achieved by simply detecting local power extrema and analysis of power ranges. During \ac{P-DCCA} the \ac{RSSI} register is sampled periodically for a period of time $T_R$. The sampling period must be larger or equal to the power-cycle period $T_T$ to ensure that at least one complete set of power extrema is covered ($T_T\le T_R$).  The collected \ac{RSSI} \emph{sample set} is then analysed using Algorithm~\ref{alg:dcca}.  

%\newcommand{\BREAK}{\STATE \textbf{break}}
\newcommand{\LINEIF}[2]{%
    \STATE\algorithmicif\ {#1}\ \algorithmicthen\ {#2} %\algorithmicend\ \algorithmicif%
}
\begin{algorithm*}
\caption{Power Differentiating Clear Channel Assessment (\ac{P-DCCA}).}
\label{alg:dcca}
  \begin{algorithmic}[1]
  
    \FOR{i = 1 \TO $N_R$}
      \LINEIF{(s[i] = RSSI()) $<\tau_{RSSI}$}{\textbf{break} } 
    \ENDFOR

    \LINEIF{$i = 1$}{\textbf{return} CLEAR}
    \LINEIF{$i < N_R$}{\textbf{return} BUSY\_INCONCLUSIVE}
 
    \FOR{i=1 \TO ($N_R-1$)}
  
      \LINEIF{$|s[i] - s[i+1]| > P_{\delta}$}{\textbf{return} BUSY\_OTHER}
  
      \IF{$s[i] > s[i+1] \land slope \neq SLOPE\_INCREASING$}
        \STATE$slope \gets SLOPE\_INCREASING$
        \STATE$counter \gets counter+1$
      \ENDIF
  
      \IF{$s[i] < s[i+1] \land slope \neq SLOPE\_DECREASING$}
        \STATE$slope \gets SLOPE\_DECREASING$
        \STATE$counter \gets counter+1$
      \ENDIF
  
    \ENDFOR
    \IF{($range(s) < P_{min} \lor (range(s) > P_{max}) \lor (counter > N_E)$)}
      \RETURN BUSY\_OTHER
    \ENDIF

    \RETURN BUSY\_PDCCA
  \end{algorithmic}
\end{algorithm*}

The first two lines in the algorithm fill the sample set $s$ with \ac{RSSI} values until a single value below the  threshold $\tau_{RSSI}$ is received, or until $N_R$ samples have been taken. $N_R$ is dictated by the achievable sampling frequency and $T_R$.  If the first RSSI sample is below the threshold $\tau_{RSSI}$,  \ac{P-DCCA} indicates that the channel is free, without taking any more samples. Thus, an unoccupied channel causes  \ac{P-DCCA} to behave identically to the normal \ac{CCA} method. If $N_R$ samples are collected over the minimum threshold, the algorithm measures these samples against the given thresholds in Table~\ref{tab:parameters}. On line 6, the absolute value between consecutive samples is compared against $P_{\delta}$, and lines 7 through 12 count the number of local power extrema. Finally on line 13, the range of the samples and the number of local extrema are compared against their thresholds $P_{min}$, $P_{max}$  and $N_E$  respectively. A signal passing these thresholds is classified as a valid, \ac{P-DCCA} modulated 802.15.4  signal.

\subsection{\ac{P-DCCA} Outcome}
\label{sec:dcca_outcome}
\ac{P-DCCA} can have four distinct outcomes:
\begin{enumerate}
\item \emph{CLEAR}: indicates that the medium is currently free. 
\item \emph{BUSY\_PDCCA}: indicates that a transmission with \ac{P-DCCA} power modulation was detected.
\item \emph{BUSY\_OTHER}: indicates that another signal without \ac{P-DCCA} was detected, such as WiFi, Bluetooth, or another IEEE 802.15.4 device outside the network.
\item \emph{BUSY\_INCONCLUSIVE}: indicates that the medium is busy but the channel occupier cannot be determined. 
\end{enumerate}

\ac{P-DCCA} requires $N_R$ \ac{RSSI} samples, above the threshold, to identify a signal. Therefore, if a \ac{P-DCCA} check coincides with the end of a busy period, an incomplete \ac{RSSI} set may be recorded and no source may be definitively identified. This results in a \emph{BUSY\_INCONCLUSIVE} result, the interpretation of which is left to the \ac{MAC}
layer. Situations leading to these \ac{P-DCCA} outcomes are shown in Figure~\ref{fig:dcca_samples}.

\begin{figure}
 \begin{centering}
  \includegraphics[width=1.0\columnwidth]{figs/dcca_outcomes.eps}
  \caption{
    Possible \ac{P-DCCA} outcomes: 
    1) One sample: channel is clear (\emph{CLEAR})
    2) Complete sample set: channel is either \emph{BUSY\_PDCCA} or \emph{BUSY\_OTHER}
    3) Incomplete sample set: channel is busy, but unknown origin: \emph{BUSY\_INCONCLUSIVE}}
  \label{fig:dcca_samples}
 \end{centering}
\end{figure}

\subsection{\ac{P-DCCA} Interpretation}
Node behaviour after obtaining a \ac{P-DCCA} result is to be decided by upper layers. \emph{CLEAR} may be treated similar to a clear resulting from a normal \ac{CCA}. \emph{BUSY\_PDCCA} would trigger the back-off procedures designed to coordinate competition for the channel among nodes of the same network. The reaction to \emph{BUSY\_OTHER} may depend on knowledge of the deployment area. For example, it might be known that other interference is likely to stem from a co-located WiFi network. 

In case of \emph{BUSY\_INCONCLUSIVE}  the decision may depend on worst-case or best-case assumptions. For example, a prudent node listening to the channel for incoming packets may treat this result as normal 802.15.4 traffic, leaving the radio powered on to receive data,  while an energy-conscious node may ignore altogether any interference except confirmed 802.15.4 traffic.

Alternatively, \emph{BUSY\_INCONCLUSIVE} results can be avoided altogether by stipulating design requirements on the \ac{MAC} protocol.
Knowing the duration between packet transmissions, two \ac{P-DCCA} checks can be arranged so that at least one will fall within a packet transmission. Thus, inconclusive results can be ignored entirely. This approach ensures that at least one sample set captures a \ac{P-DCCA} signal, ensuring reliable operation. 


\subsection{CC2420 Implementation of \ac{P-DCCA}}
Our implementation and evaluation of \ac{P-DCCA} is based on the Tmote~Sky \ac{WSN} platform~\cite{corporation2005tmote}. This platform uses the TI~MSP430~MCU and CC2420 radio, which is compliant with the IEEE~802.15.4 \ac{PHY}. The specific \ac{P-DCCA} parameters used for the implementation are given in Table~\ref{tab:parameters}.

The \ac{RSSI} register of the CC2420 does not contain an instantaneous representation of the current channel energy. It contains an average over the preceding 8 symbol periods of $128\mu s$ duration. We set $T_T=256 \mu s$. We set $T_R=T_T$ as we decided to use only one power-cycle for detection ($N_E=2$). Larger $T_R$ could improve detection accuracy but would increase \ac{P-DCCA} duration which would have an additional energy cost. However, as our evaluation shows (see Section~\ref{sec:accuracy_eval}) this duration is sufficient to achieve high detection accuracy. We use a power difference of $P_T=5dB$ as we observed this to be the smallest power difference yielding good detection accuracy. The transmit power alters between $0dB$ and  $-5dB$.  

The maximum RSSI sampling frequency we were able to achieve on the CC2420 was $33Mhz$.
To ensure reliable timing with the RSSI sampling window, this was reduced to $31.2Mhz$ to synchronise with every second IEEE 802.15.4 symbol.
Based on this, $N_R=8$  samples are collected during a power-cycle of length $T_T$. We empirically set $P_{min}=2dB$, $P_{max}=7dB$ and $P_{\delta}=4dB$ which is tuned to the chosen transmission power difference $P_T=5dB$ and accuracy of the CC2420 \ac{RSSI} measurement facility.

\ac{P-DCCA} is implemented in the Contiki \ac{WSN} OS \cite{dunkels2004contiki}, where operation of the radio driver strays little from the standard implementation. The function used to transmit a packet blocks until the transmission of a packet has been completed; in \ac{P-DCCA}, this blocking time is used to modulate the output power. In our measurements, the \ac{P-DCCA} function call may take up to $260 \mu s$, as opposed to the immediate response with normal \ac{CCA}; this timing component must be accounted for in the implementation of \ac{MAC} protocols. 


\subsection{Implementation of \ac{T-DCCA}}
\label{sec:tdcca_zisense}

In ZiSense \ac{RSSI} traces are searched for known IEEE 802.15.4 signatures. ZiSense is designed to improve the wake-up and rendezvous mechanism in \ac{WSN} \ac{MAC} protocols: to reduce the false wake-up problem, and improve the resilience of synchronisation packets in protocols such as X-MAC \cite{buettner2006x}.  Although ZiSense was not directly designed as \ac{DCCA} mechanism, it can be employed in this capacity as representation of \ac{T-DCCA} and we use it in this work for comparison in our evaluation.

Each ZiSense check listens to the channel for a fixed duration, sampling the \ac{RSSI} into a buffer. The buffer is analysed and segments are identified as contiguous subsets of \ac{RSSI} samples when the signal differs from the noise floor. For each segment detected, a feature-set is constructed describing:

\begin{enumerate}
    \item On-air time
    \item \ac{PAPR}
    \item \ac{IPS} 
    \item \ac{UNF} 
\end{enumerate}

(1) and (3) describe temporal features of the signal, which are typically specific to the \ac{MAC} protocol. The original authors of ZiSense used TinyOS \ac{LPL} as the \ac{MAC} protocol in their evaluation. (2) stems from the modulation used by the transmitter. (4) is a feature unique to interference from electric machinery (i.e. Microwave Ovens), where the signal strength drops below the noise floor.

These feature sets are then passed to a classification algorithm, to classify each segment as either IEEE 802.15.4 or other.
In \cite{zheng2014zisense}, three algorithms are described: \textit{ZiSense-1}, \textit{ZiSense-2}, and \textit{ZiSense-C4.5}.
\begin{itemize}
    \item \textit{ZiSense-1}: A classifier manually derived from strict interpretation of IEEE 802.15.4 \ac{PHY} and TinyOS \ac{LPL}.
    \item \textit{ZiSense-2}: A more forgiving classifier manually derived to identify segments that have collided with other interference.
    \item \textit{ZiSense-C4.5}: A classifier build using the C4.5 algorithm, which builds a decision tree classifier from a prior-known dataset - in this case from segments which were identified manually.
\end{itemize}

Since no implementation of ZiSense is currently available we recreated the ZiSense algorithm offline, based on \cite{zheng2014zisense}.
Our implementation samples the \ac{RSSI} register at the same frequency into a buffer on a Tmote Sky sensor node. Once full, these raw \ac{RSSI} values are sent to the host computer via serial link. The ZiSense classification algorithms described above are then run offline.
An offline approach avoided complications in developing embedded \ac{WSN} software, while still allowing for classification accuracy comparisons.

It has to be noted that a \ac{CCA} check using ZiSense (a \ac{T-DCCA}) variant takes much longer than a \ac{CCA} check using \ac{P-DCCA}. Longer sample periods are required and more complex computation is necessary compared to \ac{P-DCCA}. Hence, ZiSense is more costly in terms of energy consumption. On the other hand, ZiSense does not affect communication range. \ac{P-DCCA} requires active modification of the transmission signals while ZiSense needs to have a clear understanding of protocol behaviour. Both options have advantages and disadvantages we quantify in our evaluation. 






