set term postscript
set output 'csma_effectiveness.ps'

set multiplot layout 3,1
set yrange[0:.5]
unset key
set grid
set ylabel "PRR: 100KB/s"
plot 'csma_effectiveness_100kbs.tsv' using 2:5 title "100KB/s"
unset xlabel
set ylabel "PRR: 500KB/s"
plot 'csma_effectiveness_500kbs.tsv' using 2:5 title "100KB/s"
set ylabel "PRR: 1000KB/s"
set xlabel "Backoff window (us)"
plot 'csma_effectiveness_1000kbs.tsv' using 2:5 title "100KB/s"

