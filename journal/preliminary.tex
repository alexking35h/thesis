% !TEX root = article.tex
\section{Preliminary Discussion}
\label{sec:preliminary}

The 802.15.4 physical layer is a common choice for \acp{WSN} due to its low power consumption and suitability for low data rate applications. The \SI{2.4}{\GHz} frequency is shared with other protocols and devices; as their prevalence continues to increase, parallel deployments with 802.15.4 are becoming more common. Without due consideration to network coexistence, network performance is sub-optimal. Sources of interference include Bluetooth, WiFi, and other 802.15.4 devices, as well as non-communication devices such as microwave ovens.

\subsection{\ac{CCA} and the Case for \ac{DCCA}}
\ac{WSN} \ac{MAC} protocols use \ac{CCA} on the transmitter and receiver side. As \ac{CCA} only allows us to distinguish a busy from an idle channel the same policy must be used for all channel busy cases. However, a less ambiguous \ac{CCA}  response would be advantageous, allowing nodes to implement a response dependent on the nature of the interference.

\begin{figure*}
 \begin{centering}
 \includegraphics[width=\textwidth]{figs/WSN_LPL_diagram.eps}
 \caption{Operation of \ac{LPL} \ac{MAC} protocols:
          1) Transmit request followed by \ac{CCA} check, then packet strobes.
          2) Periodic receiver \ac{CCA} check to listen for incoming packets.
          3) After data is received, an \ac{ACK} is transmitted, packet strobes stop.
         }
 \label{fig:lpl_mac_protocols}
 \end{centering}
\end{figure*}

\paragraph*{Transmitter \ac{CCA}} 802.15.4 nodes must assess the state of the channel before transmitting data to avoid collisions with other nodes and to increase the likelihood of the data being received (see Figure~\ref{fig:lpl_mac_protocols}). For example, a sender will defer and retry later upon detecting other channel activity - presumed likely to corrupt any transmission attempt. \ac{CCA} checks are commonly used for this purpose: briefly sampling the channel and indicating the current state. Currently used \ac{CCA} implementations offer an energy threshold indicator: busy if the energy on the channel exceeds a set threshold, free otherwise.  As noise can lead to a false channel assessment, \ac{CCA} methods have been devised which analyse a number of consecutive \ac{CCA} checks together \cite{polastre2004}. Such outlier detection improves channel assessment but it can still only be decided if the channel is free or not.  This ambiguity requires \ac{MAC} protocols to implement a single policy in response to all interference. 

However, to improve network performance senders can opt to ignore interference from sources outside the network, as the recipient may still be able to receive the packet over the interference. This response is reasonable in cases without mutual channel etiquette, such as WiFi. Alternatively, the transmitter can apply different back-off timing depending on the interference source. When competing with WiFi nodes for a channel, much shorter back-off timers are required than in a case where competing with other 802.15.4 nodes.    
The optimal response policy depends on the MAC protocol used, the coexisting network(s) and the traffic patterns in the network.  

\paragraph*{Receiver \ac{CCA}} \ac{CCA} is also used on the receiving side of energy efficient \ac{WSN} \ac{MAC} protocols, such as ContikiMAC \cite{dunkels2011contikimac} (see Figure~\ref{fig:lpl_mac_protocols}). The radio is the greatest source of power consumption in most sensor nodes, often dictating network lifetime. Thus, \ac{MAC} protocols seek to leave the radio in an energy efficient sleep state, retaining operational information (such as channel, transmission power), but unable to send or receive packets. In order to receive data, nodes must periodically listen to the channel for incoming transmissions, a period defined by the \emph{duty~cycle}. A node wishing to send data to another node must transmit repeatedly to the recipient throughout this duty cycle, terminating only if an acknowledgement has been received, or a timeout has occurred.

Due to the small energy cost and immediate response, \ac{CCA} is commonly used to sample the channel, quickly indicating the channel state. Detecting an occupied channel, the radio is left powered on to receive any subsequent packet, otherwise returning the radio to a sleep state. 

Once again, the ambiguity of \ac{CCA} requires nodes to implement a single policy in response to any channel activity. In environments frequented by other interference, this leads to increased \emph{false~wake-ups}, where nodes listen to the channel in response to spurious interference, wasting energy.
To improve energy consumption a node can implement different policies in regard to the interference type detected. It is sensible to prevent a wake-up in case of non-802.15.4 traffic. 

\subsection{\ac{DCCA} Options}
As shown, \ac{DCCA} is a valuable building block for \ac{MAC} protocols and the question is how it can be realised. We consider three possible avenues to realise \ac{DCCA}: \ac{MD-DCCA}, \ac{T-DCCA}, and \ac{P-DCCA}.

\paragraph*{\acf{MD-DCCA}}  
The 802.15.4 standard defines modulation detection as an optional \ac{CCA} method - indicating busy if a signal with the correct modulation and spreading characteristics is detected. Alongside energy detection, this would be ideal for implementing \ac{DCCA} with little processing overhead. This approach, termed \ac{MD-DCCA}, could discern between a free channel, a non-802.15.4 interferer (such as WiFi), and 802.15.4 transmissions, thereby achieving \ac{DCCA}. Unfortunately, in order for transceivers to provide modulation detection, more complex radio circuitry is required.  Therefore, most currently available transceivers do not implement this method, and opt instead only for simple energy detection \ac{CCA}.

This is the case with the Texas Instruments family of IEEE 802.15.4 transceivers, including the CC2420, found in many \ac{WSN} installations. 
While the specification for the CC2420 \cite{cc2420spec} suggests that all \ac{CCA} modes are supported, this is not the case for modulation detection, where a busy channel is inferred only if a packet is currently being received. Since this requires being able to receive and decode the packet preamble, this is insufficient to implement \ac{DCCA}.

This was confirmed experimentally using three Tmote Sky nodes. The first was programmed to transmit packets continuously. The other two nodes continuously performed \ac{CCA}, one using energy detection and one using modulation detection. Two experimental runs were carried out. In the first, sender and receivers use the same \ac{SFD} while in the second run different \ac{SFD} are used. 

The results are shown in Figure~\ref{fig:cc2420_cca_modes}. Channel~0 shows the transmitter, a high signal is shown when a packet is in transmission. Channel~1 shows the receiver using modulation detection and  Channel~2 shows the receiver with energy detection. The receivers show a low signal when the \ac{CCA} detects a transmission. Figure~\ref{fig:cc2420_cca_modes}(a) shows the results when sender and receiver use the same \ac{SFD} setting while Figure~\ref{fig:cc2420_cca_modes}(b) shows the result for differing \ac{SFD}  configuration. 

It can be seen that for modulation detection to work on the CC2420, the \ac{SFD} value must match on the transmitter and the receiver. A \ac{CCA} using modulation detection that would be performed after the packet transmission has begun would result in a clear channel response (as the \ac{SFD} would not be seen).  This shows that modulation detection as-per the IEEE 802.15.4 specification is not available on the TI CC2420 family. The same observation has been made previously by Petrova et al. \cite{petrova2007interference}.

\begin{figure}
 \centering
 \subfigure[CC2420 CCA with same transmitter and receiver SFD.] {
  \includegraphics[width=.65\textwidth]{figs/logicprobe_samesfd.png}
 }
 \subfigure[CC2420 CCA with different transmitter and receiver SFD.] {
  \includegraphics[width=.65\textwidth]{figs/logicprobe_diffsfd.png}
 }
 \caption{CC2420 CCA implementations using energy and modulation detection modes.
          If the value of the SFD at the receiver is changed, modulation detection can no longer detect IEEE 802.15.4 packets}
 \label{fig:cc2420_cca_modes}
\end{figure}


\paragraph*{\acf{T-DCCA}} 
The different observable characteristics between \ac{PHY} and \ac{MAC} layers can be leveraged to implement \ac{DCCA}. For example, the largest 802.11g packet requires only \SI{254}{\mu s} on-air time, while the largest 802.15.4 packet requires \SI{4224}{\mu s}. Likewise, the \ac{OFDM} used by 802.11n means that the energy on a single overlapping 802.15.4 channel varies throughout the transmission. This is unlike 802.15.4 modulation, which uses \ac{DSSS} and has a more static profile.

WSN nodes can observe these features by recording a \ac{RSSI} trace at high frequency, from which 802.15.4 signals can be identified in the time domain. Hence, this approach is termed \ac{T-DCCA}. To achieve high accuracy, nodes implementing \ac{T-DCCA} require a longer listening duration than compared to the standard IEEE 802.15.4 \ac{CCA}. This increases latency before receivers can react to a \ac{DCCA} result, and increases the energy consumption of idle listening.

ZiSense~\cite{zheng2014zisense} is an implementation of \ac{T-DCCA}  which we use for comparison with the \ac{P-DCCA} approach presented in this paper.

\paragraph*{\acf{P-DCCA}}
Building on \ac{T-DCCA}, \ac{P-DCCA}, also samples \ac{RSSI} at high frequency to identify prior-known signal characteristics. However, in order to reduce the required sampling duration, \ac{P-DCCA} does not rely on inherent 802.15.4 features, such as spectral profiles and packet durations. Instead, \ac{P-DCCA} relies on an additional signal component that is modulated by the transmitter, and can be detected by analysing an \ac{RSSI} trace. This is orthogonal to the data transmission, and acts only to differentiate signals from interference. Since these features are intentionally observable on a smaller timescale, the detection time is reduced compared to \ac{T-DCCA}.

One implementation of \ac{P-DCCA}, which we describe in Section~\ref{sec:dcca}, achieves this by varying the output power cyclically throughout packet transmission. \ac{P-DCCA} receivers sample the \ac{RSSI} register at high frequency, detecting these encoded characteristics within the trace. Signals which lack this feature are assumed to be other interference. This approach has a short detection period, close to the IEEE 802.15.4-standard \ac{CCA} duration, and thus has a lower idle listening cost compared to \ac{T-DCCA}.

%%% Summary.

\subsection{Discussion}
Traditional energy-based \ac{CCA} has limitations and we have presented \ac{DCCA} as a conceptual extension to \ac{CCA}. \ac{DCCA} enables us to differentiate interference sources and to implement policies on handling interference.

\ac{MD-DCCA} would be an efficient solution, however, as this mechanism is not available on most radio chips used today it is desirable to have alternative methods at hand.  Both \ac{T-DCCA} and \ac{P-DCCA} rely on \ac{RSSI} features, recorded over time, to identify interference. The former is a passive approach, and relies on features inherent to the IEEE 802.15.4 \ac{PHY}, and \ac{MAC} protocol, and therefore requires long sampling durations which reduce energy efficiency. Conversely, \ac{P-DCCA} is an active approach, which modulates an orthogonal signal feature to all outgoing transmissions. This can be detected during far shorter \ac{P-DCCA} checks - hence improving energy efficiency. Also, this method allows us to differentiate between two different 802.15.4 networks if required. 

The power variation used in \ac{P-DCCA} reduces the \ac{SNR} of the transmission, which in turn reduces the maximum communication distance (See evaluation in Section~\ref{sec:range_eval}). This presents a tradeoff between \ac{P-DCCA} and \ac{T-DCCA} in terms of shorter idle listening and link range.



