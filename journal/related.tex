% !TEX root = article.tex
\section{Related Work}
\label{sec:related}

The issue of coexistence between  802.15.4 and other devices sharing the radio spectrum is widely established, leading to an assortment of detection, classification and mitigation methods. In particular,  802.11 has been the focus of previous coexistence work, due to its increasing prevalence in home and industrial automation, medical and sensing technologies -  domains popular with 802.15.4. Hauer et al.~\cite{hauer2009experimental} have shown that WiFi interference is a main cause of packet loss in 802.15.4 networks. The underlying reasons behind this coexistence issue are discussed by Liang et al.~\cite{liang2010surviving}, asserting that insensitivity to 802.15.4 transmissions and disparate timing parameters disadvantage 802.15.4 nodes and contribute to reduced network performance. 

To mitigate the effects of interference on packet loss in \ac{WSN}, Tang et al. present a solution based on \ac{CCA} threshold adaptation \cite{tang2013study}.
Under WiFi interference, the authors show that \ac{CCA} collisions are reduced by increasing the \ac{CCA} threshold, thus improving packet delivery rate in the \ac{WSN}.
The authors present a solution that adaptively sets the \ac{CCA} threshold based on the rate of transmit buffer overflows in a node.
This approach is based on ZigBee, which has only one \ac{CCA} check before transmitting.
This is not the case for many \ac{WSN} \ac{MAC} protocols, which have multiple \ac{CCA} per transmission.

Sha et al. similarly consider the \ac{CCA} threshold in order to mitigate false wake-ups caused by interference \cite{sha2013energy}.
The authors present AEDP, which attempts to raise the receiver \ac{CCA} threshold in order to reduce false wake-ups, 
while keeping the threshold low enough however to ensure valid packets are still detected.
In order to adapt to different environments, AEDP seeks this optimisation at runtime.

In both cases, these approaches rely on \ac{RSSI} of interference to distinguish from \ac{WSN} packets, which is not always guaranteed to be the case.
This is not the case with \ac{P-DCCA}, which modulates the transmission power of sent packets, which can then be detected by other \ac{WSN} receivers.

Tang et al. present Interference Aware Adaptive Clear Channel Assessment (IAACCA), which more proactively contends for channel access by replacing the standard \ac{CCA} \cite{tang2013interference}.
Instead of a single \ac{CCA} check, the channel is sampled continuously until found to be clear.
IAACCA is shown to reduce packet loss under WiFi interference compared to standard \ac{CSMA} mechanism.
In the context of this work, IAACCA offers a policy decision after collision with interference, whilst \ac{P-DCCA} is a replacement for the standard \ac{CCA}.
These works are therefore compatible, and may offer benefits if used in parallel.

Zheng et al. describe ZiSense \cite{zheng2014zisense}, an active scanning technique in duty cycling \ac{MAC} protocols to reduce false wake-ups. Similar to \ac{P-DCCA}, ZiSense samples \ac{RSSI} at high frequency, listening for timing and spectral characteristics indicative of 802.15.4. Based on these features, ZiSense presents one approach to realising \ac{DCCA}, which we referred to  as \ac{T-DCCA} in Section~\ref{sec:tdcca_zisense} where it is discussed. By contrast, \ac{P-DCCA} does not rely on signal characteristics inherent to 802.15.4 to detect incoming traffic. It instead modulates an identifiable signal to the output power of the transmitter, which requires less radio-on time to detect.

The accuracy of ZiSense and \ac{P-DCCA} is evaluated empirically in Section~\ref{sec:accuracy_eval},  and the energy consumption of both is compared theoretically in Section~\ref{sec:energy_evaluation}. \ac{P-DCCA} is shown to achieve greater accuracy, while requiring less listening time - hence further reducing the energy cost of idle listening. 

Tang et al.~\cite{tang2013study} evaluates the available 802.15.4 \ac{CCA} modes, energy thresholds and packet loss in the presence of WiFi interference. An adaptive method is proposed to set the energy threshold to achieve optimum performance. Our work goes further than modification of \ac{CCA} thresholds and an entire new \ac{CCA} method is proposed.

Detecting and classifying interference has featured in literature to permit deployment planning and direct mitigation techniques \cite{zacharias2012identifying,zheng2014zisense,chowdhury2009interferer,zhou2010zifi,bloessl2012low}. These techniques scan the channel over time and observe temporal and spectral indications of heterogeneous traffic. Reaction time is typically in the order of milliseconds or seconds, permitting only per-link countermeasures and infrequent samples to save energy. \ac{P-DCCA} is not designed to classify environmental interference.  Rather, to infer in real time the nature of present channel usage, replacing standard \ac{CCA} and permitting interference-specific responses for each transmission. We focus on shortening the detection time without sacrificing accuracy, differentiating only between 802.15.4 and non-802.15.4 interference.  We find this to be sufficient for mitigating interference.

An experimental comparison with interference classification methods would be frivolous. Nonetheless, comparing the results of our \ac{P-DCCA} evaluation in Section~\ref{sec:accuracy_eval} with these approaches, suggests comparable accuracy in detecting 802.15.4 transmissions.

Spectral avoidance has featured in literature as a mitigation approach to interference, ensuring coexistence by avoiding collisions entirely~\cite{musaloiu2008minimising,xu2011muzi}. While effective in some situations, this method is ineffective in crowded circumstances, as free channels may become a scarcity. Liang et al.~\cite{liang2010surviving} describe how to bolster each packet's resilience to the effects of interference through embedding multiple packet headers and by employing forward error correction. An Automatic Repeat Request scheme for retransmitting partially damaged packets is explored by Hauer et al.~\cite{hauer2010mitigating}. By scanning the \ac{RSSI} at high frequency during packet reception, spikes from local interference are associated with corruptions in the packet. Our work is orthogonal to these aforementioned works, permitting parallel use to counter interference.

The benefits of a \ac{CACCA}  are explored by Tytgat et al.~\cite{tytgat2012avoiding}, followed by a  \ac{CACCA}  evaluation using a Software Defined Radio (see \cite{de2013coexistence}). In this existing work WiFi nodes are equipped with the ability to detect different network types while in our work we consider a \ac{CCA} extension of 802.15.4 devices. Our proposed \ac{DCCA} method requires no special hardware, operating on commodity sensor nodes.

In our previous work we have shown that \ac{DCCA}~\cite{king2014} can help in general to improve network performance.  However, in this previous work we have not detailed a mechanism such as \ac{P-DCCA} that can be used to implement \ac{DCCA}. 


