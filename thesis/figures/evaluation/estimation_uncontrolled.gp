set terminal postscript #png #pdf dashed enhanced font "Helvetica,14" lw 3
set output "estimation_uncontrolled.eps"
set multiplot layout 2,1

set ylabel "Duty Cycle D(p) in [%]"
set yrange [0:1.5]

set grid

unset xtics #set xtics rotate
unset key
set datafile separator ","
plot 'estimation_uncontrolled_a.csv' using 2:xtic(1) title "Measured" lw 2 with lines, \
     'estimation_uncontrolled_a.csv' using 3:xtic(1) title "Monte Carlo" lw 2 with lines
set key

plot 'estimation_uncontrolled_b.csv' using 2:xtic(1) title "Measured" lw 2 with lines, \
     'estimation_uncontrolled_b.csv' using 3:xtic(1) title "Monte Carlo" lw 2 with lines


