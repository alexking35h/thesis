\chapter{Introduction}
\label{chap:introduction}

In the 60 years following the development of the Integrated Circuit (IC), programmable computers have become widespread in many devices, from supercomputers to smartphones, and many applications, from military to healthcare.
At the smaller, more discrete scale, advances in manufacturing techniques have allowed hardware to be built with a smaller footprint.
Likewise, more efficient electronic design means devices can be built with lesser power requirements, permitting the use of more flexible power sources, including battery power, or energy harvesting.
These trends have carved a path for Embedded Computer Systems, which share many similarities with larger computers, but are typically smaller, more portable, and less powerful.
Embedded systems are now found in mobile phones, MP3 players, engine control units, game controllers, and many other applications that require computation but have limited cost, space, or power requirements.
In conjunction, wireless communication techniques have advanced 
in terms of range, data rate, and reliability.
A number of physical and protocol standards are now available to meet 
numerous cost and functionality demands.

\acfp{WSN} are an example embedded system that has 
evolved over the past two decades in line with hardware advances and user demands.
These consist of a network of sensor nodes, deployed in an 
environment to monitor a particular phenomenon.
Each node is typically built around a \ac{MCU}.
incorporating the \ac{CPU}, memory, non-volatile storage, and communication peripherals.
The MCU interfaces with other on board devices, including wireless radio for 
node-to-node communication, power sources, and at least one sensor.
Only limited data processing, such as compression or data aggregation, is done within the network. 
Instead, nodes forward sensor readings through the network toward the sink, 
where they are made available for analysis.
Example uses of \acp{WSN} are bridge structural health monitoring \cite{kim2007health},
wildlife monitoring \cite{mainwaring2002wireless}, and volcano emission monitoring \cite{werner2006deploying}.
In all three cases, \acp{WSN} are preferred over more traditional sensors 
because of their smaller initial and operating costs, and in some cases 
because fewer human involvement reduces the risk of injury.

To allow flexible deployment, \ac{WSN} nodes are often powered by a small, onboard power source such as a battery.
Likewise, \ac{WSN} deployments may often be in environments where regular maintenance is difficult,
making battery replacement to sustain a network either practically or commercially unfeasible.
Therefore, the lifetime of the network is determined by the 
energy capacity, and energy consumption of each sensor node.
\ac{WSN} nodes may also employ energy harvesting techniques, such as solar panels.
In this case, the energy consumption of each node must meet a limited budget.

On typical \ac{WSN} hardware, the radio is the greatest source of energy consumption, 
even when not actively transmitting or receiving data.
Over the past decade, much research has focused on optimising the energy 
efficiency of \ac{WSN} \ac{MAC} protocols, by keeping the radio powered down as much as possible.
This conserves energy and extends the lifetime of the deployment.
Since nodes can only communicate when their radios are powered-on, these 
\ac{MAC} protocols must facilitate a synchronisation mechanism to send data.
To meet energy efficiency requirements while still able to transmit and receive, 
\ac{WSN} nodes use Clear Channel Assessment (CCA) for both functions in the \ac{MAC} protocol.
\ac{CCA} provides an indicator of the channel as busy or free, and is used 
by receiving nodes to detect incoming packets; and by transmitting nodes to avoid collisions.

%%% paragraphs above have discussed what \ac{WSN}, embedded systems are
%%% challenges, etc. Here, present this thesis.

In ideal conditions, these approaches allow nodes to reach extremely low 
radio duty cycles, enabling long network lifetime without sacrificing communication requirements.
However, depending on the deployment environment, ideal conditions may 
not be possible due to other interference sources.
The 2.4$Ghz$ Industrial, Scientific and Medical (ISM) frequency band is a common choice amongst \ac{WSN}.
This is shared with a plethora of other wireless networks and devices, including IEEE 
802.11 (WiFi), 802.15.4 (ZigBee), and 802.15.2 (Bluetooth) standards.
As well, other electronic devices - such as electric motors and microwave ovens - 
emit interference on this band as a by-product of their operation.

For most wireless networks, interference from other devices may cause packet decoding errors, which reduce link quality and throughput.
This issue is referred to as \acf{CTI}, and in \ac{WSN} \ac{MAC} protocols, is further exacerbated to also include reduction in energy-efficiency of the node.
This stems from the use of \ac{CCA}, which in most radio hardware is unable to discern \ac{WSN} traffic from other interference.
Consequently, when transmitting, \ac{WSN} nodes are unable to adequately respond to channel contention from other interferers.
This affects packet loss and the reliability of \ac{WSN} links in these conditions.
Under light interference, this may only negligibly impact \ac{WSN} operation;
under heavy interference, however, communication between nodes may be prevented entirely.

Similarly, under interference conditions the use of \ac{CCA} during the receive process of \ac{WSN} \ac{MAC} protocols
leads to false wakeups - where incoming traffic is wrongly inferred due to other channel activity.
Energy is then wasted whilst the radio is kept powered on, 
increasing energy consumption beyond that expected in an ideal environment.
Therefore, when installing a \ac{WSN} in an environment subject to \ac{CTI}, it is difficult to predict beforehand the energy consumption, and lifetime of the network.
In this thesis, a methodology to estimate energy consumption is presented to solve this,
based on \ac{MAC} protocol behaviour and environmental interference measurements.

To offset the effects of interference on \ac{WSN} \ac{MAC} protocols,
\ac{DCCA} is presented in this thesis as an extension to the standard IEEE 802.15.4 \ac{CCA}.
\ac{DCCA} is able to indicate the type of interference, as well as the state of the channel.
\acf{P-DCCA} is described as an implementation of \ac{DCCA} feasible on current \ac{WSN} hardware.
\ac{P-DCCA} is evaluated in a common \ac{WSN} \ac{MAC} protocol, and shown 
to mitigate both energy inefficiency and packet loss in interference conditions.

Fuelled by falling manufacturing costs and advances in hardware design,
commercial and academic paradigms have spawned from \ac{WSN} research to meet new demands.
This includes Vehicular Sensor Networks (VSN), and Home Automation Networks (HAN),
which share similar processing and communication requirements with \ac{WSN}.
The Internet of Things (IoT) proposes collection and exchange of data from ubiquitous 
embedded sources, across an existing infrastructure: typically, the internet.
This is enabled by the advent of IPv6, whose $2^{128}$ address 
space may allow each IoT device to be uniquely identifiable.
In this thesis, the focus is specifically \ac{WSN}, however the findings presented
are applicable to these other domains also.

\section{Problem Statement}

% Interference can originate from a variety of sources in the 2.4G band.
% This can have an adverse affect on 802.15.4 communication.
% Exaccerbated for the case of WSN, in two regards.
% 1. link performance
% 2. energy efficiency.
%   + Because we're spending more energy to meet performance requirements
%   + How the radio detects a busy channel.
%
% Previous literature has shown that CCA is not good for this.
% Beneficial to consider an extension to this mechanism
% Fix the 'one-size-fits-all'
% better responses.
%
% Need to predict lifetimes.
% Why? What use is this to WSN designers?
% Difficult due to CTI, for some MAC protocols.
% Mechanism needed to predict energy use and lifetime in CTI environments.

Interference in the 2.4$Ghz$ frequency domain can originate from co-located WiFi, Bluetooth, and microwave ovens, among other devices.
In each case, coexistence with other technologies is often overlooked, resulting in sub-optimal performance for affected networks.
This is the case for IEEE 802.15.4, whose link quality is degraded under \ac{CTI} conditions.
For \ac{WSN} \ac{MAC} protocols based on the IEEE 802.15.4 standard, this issue is further exacerbated in two regards:

\begin{itemize}
    \item \textbf{Link Performance} Collisions between \ac{WSN} and other interferers result in packet loss, which restricts the capacity of the link.
                                    This issue is made worse by the design of \ac{WSN} \ac{MAC} protocols, which are more susceptible to \ac{CCA}-collisions than other IEEE 802.15.4 devices.
                                    Link capacity is further penalised due to infrequent sender/receiver synchronisation opportunities in these \ac{MAC} protocols.

    \item \textbf{Energy efficiency} In interference environments, efficient use of the radio may cease due to two reasons.
                                     Firstly, in order to mitigate packet loss, retransmission and error correction mechanisms may be employed; these incur an additional energy cost.
                                     Secondly, interference is mistaken for incoming data in the \ac{MAC} protocol wakeup sequence.
                                     This leads to the radio being left powered on, which wastes energy.
\end{itemize}

In both cases, previous literature has shown that a common cause is the ambiguous \ac{CCA} mechanism in 
IEEE 802.15.4 transceivers, that is used by \ac{WSN} \ac{MAC} protocols.
It would be beneficial to extend this mechanism, with the ability to discern the nature of a signal source.
This would enable per-interferer response policies, as an improvement to the one-size-fits-all response currently implemented.

Further, reliable prediction of network lifetime is a requirement not possible in environments subject to \ac{CTI}, due to the second point above.
A mechanism to accurately predict node energy usage and node lifetime, which accounts for environmental interference, would be beneficial.

\section{Contributions}

It is the aim of this thesis to describe a method to extend the standard \ac{CCA}, used in \ac{WSN} \ac{MAC} protocols for sending and receiving, 
with the ability to discern interference types.
This will enable more reactive interference mechanisms.
As well, describing methods to enable reliable and precise predictions of node energy use in busy environments is the second aim of this thesis.
Specifically, this work is broken into two core contributions:

\begin{itemize}

\item Methods for estimating \ac{WSN} \ac{MAC} protocol duty cycle for a node, based on the environmental interference, are presented. 
      This can be used to gauge node energy use and estimate network lifetime. 
      Two methods are described and evaluated based on one typical \ac{WSN} \ac{MAC} protocol, ContikiMAC.

 \item An extension to the IEEE 802.15.4 standard \acf{CCA} mechanism: \acf{DCCA}, which can differentiate between sources of interference, as well as detecting the current occupation of the channel.
       \acf{P-DCCA} is an implementation approach to \ac{DCCA} that is pursued in this thesis.

 \end{itemize}

\section{Related Publications}

Three peer reviewed publications have resulted from the presented work, in addition to another publication that is awaiting review.
Each publication is listed below, including an explanation of how it relates to the work in this thesis.

\begin{itemize}
\item Alex King, James Brown and Utz Roedig. DCCA: Differentiating Clear Channel Assessment for Improved 802.11/802.15.4 Coexistence. In Proceedings of the 3rd International Workshop on Internet of Things Communications and Technologies (IoT - CT 2014). This workshop paper introduced the concept of DCCA in sensor networks, without any specific implementation.
\item Alex King, James Brown, John Vidler and Utz Roedig: Estimating Node Lifetime in Interference Environments. In Proceedings of the 10th International Workshop on Practical Issues in Building Sensor Network Applications (IEEE SenseApp 2015). This workshop paper describes the work on estimating node lifetime in busy interference environments. This paper forms the basis of Chapter \ref{chap:estimating}.
\item Alex King, James Brown and Utz Roedig: Differentiating Clear Channel Assessment using Transmit Power Variation. This journal article has been submitted to the ACM Transactions on Sensor Networks (TOSN) journal. This paper forms the basis of Chapter \ref{chap:dcca}. This journal article is awaiting review.
\item Alex King, James Hadley and Utz Roedig: Dependability Competition: ContikiMAC with Differentiating Clear Channel Assessment. This entry into the 13th International Conference on Embedded Wireless Systems and Networks dependability competition incorporates \ac{P-DCCA} into the ContikiMAC protocol. 
In this event, \ac{WSN} \ac{MAC} protocols were pitted against one another under interference conditions. \ac{P-DCCA} came 6th out 11 overall, and scored the highest energy efficiency of all competitors.
\end{itemize}

\section{Thesis Layout}

The remainder of this thesis is divided into six chapters:

Chapter two presents the foundations of this thesis, beginning with typical \ac{WSN} hardware and software components.
This chapter then covers the IEEE 802.15.4 PHY and \ac{MAC} wireless standard, which is a common choice in \ac{WSN} design.
Finally, sources of interference in the 2.4$Ghz$ domain are described,
and how they affect \ac{WSN}.

Chapter three reviews the related work in the domain of IEEE 802.15.4 and \ac{WSN} \ac{CTI}.
This includes firstly measurement studies and theoretical models, which evaluate the effects of interference on \ac{WSN}.
Then, previous solutions in literature to mitigating \ac{CTI} are discussed and compared.

Chapter four presents a methodology for estimating energy consumption of a \ac{WSN} in an interference environment.
This includes a theoretical model and monte-carlo simulation of the \ac{WSN} radio behaviour, in known interference conditions.
By way of example, these techniques are demonstrated for a common \ac{WSN} \ac{MAC} protocol: ContikiMAC.

Chapter five describes an extension to the standard IEEE 802.15.4 \ac{CCA} mechanism: \ac{DCCA}.
Different implementation options of \ac{DCCA} are compared, based on detection accuracy and energy cost.
\ac{P-DCCA} is presented as an implementation option that 
is available on commodity hardware,
is capable of high accuracy, 
and is energy efficient.
\ac{P-DCCA} is implemented in a \ac{WSN} \ac{MAC} protocol, and evaluated in terms of link performance and energy efficiency.
The evaluation includes small scale, single-hop deployments, and large testbed deployments spanning multiple hops.

Finally, the thesis is concluded in chapter seven. 
The contributions of this work are reviewed in the context of the previous literature, and the scope for future work is considered.


