
# Height and width of all things!
h=3
w=5

# term type for all things!
t="epscairo"

### DCCA inconclusive outcome graph
reset
set terminal t size w,h
set output 'dcca_outcome.eps'
set xrange [20:120]
set ylabel "P-DCCA Inconclusive (%)"
set xlabel "Packet Size (Bytes)"
set grid
plot 'dcca_outcome.tsv' using 1:(100.0*$2) w l lc rgb 'red' lt 1 title "recorded",\
     700/(13+x) w l title "Model"

### True-positive accuracy
reset
set terminal t size w,h
set output 'tp-accuracy.eps'
set style data histogram
set style histogram errorbars lw 1
set style fill solid .4
set xtics offset 3,0
set xtics rotate .2
set yrange[0:1]
set key top outside horizontal
set grid
set ylabel "True Positive Accuracy"
unset key
plot 'tp-accuracy.tsv' using 2:($2-$3):($2+$3):xtic(1) lc rgb 'blue'

### False-positive accuracy.
reset
set terminal t size w,h
set output 'fp-accuracy.eps'
set style data histogram
set style histogram errorbars lw 1
set style fill solid .4
set xtics offset 1,0
set xtics rotate
set key top outside horizontal
set grid
set ylabel "False Positive Rate (%)"
set xlabel "Inteference Source"
set yrange[0:1]
plot 'fp-accuracy.tsv' \
using ($2):(($2)-$3):(($2)+$3):xtic(1) title 'ZiSense-1' lc rgb 'blue',\
'' using ($4):(($4)-$5):(($4)+$5) title 'ZiSense-2' lc rgb 'red',\
'' using ($6):(($6)-$7):(($6)+$7) title 'ZiSense-C4.5' lc rgb 'orange',\
'' using ($8):(($8)-$9):(($8)+$9) title 'P-DCCA' lc rgb 'green'

### Energy models.
set term epscairo size 5,3
set output 'energy.eps'

set ylabel "Idle listening time per channel check ({/Symbol m}s)
set xlabel "Channel Busy Duration ({/Symbol m}s)"

set key at 1400., 2500.

set grid

busy=.25

N_R = 8
T_st = 128
T_RSSI = 32
f = 8

T_A(t) = ( T_RSSI * N_R ) * ( t - ( N_R * T_RSSI ) ) / t
T_B(t) = (N_R - 1) * ( T_RSSI / t ) * ( T_RSSI ) * ( (N_R^2)-N_R ) / 2
T(t,p) = T_st + ( p * ( T_A( t ) + T_B( t ) ) )
T_ALL(t,p) = f * T(t,p)

# The model is only valid if t (which is the duration of the interference)
# is less than the total sampling duration - which is N_R * T_RSSI.
set xrange[ (N_R * T_RSSI) :1500]
set yrange[0:3000]

plot 2900 w l title "ZiSense" ls 1 lc rgb "black" lw 3 dt 1,\
T_ALL(x, 0.1) w l title "P=0.1" ls 1 lc rgb "black" lw 3 dt 2,\
T_ALL(x, 0.25) w l title "P=0.25" ls 1 lc rgb "black" lw 3 dt 3,\
T_ALL(x, 0.5) w l title "P=0.5" ls 1 lc rgb "black" lw 3 dt 4,\
(f * T_st) w l title "Plain CCA" ls 1 lc rgb "black" lw 3 dt 5


### PRR Model/distance.
reset
set term t size w,h
set output 'predicted_prr.eps'
set xlabel "Distance (m)"
set ylabel "Packet Reception Rate (%)"
set grid
set key top outside center horizontal
plot 'predicted_prr.tsv' using 1:2 title '0dBm' w l ls 1 lw 2 lc rgb 'blue',\
'' using 1:3 title '1dBm' w l ls 1 lc rgb 'green',\
'' using 1:4 title '2dBm' w l ls 1 lc rgb 'red',\
'' using 1:5 title '3dBm' w l ls 1 lc rgb 'grey',\
'' using 1:6 title '4dBm' w l ls 1 lc rgb 'purple',\
'' using 1:7 title '5dBm' w l ls 1 lw 2 lc rgb '#00008B',\
'' using 1:8 title '6dBm' w l ls 1 lc rgb 'orange',\
'' using 1:9 title '7dBm' w l ls 1 lc rgb 'black',\
'' using 1:10 title '8dBm' w l ls 1 lc rgb '#5F9EA0',\
'' using 1:11 title '9dBm' w l ls 1 lc rgb 'yellow',\
'range_prr.tsv' using 1:($2/2880) title 'DCCA (6dBm)' w l ls 1 lc rgb 'cyan',\
'range_prr.tsv' using 1:($3/2880) title 'Standard (0dBm)' w l ls 1 lc rgb 'grey'

### Predicted model of link distances
reset
set terminal t size w,h
set output 'predicted_distance.eps'
set grid
set ylabel "Predicted Link Distance (m)"
unset key
plot 'predicted_distance.tsv' using 1:2 w l

### ContikiMAC DCCA eval PRR
reset
set terminal t size w,h
set output 'cmac_prr.eps'
set yrange [0:0.8]
set xrange [37.5:225]
set ylabel "Packet Reception Rate (%)"
set xlabel "IEEE 802.11g interference (KB/s)"
set grid y
plot 'cmac_prr.tsv' using 1:2 w lines t "ContikiMAC P-DCCA" lw 3 lc rgb "red" , \
'' using 1:3 w lines t "ContikiMAC Standard" lw 3 lc rgb "blue" lt 1

### ContikiMAC DCCA eval radiotime.
reset
set terminal t size w,h
set output 'cmac_rtime.eps'
set yrange [0:80]
set xrange [37.5:225]
set ylabel "Radio on-time (ms)"
set xlabel "IEEE 802.11g interference (KB/s)"
set grid y
plot 'cmac_rtime.tsv' using 1:2 w lines t "ContikiMAC P-DCCA" lw 3 lc rgb "red" , \
'' using 1:3 w lines t "ContikiMAC Standard" lw 3 lc rgb "blue" lt 1

### ContikiMAC multiple-nodes
reset
set terminal t size w,h
set output 'multi_nodes.eps'
set key title "Num. TX." box
set yrange [20:400]
set ylabel "Average Throughput per node\n(Bytes/Sec)"
set xlabel "ContikiMAC-variation"
unset xlabel
set grid y
set border 3
set style data histograms
set style histogram cluster gap 1
set style fill solid 0.25 border -1
set boxwidth 0.75
set xtics nomirror rotate by -45
plot 'multi_nodes.tsv' using 2:xticlabel(1) t "1" lc rgb "red" lt 1, \
'' using 3 t "2" lc rgb "green" lt 1, \
'' using 4 t "4" lc rgb "blue" lt 1, \
'' using 5 t "8" lc rgb "pink" lt 1

### Testbed Radio.
reset
set term t size w,h
set output 'testbed_energy.eps'
set grid
set xlabel 'Simulated-IEEE 802.11g Interference Rate (KB/s)'
set ylabel 'Radio-on time per packet received (s)'
plot 'testbed.tsv' every ::1 using ($1/1000):($3/1000) w l title "P-DCCA" lc rgb 'blue',\
     '' every ::1 using ($1/1000):($5/1000) w l title "Standard" lc rgb 'red'

### Testbed PRR
reset
set term t size w,h
set output 'testbed_prr.eps'
set grid
set xlabel 'Simulated-IEEE 802.11g Interference Rate (KB/s)'
set ylabel 'Packet Reception Rate (%)'
plot 'testbed.tsv' every ::1 using ($1/1000):2 w l title "P-DCCA" lc rgb 'blue',\
     '' every ::1 using ($1/1000):4 w l title "Standard" lc rgb 'red'

### 24 hour radio-on time
reset
set terminal t size w,h
set output '24hr_energy.eps'
set key horiz right top
set yrange [9:17]
set timefmt "%H:%M:%S"
set xdata time
set format x "%H:%M"
set xtics nomirror rotate by -60
set ylabel "Radio On Time (ms)"
set xlabel "Time"
set grid y
plot '24hr_energy.tsv' using 1:2 w lines t "P-DCCA" lw 3 lc rgb "red" , \
'' using 1:3 w lines t "Standard" lw 3 lc rgb "blue" lt 1

### 24 hour PRR
reset
set terminal t size w,h
set output '24hr_prr.eps'
set key horiz right top
set yrange [0:1.1]
set timefmt "%H:%M:%S"
set xdata time
set format x "%H:%M"
set xtics nomirror rotate by -60
set ylabel "Packet Reception Rate (%)"
set xlabel "Time"
set grid y
plot '24hr_prr.tsv' using 1:2 w lines t "P-DCCA" lw 3 lc rgb "red" , \
'' using 1:3 w lines t "Standard" lw 3 lc rgb "blue" lt 1

### Range Experiment ## 2880 packets were sent in each 3' run.
reset
set terminal t size w,h
set output 'range_prr.eps'
set key horiz right top
set yrange[0:1]
set xrange[0:]
set xtics 50
set ytics .1
set grid
set xlabel "Distance (m)"
set ylabel "Packet Reception Rate (%)"
plot 'range_prr.tsv' using 1:($2/2880) w l t "P-DCCA",\
     '' using 1:($3/2880) w l t "Standard"              ## Added on 11/9
