% Introduce the 803.15.4 protocol, and what it's designed for.

The IEEE 802.15.4 \acf{LR-WPAN} is a common choice in \ac{WSN}, and is therefore described in detail in this section.
The standard defines wireless communication 
physical (PHY) and medium access control (MAC) layers for low power, low data-rate, and low cost 
applications: making it suitable for WSN.
The communication range of these devices is typically at least 10$m$, described by the standard
as the Personal Communication Space (POS).
Suggested uses include Home Automation Systems (HAS), asset and inventory tracking, and industrial control.
Unlike WLANs, little or no infrastructure is required for operation, which prioritises 
ease-of-installation and low maintenance overhead.

% Now: discuss PHY options, single out 2.4Ghz DSSS/O-QPSK
The standard defines multiple PHY options, each describing the operating frequency, and type of modulation. Each has tradeoffs between communication speed, range, security, and energy consumption. 
Four frequencies, and subsequent channels, are supported based on the Industrial Scientific and Medical (ISB) bands: 780$Mhz$, 868$Mhz$, 915$Mhz$, and 2.4$Ghz$.
Due to the number of channels, energy efficiency features, communication range, and data rate,
the 2.4Ghz frequency is a common choice in devices using the LR-WPAN standard, including WSN.
Here, two modulation methods are available: Chip Spread Spectrum (CSS) and Offset-Quadrature Phase-Shift Keying (O-QPSK), which provide 1$Mb/s$ and 250$kb/s$ date rates respectively.
Most currently available 2.4$Ghz$ IEEE 802.15.4 transceivers are based on O-QPSK, 
which has 16 channels, each 2$Mhz$ wide, spaced 5$Mhz$ apart.
The work in this thesis was based on the 2.4Ghz O-QPSK PHY, although should be applicable to others 
as well.

% The PHY interface, which WSN MAC build on:
The functionality provided by the radio is common across all PHY layers, and provides
a common interface to the upper MAC and NET layers:

\begin{itemize}
	\item \textbf{\acf{RSSI}}
        an estimate of the received signal power on the channel, intended to be used by the network
        layer for the purposes of channel selection. 
        No attempt is made to identify or decode signals on the channel.
        The \ac{RSSI} result is calculated by averaging over eight symbol periods, which the 2.4$Ghz$ O-QPSK
        defines as 128$\mu s$.
        The standard requires that the span of \ac{RSSI} values be at least 10$dB$, and provide 
        a linear relation between the power received in decibels to the \ac{RSSI} value.

	\item \textbf{\acf{LQI}}
        a characterisation of the quality of a received packet, which may be based on \ac{RSSI}, \ac{SNR}, or
        received PN-correctness.
        \ac{LQI} is performed for each packet, and at least eight unique values should be supported.
        \ac{LQI} is intended for the purposes of link quality assessment.

	\item \textbf{\acf{CCA}}
        a determination of the current status of the channel as either \textit{busy} or \textit{clear}.
        This is intended for use in the \ac{CSMA}/CA mechanism, before transmitting data. The standard 
        requires that at least one of the following CCA methods is supported:
        \begin{itemize}
            \item \textit{Mode 1: \acf{ED}} \\
                If the energy detected on the channel exceeds the set threshold, which is configurable
                in software, a busy channel is reported.
            \item \textit{Mode 2: \acf{CS}} \\
                If a signal compliant with the standard, with the same modulation and spreading 
                characteristics, is detected, a busy channel is reported.
                The signal may be above or below the ED threshold. 
            \item \textit{Mode 3: \ac{ED} and \ac{CS}} \\
                A combination of both above methods, either of which may indicate a busy channel.   
            \item \textit{Mode 4: ALOHA} \\
                The channel is always reported idle.
        \end{itemize}
    \item \textbf{Channel frequency selection}:
        Transceivers must be able to change the current channel, within the allowable frequency
        for the particular PHY.
    \item \textbf{Data transmission and reception}
        Transceivers must be able to transmit and receive packets.
\end{itemize}

The 2.4Ghz PHY defines the PHY Protocol Data Unit (PPDU) as the underlying packet format, shown in 
figure \ref{fig:ppdu}, taken from \cite{cc2420spec}.
Each packet is prepended by a four-octet preamble, which is used for synchronising the receiver to
incoming data.
This is followed by the start of frame delimiter (SFD), a single octet with the value 0xE5, which
indicates the start of a packet.
The 7-bit frame length field describes the length of the PHY payload, and has a maximum value of 
127, thereby dictating the maximum packet length for this PHY.

\begin{figure}
    \centering
    \includegraphics[width=.7\textwidth]{figures/background/ppdu.jpg}
    \caption{PHY Protocol Data Unit}
    \label{fig:ppdu}
\end{figure}

% The 802.15.4 MAC protocol, according to the standard.
% Bottom line: not good enough.
The IEEE 802.15.4 MAC protocol supports two network topologies: star, and peer-to-peer.
The former has a single central node called a PAN coordinator, with which all other nodes
exclusively communicate, and is responsible for initiating and managing the network.
This node has higher energy demands than other nodes, so is suited to applications that can afford 
a universal base-station, such as HAS and health care.
Peer-to-peer topologies similarly have a PAN coordinator, but nodes are able to communicate amongst 
each other, therefore allowing multiple hops and more complex routing.

To cater for the diversity in supported devices, two device classes are defined in the standard:
Full Function Device (FFD) - which can be used in either topology, and Reduced Function Device (RFD) 
- which can only be used in star topologies.

The MAC protocol supports two methods of channel arbitration: beacon-enabled, and nonbeacon-enabled.
In beacon-enabled mode, the PAN coordinator broadcasts beacons which are used to synchronise attached
devices and identify the PAN.
The beacon is followed by the \acf{CAP}, and \acf{CFP}.
Nodes wishing to communicate then use slotted-\ac{CSMA}/CA within the \ac{CAP},
else may be assigned Guaranteed Time Slots (GTS) within the \ac{CFP} for uncontested channel access.
This is useful in applications with specific bandwidth, or predictable latency requirements.
Between beacons some quanta may be designated $inactive$, wherein nodes can sleep; this allows
for some degree of energy efficient operation.
In nonbeacon-enabled mode, nodes use unslotted \ac{CSMA}/CA to mediate channel access between nodes.
In both cases, acknowledgements are sent without \ac{CSMA}/CA.

Nodes have two forms of address: a 64-bit long address, and a 16-bit short address which is assigned
when the node joins the network.
Each PAN is identified by a 16-bit PAN identifier, the selection of which is beyond the scope of the
standard.

The standard defines the MAC Protocol Data Unit (MPDU), encapsulated within the PPDU, shown in 
figure \ref{fig:mpdu}, taken from \cite{cc2420spec}.
The 2-byte frame control field defines the packet type, addressing mode, and attributes required
for processing the packet.
The address fields may be either short (2-byte) or long (8-byte) as described above,
and so the minimum length of the MPDU Header is 11 bytes.
Each packet is appended with a Frame Control Field (FCS) to validate received packets. 
The FCS contains a 16-bit \ac{CRC}, calculated over the MPDU contents.

\begin{figure}
    \centering
    \includegraphics[width=.8\textwidth]{figures/background/mpdu.jpg}
    \caption{MAC Protocol Data Unit.}
    \label{fig:mpdu}
\end{figure}

There is a disparity between the goals of WSN applications and the IEEE 802.15.4 MAC protocol.
While the latter strives for interoperability via standardisation, this is less prominent in WSN
deployments, which favour flexibility, and have low likelihood of communicating beyond the WSN.
WSN applications typically have requirements exceeding the IEEE 802.15.4 MAC, such as:
low power consumption - which may demand more efficient radio usage,
low latency - which may demand a more refined \ac{TDMA} approach,
and complex routing protocols - to support multi-hop networking.
In light of this, most WSN designers opt instead for a WSN-specific MAC protocol, implemented in 
software, which is built on top of the IEEE 802.15.4 PHY interface.
These are discussed in the next section.

The IEEE 802.15.4 physical layer is adopted by a number of other wireless standards, each of which define a separate MAC protocol.
These include ZigBee \cite{alliance2009ieee} and WirelessHART \cite{wirelesshart}, both of which are similarly affected by \ac{CTI}.
Consequently, the literature discussed in Chapter \ref{chap:related} is
in most cases, unless stated otherwise, assumed interchangeable between these standards and \acs{WSN}.

In the remainder of this thesis, the IEEE 802.15.4 standard is referred to as 802.15.4 for brevity.
Unless stated otherwise, only the PHY layer is being referred to.

% Todo: coexistence with other technologies.
