Among coexistence solutions in literature, the three most prominent classifications discussed are frequency avoidance, detection, and resilience.
Each solution has merits and tradeoffs that suit particular circumstances; these must be accounted for when considering which to employ in a given environment.

Previous studies have shown that frequency avoidance offers the greatest improvement in link quality, as interfered channels can be avoided entirely allowing unimpeded WSN operation.
While reducing packet loss, this will also reduce false wakeups and improve energy efficiency - although the latter has not been evaluated in practice.
Frequency avoidance requires that a minimum number of channels are not subject to interference at any instant.
While previous studies of \acp{BAN} and HANs support this assumption \cite{hauer2009experimental,sha2011multi}, 
the emergence of new, wide-bandwidth, standards may challenge this (such as 802.11n, which uses 40$Mhz$-wide channels).
Likewise, the channel selection and synchronisation protocols require reliable communication which, on an already severely affected channel, may not be possible.
Therefore, frequency avoidance solutions should ideally be complemented by other approaches, either resilience, or detection.

Resilience solutions increase the likelihood of WSN packets being received in the presence of interference. 
This includes post-collision recovery approaches, such as ARQ (standard amongst WSN MAC protocols) and PPR.
Alternatively, pre-collision redundancy embed sufficient information in packets for them to be recoverable even after collisions with interference.

Detection approaches facilitate greater sensitivity of wireless devices to heterogeneous interference, and may focus on either WSN-, or interferer-detection improvement.
In the case of 802.11 - less sensitive and more destructive to other networks - interferer-detection provides the greatest link performance for 802.15.4.
However, this incurs the cost of complexity of such solutions.
Conversely, WSN-detection approaches allow for more selective contention policies in the WSN MAC.
While not able to mitigate collisions with higher-powered interferers - such as WiFi - packet loss due to transmit FIFO overflow can be reduced.
WSN-detection is also able to minimise energy consumption of LPL MAC protocols in interference environments, by reducing false wakeups.
These approaches typically do not require any special radio hardware and can be implemented on most WSN platforms.

Previous WSN-detection approaches have tuned the CCA threshold to meet energy consumption and packet delivery rate goals \cite{sha2013energy,tang2013interference}.
These approaches suffice in environments where signal strength alone is sufficient to differentiate WSN traffic from interference.
In more dynamic environments however, this is not the case \cite{zheng2014zisense}.
Zheng et al have proposed ZiSense, which detects 802.15.4 transmissions based on temporal RSS features, irrespective of signal strength.
ZiSense is shown to reduce false wakeups in CTI environments, however the idle listening - hence minimum energy consumption - is significantly increased.
ZiSense is also not able to mitigate link quality degradation.
In Chapter \ref{chap:dcca}, \ac{DCCA} is presented as a similar WSN-detection solution that 
mitigates packet loss; ensures energy efficiency; does not significantly increase the cost of idle listening.

