WSN-side detection solutions mitigate CTI by affording the presence of heterogeneous interference in the design of those protocols.
These solutions are designed to reduce either packet loss or false wakeups - the latter is only applicable to LPL MAC protocols.
For example, packet loss may be reduced if WSN nodes are able to handle collisions from WiFi interference, or within the same network, differently \cite{tang2013interference}.
Likewise, LPL protocols may be made more efficient under interference if the rendezvous mechanism were able to differentiate WSN traffic from interference, waking up only for the former \cite{sha2013energy,zheng2014zisense}.

Yuan et al and Tang et al both present interference mitigation mechanisms based on CCA threshold adaptation \cite{yuan2010adaptive,tang2013study}, which aim to reduce ZigBee packet loss in interference environment.
Under Wifi interference, Tang et al show that the CCA threshold of ZigBee devices heavily influences the rate, and cause, of packet loss.
Increasing the threshold reduces the rate of CCA collisions, and subsequently is able to achieve higher packet delivery.
However, if the threshold is set too high, collisions with WiFi interference may increase. Therefore, a equilibrium must be found.
Both approaches adaptively change the CCA threshold: Yuan et al compare the rate of CCA failures to a preset threshold, while Tang et al measure the rate of packet buffer overflows.
The former approach is evaluated in a network simulator comprising a WiFi network and sixteen ZigBee nodes; the latter is evaluated on a small-scale testbed.
Both evaluations show that packet loss, caused by CCA failure and collisions with interference, is reduced.
However, neither evaluates the affect of this CCA threshold adaption on communication within the WSN, where multiple nodes may be vying for channel access simultaneously.
These methods are based on ZigBee \ac{CSMA}, which has only one CCA check before transmitting.
This is vastly different to WSN MAC protocols which have multiple packet strobes per transmission, each preceded by CCA.

Tang et al present Interference Aware Adaptive Clear Channel Assessment (IAACCA), which more proactively contends for channel access by replacing the standard CCA \cite{tang2013interference}.
Instead of a single CCA check, the channel is sampled continuously until found to be clear.
If a timeout is reached, the packet is dropped.
Otherwise $n$ further CCA checks are taken - where $n$ is random to avoid collisions with other ZigBee devices - after which, the packet is transmitted.
Compared to the standard \ac{CSMA}/CCA mechanism, which more conservatively backs off on finding a busy channel, IAACCA is shown to reduce packet loss under WiFi interference.
IAACCA is evaluated only in a small network, where any interference is assured to have originated outside the network.
In a heterogeneous environment however, a collision policy informed by the interferer source would be more beneficial: able to enact the most suitable response deterministically.
Nonetheless, this approach demonstrates that the standard \ac{CSMA}/CCA mechanism, and it's sweeping CCA collision response, is inadequate in CTI environments (particularly under WiFi interference).
\ac{DCCA} is evaluated in this context in Chapter \ref{chap:dcca}, and found to reduce packet loss by employing such a policy.

To mitigate false wakeups incurred by LPL MAC protocols, Sha et al present Adaptive Energy Detection Protocol (AEDP) \cite{sha2013energy}.
In 802.15.4 compliant radios the Energy-CCA threshold, above which any signal indicates a busy channel, is configurable in software.
The authors find that this parameter affects the rate of false wakeups: higher thresholds are less liable to detect other interference and cause false wakeups;
it must be low enough, however, to detect valid incoming packets.
The CCA threshold must therefore be raised as much as possible, but not so much as to miss valid packets.
In order to adapt to different environments, AEDP seeks this optimisation at runtime.

Each node tracks two variables within a sliding, 15 minute window: \textit{1)} ETX from incoming packets ($ETX$), \textit{2)} false wakeup rate ($WR$).
For both variables, upper thresholds are set to guide the refinement algorithm:
if $ETX > ETX_{threshold}$, the CCA threshold is quickly reduced;
if $WR > WR_{threshold}$, the CCA threshold is gradually increased.
Over time, AEDP will find the most efficient CCA threshold which balances ETX and false wakeups.
AEDP must be able to adapt to changing network topology, which may require lowering the CCA threshold to detect new neighbours.
This is achieved by periodically setting the CCA threshold to its lowest value.
Since this is neither energy-efficient or highly reactive, AEDP is better suited to stable network topologies.

AEDP is implemented in BoX-MAC-2 \cite{moss2008box}, using the default wakeup interval of two seconds.
The node duty cycle is used to measure energy efficiency with, and without AEDP - using the default CCA threshold.
In a quiet environment, false wakeups are rare, and hence AEDP brings no changes in duty cycle.
Alongside an 802.11n network generating interference, AEDP reduces duty cycle by 47.3\%.
Likewise, in a residential deployment, AEDP is able to reduce the duty cycle by 45.5\%.
AEDP is tested in a multi-hop network on a 55-node deployment in a residential deployment exposed to interference, each node sending a packet to the sink every five minutes.
During the 24-hour experiment, the duty cycle is again reduced to 35.44\% using AEDP.
Interestingly, AEDP reduces the average ETX by 11.26\%, due to unintentionally pruning unstable links which are below the CCA threshold.
Since AEDP cannot reduce the CCA threshold below the lowest neighbours RSS, this is shown to determine the effectiveness of AEDP.
Thus, the authors note that AEDP may offer no benefit in sparse deployments.

%%%%%%%%%%%%%%%%%%%%

 % Zheng et al present ZiSense, a WSN MAC protocol enhancement to improve transmitter/receiver rendezvous mechanisms in the presence of interference.
 % The authors supplement two MAC protocols with ZiSense. 
 % Firstly, LPL \cite{moss2007low} is able to distinguish 802.15.4 signals from other interference, hence avoiding false wakeups and improving energy inefficiency.
 % Secondly, probe packets corrupted by interference in LPP \cite{dutta2010design}  can still be detected, improving packet reception performance under such conditions.
 % Despite having different motivations to these other works, ZiSense is comparable as an active-sampling classification mechanism.
 % 
 % From the RSS trace, ZiSense identifies individual signals, and for each constructs a feature set consisting of \textbf{1) On-air time:} the duration of each signal, \textbf{2) Peak-to-Average-Power Ratio (PAPR)}: a measure of the shape of a signal, \textbf{3) Minimum Packet Interval (MPI):} the minimum interval between successive transmissions, and \textbf{Under Noise Floor (UNF):} a binary indication of RSS dips beneath the noise floor, characteristic of mcirowave oven interference.
 % (1) and (3) can be correlated for each interference source, for example the maximum value of (1) for an 802.11g device is 542$\mu s$.
 % The authors found that PAPR differs depending on the modulation technique: WiFi has a higher value than Bluetooth/ZigBee due to different modulation techniques.
 % 
 % 802.11b interference, which is not included in the original evaluation of ZiSense,
 % is found later in Chapter \ref{chap:dcca} to cause a high rate of false positives in ZiSense - being miss-classified as 802.15.4 packets.
 % This is due to the DSSS modulation used in 802.11b - as opposed to OFDM in 802.11g, having  closer measurable PAPR and timing characteristics to 802.15.4.
 % While almost obsolete, rate selection algorithms in many 802.11 stations may fall back to this older revision under poor channel conditions, 
 % including those caused by interference from other non-802.11 devices.
 % This was found to be the case in Chapter \ref{chap:impact}.
 % Further, the high degree of accuracy under 802.11g/n, microwave, and Bluetooth interference as reported in \cite{zheng2014zisense} could not be replicated in Chapter \ref{chap:dcca};
 % however, a different method of measuring accuracy, and different testing conditions, were used.

%%%%%%%%%%%%%%%%%%%%%%%


Zheng et al point out that many deployments are dominated by such Intermediate Quality (IQ) links, with RSS indiscernible from other interference sources.
Examining a dataset which includes link RSS of a large testbed, 90\% of links are shown to be below -66$dBm$.
Following this, Zheng et al describe ZiSense - a mechanism to reduce false wakeups not reliant on signal strength \cite{zheng2014zisense}.
ZiSense is similar to other interference classification mechanisms \cite{zhou2010zifi,ansari2011wispot}, insofar as an RSSI trace is searched for known spectral and temporal features.
However, ZiSense is intended only to detect the presence of 802.15.4, in order to bolster the rendezvous mechanism.

Instead of a single CCA check, ZiSense samples the RSSI register at high frequency over a sample period.
From the RSS trace, ZiSense identifies individual signals, and for each constructs a feature set consisting of 1) On-air time: the duration of each signal, 2) Peak-to-Average-Power Ratio (PAPR): a measure of the shape of a signal, 3) Minimum Packet Interval (MPI): the minimum interval between successive transmissions, and Under Noise Floor (UNF): a binary indication of RSS dips beneath the noise floor, characteristic of microwave oven interference.
(1) and (3) can be correlated for each interference source, for example the maximum value of (1) for an 802.11g device is 542$\mu s$.
The authors found that PAPR differs depending on the modulation technique: WiFi has a higher value than Bluetooth/ZigBee due to different modulation techniques.
Each feature set is then passed to an identification algorithm, three of which are described in \cite{zheng2014zisense}.

The false wakeup rate of ZiSense is first evaluated under controlled conditions, compared to B-MAC and AEDP under WiFi, Bluetooth, and microwave oven interference.
B-MAC, which uses a fixed CCA threshold, performs the worst, approaching 100\% false wakeup rate for short distances.
AEDP reduces this only when the interferer is further than 4$m$ away, or when the 802.15.4 links have RSS greater than -65$dBm$.
This is expected, since high interferer, or low 802.15.4, signal strength render both indistinguishable.
On the other hand, ZiSense consistently achieves fewer false wakeups independent of link RSS or interferer distance than AEDP or B-MAC.
ZiSense is then evaluated in a large-scale testbed of 41 nodes in an office environment.
Each node in the multi-hop network forwards one packet every five minutes to the sink, and the channel check rate is 2$Hz$.
ZiSense is shown to reduce the duty cycle from 3.74\% (B-MAC) and 4.14\% (AEDP) to 2.46\%.
The ZiSense implementation used in \cite{zheng2014zisense} is 2800$\mu s$ - 90 samples. This is a significant departure from a single CCA check (128$\mu s$), and adds significantly to the cost of idle listening.

\ac{P-DCCA} is presented in Chapter \ref{chap:dcca}.
As with ZiSense, \ac{P-DCCA} searches an RSSI trace for prior known temporal and spectral features, in order to differentiate 802.15.4 from other interference.
However, to shorten the timescale on which such features can be detected, \ac{P-DCCA} does not rely on inherent 802.15.4 characteristics.
Instead, \ac{P-DCCA} nodes embed additional information in the amplitude of outgoing transmissions - by varying the output transmission power, effectively creating an orthogonal channel.
Detection of this unique feature in an RSSI trace indicates an \ac{P-DCCA} transmission.
The duration of a \ac{P-DCCA} check is variable between 128$\mu s$ and 256$\mu s$, more closely approximating that of a standard CCA check.
In Section \ref{sec:dcca__energy}, \ac{P-DCCA} is shown to significantly reduce the cost of idle listening compared to ZiSense.

As with these approaches, \ac{P-DCCA} is designed to mitigate the false wakeup problem in low power MAC protocols, however \ac{P-DCCA} is also designed to alleviate channel contention and packet loss in interference environments.
\ac{P-DCCA} and ZiSense search for signal features discernible from interference, that are invariable with distance or signal strength.
Consequently, in either a diverse link environment, or one subject to link dynamicity, both would expect a lower false positive rate compared to AEDP.
However the underlying CCA in AEDP relies on a simple RSSI threshold, is inherently less prone to missing valid signals, and therefore may suffer fewer false-negatives.
The drawback of \ac{P-DCCA} is that the power variation used to identify transmissions reduces the total SNR of each packet, and hence may reduce the communicable link range.
As with AEDP, \ac{P-DCCA} may therefore be less suitable in sparse deployments, although such links are more heavily penalised in \ac{P-DCCA}.

\ac{P-DCCA} is evaluated against ZiSense in Chapter \ref{chap:dcca}, measuring true-positive and true-negative accuracy.
While the high accuracy recorded in \cite{zheng2014zisense} could not be replicated, \ac{P-DCCA} was shown nonetheless to achieve comparable or greater accuracy.
The authors note that interference sources other than those considered in \cite{zheng2014zisense} may be less discernible from 802.15.4, yielding lower true-positive accuracy.
In Section \ref{sec:pdcca_accuracy}, this is found to be the case with 802.11b interference, under which ZiSense is highly susceptible to interference.
While now mostly obsolete, there are circumstances where 802.11b interference may be encountered, where ZiSense would perform poorly.
This is due to the modulation and timing similarities between 802.11b and 802.15.4.

Both \ac{P-DCCA} and ZiSense rely on preset thresholds to differentiate unknown interference from incoming traffic.
These face a similar tradeoff to the CCA threshold, and therefore a mechanism based on AEDP may be used to fine tune such parameters at runtime - in order to optimise the false-negative/positive tradeoff.

Detection has been proven an efficient solution to reduce packet loss and maintain energy efficiency in CTI environments.
As the most prolific example of CTI, studies here have mostly focused on 802.11 and 802.15.4 coexistence.
Improving the ability of WiFi to detect other networks is shown to yield the greatest improvement in 802.15.4 packet delivery.
This is because WiFi devices are unlikely to detect other CTI, and whose transmissions are most destructive.
Solutions here based on modified radio hardware - to improve either sensitivity of, or visibility too, WiFi CCA, achieve the greatest improvement in 802.15.4 link performance.
However, the cost and energy consumption of these additional components precludes many WSN applications.

In WSN-side detection solutions, packet loss is mitigated by affording more selective channel arbitration.
Collisions with other WSN traffic are avoided, while more assertively vying for channel access amongst interference.
WSN-side detection solutions are also able to ensure energy efficiency in CTI environments, by avoiding false wakeups.
In Chapter \ref{chap:dcca}, \ac{P-DCCA} is presented as a WSN-side detection solution.
\ac{P-DCCA} has greater accuracy compared to existing approaches with minimal energy overhead, and is shown to both mitigate packet loss, and preserve energy efficiency.















