Frequency avoidance mechanisms mitigate the effects of interference by distancing the WSN from interference sources in the frequency domain.
These approaches require that at least a minimum number of unaffected 802.15.4 channels are available at any instant,
an assertion supported by previous interference environment studies (\cite{hauer2009experimental,sha2011multi}).
These approaches can be network-centric - all nodes switching to a new channel, or link centric - applying channel change to affected
links only.

In general, these mechanisms share the same components:
\begin{itemize}
    \item \textbf{Detection}\\
          Detection of deteriorating link quality on a given channel, prompting channel change.
          To this end, nodes may periodically check channel conditions, else detect deteriorated conditions 
          during the course of operation.
    \item \textbf{Selection}\\
          Selecting a new channel to use for communication.
          This may be an empirical process of scanning available channels, or a random selection.
    \item \textbf{Synchronisation}\\
          Informing other nodes in the network of the new operating channel.
          In methods that use a common channel for all nodes, this will end with all nodes performing a channel switch.
    \item \textbf{Communication}\\
          Communicating on the new channel. In networks without a common channel, this will involve some kind of switching mechanism before each transmission.
\end{itemize}

\textit{Detection} is practically interchangeable between these approaches, and may be launched as an integral part of the mechanism, or by a higher layer application.
Likewise, the \textit{selection} process is interchangeable: \cite{musaloiu2008minimising} and \cite{xu2011muzi} both use PHY-level measurements based on \ac{RSSI}, while
\cite{iyer2011chrysso} and \cite{sha2011arch} pick the next channel randomly or sequentially with no input.

The synchronisation method is unique in each case, and must be designed to handle:
\begin{itemize}
    \item Reliable communication of new channel to all affected neighbours.
    \item A bootstrap procedure, for a node to join an existing network
    \item A fallback procedure, in case node synchronisation cannot be achieved on the current channel. For example, due to particularly bad interference.
\end{itemize}

% Compare:
% musaloiu first. - bad because it's across the network.
% muzi second - both use PHY metrics, but muzi is per-link
% Then chrysso and arch - both use NET metrics, and are per-link.

% Minizing the effect of wifi interference in 802.15.4 wireless sensor networks. Musaloiu & Terzis, 2008
 % Paper has two main contributions:
 %  - measurement study
 %     + indoor, outdoor. 
 %     + 802.11bg max traffic
 %     + Measure 802.15.4 packet loss, and RSSI
 %     + Uses ZigBee, CSMA
 %     + Find: upto 58% PL, RSSI and PL correlated
 %     + *802.11 causes worse interference than 802.15.4, cross- worse than mutual-interference*
 %  - Mitigation approach, via channel hop.
 %  - Here, before tx upsteam, all nodes sample channel.
 %  - forward RSSI to sink, which aggregates resuts, chooses best channel
 %  - forward this decision to all nodes on a path.
 %  - Findings: reduce PLR from 58% to 1%.
 %  - Crits:
 %     + single channel
 %     + zigbee, not wsn
 %     + survive really really bad conditions?
 %     + energy expensive, sample too frequently?
 %     + Method does not describe what happens if network becomes fragmentated due to different frequencies, not on path.

In \cite{musaloiu2008minimising}, Musaloiu and Terzis present a frequency avoidance mechanism for multi-hop sensor networks in the context of an environmental monitoring deployment.
Atop the ZigBee MAC and routing protocol, periodic data requests are forwarded to a specific node from the base station.
On each request, nodes en route conduct scans across all channels to assess their conditions;
this data is relayed back to the base station which selects the least-busy channel from this aggregate information.
A channel-change request is then sent from the base-station, after which the sensor readings are transmitted.

The authors evaluate packet loss in a multi-hop ZigBee network of four nodes, alongside an 802.11b network which generates controlled interference.
Compared to the worst-case single channel - which coincides with the WiFi network, packet loss is reduced from 58\% to less than 1\%.
This method adds significant control traffic overhead, increasing latency and energy consumption even in instances where no channel change is required.
Since the number of control packet increases with the path distance, this centralised approach is unsuitable for large networks.
Likewise, this method uses a single channel for all links on a given path, and therefore may suffer in deployments with disjoint interference regions which require more flexible frequency avoidance.
   
% Also, this control traffic compounds vulnerability to adverse channel conditions on the original channel:
% if either channel scanning information or channel change instruction cannot be sent, nodes cannot coordinate a new channel.

 % Muzi: Multi-channel zigbee networks for avoiding wifi interference. Xu et al, 2011
 %  - Similar to Musaloiu, paper consists of 1) a channel assessment mechanism, and 2) a frequency avoidance solution.
 % 
 %  Channel Assessment mechanism
 %  ----------------------------
 %  - Sample channel 10 times, RSSI. 
 %  - From this, build a <density,intensity> pair. Density is the time parameter of interference, how much?   
 %  - Insentity is the RSSI/SINR component, how bad?
 %  - This is justified that wifi interference is not a constant, and comes and goes.
 %  
 %  Frequency avoidance
 %  ------------------
 %  - Periodically sample, if it's changed much, initiate search for a new channel.
 %  - Find the least busy channel, that may potentially be shared with other neighbors
 %  - Once picked, broadcast this on all channels that neighbors are using.
 %  - (Maintain a <neighbor,channel> table), switch on demand to Tx packet.
 % 
 %  Crits:
 %  - Mechanism is potentially more vulnerable to interference on a single channel, for control packets.
 %  - Channel change is expensive: comm. on every other channel.
 %  - Channel choice is primitive: weigh up the least busy channel against the most popular channel?
 %  - No mention of what happens if new node joins/resets?
 %  - Small, unrealistic evaluation (4 nodes)
 %  - evaluation does not use mutliple children.

Similar to \cite{musaloiu2008minimising}, Muzi is a frequency avoidance mechanism which uses PHY measurements, sought from \ac{RSSI}, to infer channel quality \cite{xu2011muzi} and inform detection and selection.
However, channel selection is per-link, and control is decentralised to each node, thereby improving scalability and reducing overhead.
Each node maintains a table of \textit{$<$node,channel$>$} pairs for each neighbour, updated whenever a channel change notification is received.
To communicate with another node, this table is searched for the correct channel.
Nodes periodically measure interference on the current channel; therefore, unlike \cite{musaloiu2008minimising} channel scan and selection process is initiated only if the set threshold is exceeded.
Following the channel scan, the least-busy channel is selected, weighted in favour of those channels in use by nearby nodes.
Notification of the new channel is then unicast individually to neighbour nodes, after which the new channel is switched to.

Muzi is evaluated on a small 4-node testbed in conditions favouring a link-centric frequency-avoidance strategy: two disjoint WiFi stations, covering collectively all available 802.15.4 channels.
Two configurations based on fixed channels, which fall under the 802.11 networks, experience 3.5\% and 28\% packet reception rate; this increases to 94\% with Muzi.
In the implementation described, the time between the first channel change notification, and the channel change itself is determined by the number of neighbours;
Therefore, channel change is not atomic, and may lead to packet loss in networks where nodes have many children. Such a case is not represented by the small evaluation employed.
As with \cite{musaloiu2008minimising}, no fallback option is discussed if control packets cannot be communicated.

 % Arch: practical channel hopping for reliable home-area sensor networks. Sha et al, 2011.
 % Based on their previous work (which is an interference coexistence study paper)
 % Channel assessment:
 %  - Unlike the other works so far, this bases channel decisions on ETX, therefore is slower and less reactive.
 %  - Indeed, the sliding ETX window is 15 minutes?
 %  - When ETX for a given link drops below a threshold, receiver switches to another random channel that is probablistically far from the current channel
 %  - (Based on their prior work that found bad channels to be next to each other)
 %  - Then try this one, blacklist bad channels.
 % 
 % Notification:
 %  - Two options described: unicast to all neighbors, or broadcast.
 %  - For unicast, reduce overhead by piggybacking ACKs.
 %  - (This is slower)
 % 
 % Good:
 %  - Explicit mechanism to deal with coordination in poor channel conditions (just switch back to default channel - good/bad?)
 %  - Thorough evaluation, big testbed in realistic setting (there is no wifi-rate controlled variable, all realistic
 % 
 % Bad:
 %  - Slow to change, relies on ETX.
 %  - Evaluation is limited to a single environment, single testbed.
 %  - Unlike Muzi, doesn't try to group nodes on same channel if possible.
 %  - When too few channels are available (because of blacklisting), the blacklist is cleared. This is bad in really busy RF environments?
 %  - Based on HAN, applicable to other WSNs?

ARCH \cite{sha2011arch} and Chrysso \cite{iyer2011chrysso} use higher-level metrics to measure channel performance, namely ETX and recent backoff congestion.
These may more accurately represent link performance for a given channel, but inherently take longer to measure and are therefore less reactive to changing conditions.
In both cases, channel quality can only be observed after switching to it, and thus channel selection is either random (ARCH) or sequential (Chrysso).
Adjacent channels - which may be affected by the same interferer spanning multiple 802.15.4 channels - are avoided.
To reduce the overhead of control packets experienced in \cite{musaloiu2008minimising} and Muzi, channel change notifications are appended to outgoing acknowledgements.
This increases the time taken to change channel, and amplifies the atomicity issue - which ARCH addresses.

ARCH is designed for Home-Area Sensor Networks (HANs).
A channel change is prompted once conditions deteriorate below the threshold.
Once a channel is found suffering interference conditions, it is temporarily blacklisted from future selection; once too few channels become available, the blacklist is cleared.
While applicable in HANs with relatively stable interference, this approach may be inefficient in dynamic environments with constantly changing interference.

ARCH is firstly evaluated offline based on packet delivery traces, obtained from ten sensor network deployments in an apartment building (\cite{sha2011multi}).
These deployments are subject to uncontrolled interference originating from microwave ovens, WiFi, Bluetooth devices of the occupants, and thus \cite{sha2011arch} gives the most realistic evaluation.
In each apartment, the nodes cycled through the available 802.15.4 channels, recording packet error rate in 5 minute intervals - the experiment lasting 24 hours.
The findings show that the selection algorithm outperforms random, and fixed selection, and is only 6\% below the optimum achievable packet delivery.
Also, ARCH requires relatively few - at most 25 - channel switches per day to avoid interference.
ARCH is then evaluated in practice using the same testbed.
In both single-hop and multi-hop configurations, ARCH increases packet delivery rate, and reaffirms the earlier findings.

Chrysso incorporates frequency avoidance into the NET layer as part of the routing protocol, although channel change is still coordinated per-link.
Nodes append channel quality information to outgoing data packets sent to the parent node. There, the aggregate of this information is used to determine if a channel change is required.
Channels are switched, sequentially avoiding adjacent channels which may fall under the same interferer.
The implementation described in \cite{iyer2011chrysso} uses only a subset of five 802.15.4 channels.
As with ARCH, channel change is communicated via ACK packets, however the authors also describe a robust bootstrapping procedure to join a network, and include
logic to re-synchronise if control packets cannot be exchanged.

In addition to packet loss, \cite{iyer2011chrysso} measure energy consumption, thereby evaluating the energy cost of frequency avoidance strategy.
Chrysso is tested on two sensor network testbeds under interference conditions.
Compared to a fixed channel, Chrysso reduces packet loss and energy consumption; more so under heavy interference.
This indicates that the energy cost of overhead traffic is outweighed by the improved channel conditions.

These approaches represent single channel, \textit{reactive}, approaches - in that channel change is instigated in response to change in conditions, such as increase in channel activity or packet loss.
Recent multi-channel MAC protocols on the other hand, define a multi-channel \textit{proactive} approach - which hop between channels systematically during packet transmission as part of the MAC protocol design
\cite{kumar2013multi,gonga2012revisiting,mohammad2016oppcast}, similar to Bluetooth.

Oppcast is a receiver-initiated MAC protocol \cite{mohammad2016oppcast} that combines opportunistic routing and multi-channel communication, providing spacial and spectral diversity respectively to counter local interference sources.
Receivers periodically broadcast probes on a subset of 802.15.4 channels; to transmit, nodes listen on each channel for a probe, before transmitting the packet.
\textit{Fast Channel Hop} is defined as an efficient rendezvous mechanism, where receivers and transmitters cycle channels symmetrically until a probe is received.
Instead of a stringent routing path, nodes forward the packet on the next hop closer toward the sink, inherently avoiding interfered links.
Oppcast is evaluated against single channel-opportunistic, and multi-channel tree-based routing protocols.
In a 96-node testbed, Oppcast achieves increased packet delivery, lower latency, and lower energy consumption under interference conditions.
In ideal channel conditions, however, Oppcast experiences increased latency compared to single-channel protocols - a cost of additional channel rendezvous.
Therefore, reactive approaches may achieve better performance in cases where interference is less dynamic, and can be responded to quickly,
while proactive approaches such as this may better suit dynamic environments which mandate frequent channel changes.

\cite{art2016li} describes ART, a frequency planning technique that assigns each nodes' frequency based on its location within the network and measured environmental interference, including WiFi.
The frequency domain is divided into a continuous domain (within hardware limitations), rather than the 5$Mhz$ increments defined by the standard \cite{ieee802154}.
Thus, nodes in close proximity are assigned isolated frequencies far apart, otherwise using nearer frequencies.
This approach gives the WSN more freedom to avoid WiFi in the frequency domain, yet contravenes the standard.
The authors describe centralised and distributed implementations, the latter requiring significant overhead to coordinate channel assignments.
Each node must also know it's location within the network, which is unlikely in many deployments.

 %% All of these works requires the ability to measure interference and predict performance on a given channel.
 %% This is similar to LQU: Radio Link Quality Estimation in Wireless Sensor Networks: A Survey 2012, baccour et al
 %  - Presents a summary firstly of channel characteristics in WSN links:
 %     + In particular,transient, or Intermediate Quality (IQ) links, that vary significantly over time between good and bad.
 %     + These can be observed as 'bursty' links, that bare some resemblence to links affected by interference
 %     + Findings: very unstable, compared to high/low PRR
 %     + significant in number.
 %     + Did also evaluate interfered links: 802.11/802.15.1 unaffected, interference effects typically felt across adjacent channels.
 %  - Then, surveys existing LQEs as either:
 %     + hardware based: RSSI, LQI, or SNR
 %     + Software based: PRR, ETX, RNP, at packet-level
 %  - Findings:
 %     + HW solutions are only measured for successfully received packets, therefore can incorrectly classify links, especially transient ones, as good.
 %     + But, are good for reactivity, therefore combine into single metric (hybrid solution)
 %     + Metrics: PRR good, but takes a long time to measure   
 %     + Therefore, many solutions have averaging/window/weighted function to estimate PRR from more recent inputs.
 %     + For both cases, a minimum number of traffic is required.
 %     + Comparison between receiver side, and sender side.
 % 
 %  - My conclusions:
 %     + not useless to assessing link qualities for channel selection
 %     + Can use hybrid solution: choose channel based on RSSI sampling, fine tune with these metrics.
 %     + No work on how to include L
 %     + How to estimate individual link PRR from fluctuating channel properties?

A common feature of all these works is evaluating link conditions, in order to inform channel selection.
This shares many similarities with Link Quality Estimation (LQE): metrics which are used in route selection to structure the network.
For example, \ac{PRR} and \ac{RSSI} metrics may be used to determine the most suitable next hop in a network, for a given node.
Given the variety of LQE goals (such as reactivity, stability, and accuracy), approaches, and evaluation metrics, there has been a large body of work investigating LQE.

Baccour et al survey WSN link studies and LQE methods in \cite{baccour2012radio}, separating methods into \textit{hardware} - such as \ac{RSSI}, \ac{SNR}, and \textit{software} - such as \ac{PRR}, ETX.
LQE's operate on a per-link basis, and therefore can only extract information from delivered packets between nodes.
Conversely, frequency avoidance methods require measurements per-channel, although some may aggregate per-link information (\cite{iyer2011chrysso}).
Baccour et al conclude that combinations of LQEs, hardware and software, may be more accurate than a homogeneous metric \cite{boano2010triangle}.
Similarly frequency avoidance mechanisms which combine hardware and software measurements could allow for more efficient channel selection;
this remains an open research question.

 % SUMMARY
 %  - Evaluation has shown that frequency avoidance is a viable method to mitigating interference in some environments, importantly:
 %     + improving energy efficiency - cost of fa overhead is outweighed by benefits from retx
 %     + packet delivery - fewer packets lost, presumably to either CCA or collision
 %     + Retransmissions - fewer needed, better energy efficiency
 %     + multi-hop - more stable links.
 %  - These are all side effects of using more stable channels.
 %  - Distributed mechanisms better.
 %  - Studies have shown that it most environments, spectral avoidance is sufficient, there normally being at least one available channel.
 %  - Missing:
 %     + [x] and [x] include fallback options, in case synchronization cannot be achieved, however these are not evaluated or stress tested, therefore cannot be assessed.
 %     + environments like this could include...
 %     + Therefore, need mechanisms beyond switching, because...
 %     + Nodes either use PHY, or NET metrics. Combination of both would be good.
 %     + Evaluation with > 1 children.
 %  - Future:  
 %     + Analytical measure of freq. of channel change tradeoff - minimum number to justify these methods.
 %     + Distributed mechanism, that still tries to create large groups within WSN on the same channel.

These works have shown that frequency avoidance is a viable option to mitigate interference:
by switching to an unaffected channel, the adverse effects of interference can be avoided entirely.
Link metrics, including packet loss rate and retransmission count, are reduced where frequency agility is employed.
In all cases, additional control overhead is required, such as to aggregate channel readings and initiate channel change, which may incur additional energy cost.
Chrysso \cite{iyer2011chrysso} found, however, that the energy cost is outweighed by the benefits of frequency avoidance; for example, by requiring fewer retransmissions.

Studies measuring interference in typical environments have found that, in most cases, interference avoidance is sufficient to mitigate interference.
However, channel availability is dynamic, leaving no single channel consistently available.
Since Chrysso and ARCH describe only limited fallback options, relying on some other channel being deterministically available to rendezvous,
it is feasible that a network make become disconnected despite other channels being unaffected.
Therefore, other solutions that are able mitigate interference on the same 802.15.4 channel are justified in order to complement these frequency avoidance approaches.
The issue of switching atomicity is mitigated only by ARCH, however none of these methods have been evaluated under such conditions - where any nodes have more than one child node.
Similarly, these solutions must ensure agreement between nodes during channel selection and synchronisation marred by CTI, else channel change may not be unanimous \cite{boano2012jag}.

Scope for future work in this domain includes a comparison of software-based, NET-layer measurements (such as ETX, congestion) vs hardware PHY-layer metrics (\ac{RSSI}) for channel selection. 
An approach which incorporates both of these approaches could quickly eliminate bad channels via coarse PHY measurements, then more closely scrutinise channel selection based on finer NET measurements.
From these methods, a distributed scheme is preferred over a centralised approach, but may lead to unnecessary fragmentation of the network over multiple channels.
An approach which fragments the network as little as possible while still improving link-level reliability would build on these works.
