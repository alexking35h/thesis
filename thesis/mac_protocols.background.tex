\label{sec:mac_protocols_bg}
Given the implications of radio use on energy efficiency, many MAC protocols have been proposed for WSNs.
As well, as WSN applications have diversified, so to have MAC protocol requirements, to seek optimisation's also in QoS, link latency, and network throughput.
An exhaustive review can be found in \cite{demirkol2006mac,huang2013evolution}.
In order to conserve energy, nodes duty cycle the radio between low power sleep and active modes, the latter required to send/receive data.
Therefore, in order for two nodes to communicate, both must be in active mode.

In previous literature, the mechanism used to facilitate this synchronisation appears in two flavours: contention- and slotted/\ac{TDMA}-based.
Contention-based MAC protocols do not assume any synchronisation between neighbouring nodes' duty cycles.
In order to send data, both nodes must first initiate a costly transmission mechanism.
These approaches achieve a low idle cost, yet high transmission costs, and so are ideal in low data rate applications.
Conversely, slotted approaches (also known as \ac{TDMA}) require synchronisation amongst nodes in the network, each node assigned an uncontended slot to access the channel.
This allows for more predictable latency, and high throughput under heavy load.
When only a few nodes have data to send, the restriction on assigned transmission slots curtails maximum network throughput; this has been an active area of research in this domain.

Slotted approaches are preferable in high-data rate applications, however for infrequent communication, contention-based are more energy efficient,
and therefore more common in typical WSN deployments.
Contention-based approaches are therefore used as the basis for work in this thesis. The remainder of this section discusses the existing literature into contention-based MAC protocols.

Preamble sampling \cite{el2002aloha} is the precursor to many later contention-based MAC protocols which follow the same paradigm.
Here, nodes periodically wakeup and check for channel activity, returning to sleep if an idle channel is detected.
Transmissions are prepended with a preamble, of sufficient duration to be detected by the receiver.
This approach allows nodes to keep the radio powered down most of the time, and achieve better energy 
efficiency under light traffic loads than previous \ac{TDMA}-based protocols \cite{sohrabi2000protocols}.

In either case, the idle listening cost is kept relatively low, at the expense of having a high transmission cost:
transmitters must transmit a preamble throughout the receivers' duty cycle, in order for a packet to be received.
On the receiver-side, after detecting a preamble, nodes must wait upto the entire duration of the preamble before the packet begins.
Also, other nodes besides the target node can detect this preamble.
Energy is then wasted by keeping the radio powered until it is determined to be destined for another node; this is called the overhearing problem.
El-Hoiydi et al describe WiseMAC \cite{el2004wisemac}, a technique to reduce the transmitting cost.
While running, each node in the WSN learns the wakeup schedules of its neighbours.
Then, to transmit, the sender need only wait until the receiver is expected to wake up and sample the channel.
Not only does this reduce the energy cost of transmitting, but also increases the throughput available to the channel.
To reduce the overhearing problem, timing and address information can be encoded in the preamble, as in B-MAC+\cite{polastre2004versatile}.
Then, nodes can quickly determine the recipient of a packet, and non-target nodes can return to sleep, avoiding the overhearing problem.
The timing information allows target nodes to discover the time remaining until the start of the packet, and power down until then.

Both approaches are incorporated into X-MAC \cite{buettner2006x}, where the preamble is transmitted as multiple short packets strobes.
Target nodes are able to acknowledge a strobe, thereby allowing the packet transmission to begin earlier, further reducing the cost of transmitting.

In many \ac{WSN} \ac{MAC} protocols, \ac{LPL} is adopted to reduce idle listening time and further improve energy efficiency.
Here, \ac{CCA} is used to infer incoming traffic \cite{polastre2004versatile}.
If the channel is detected busy, the radio is kept powered on to receive data; otherwise, the node enters sleep state.
Thus, \ac{CCA} are used in these protocols for both sending and receiving; this is illustrated in figure \ref{fig:lpl}.
\ac{LPL} requires that the \ac{CCA} threshold is set accordingly.
Set too low, and false positives may be increased, causing the radio to waste time listening to an idle channel - referred to as a \textit{false-wakeup}.
Conversely, set too high and valid packets may be missed.
In B-MAC \cite{polastre2004versatile}, this threshold is set based on prior measurements of the noise floor.

\begin{figure}
 \centering
 \includegraphics[width=.7\textwidth]{figures/background/lplmac.eps}
 \caption{Operation of \ac{LPL} \ac{MAC} protocol:
          1) Transmit request followed by \ac{CCA} check, then packet strobes.
          2) Periodic receiver \ac{CCA} check to listen for incoming packets.
          3) After data is received, ACK transmitted, packet strobes stop.}
 \label{fig:lpl}
\end{figure}

ContikiMAC \cite{dunkels2011contikimac} is a \ac{LPL} \ac{MAC} protocol that is optimised to provide extremely low duty cycles by optimising the idle listening mechanism.
In this thesis, ContikiMAC is used as a base for optimisation in interference conditions, and is described in more detail in Section \ref{sec:contikimac}.
Within the TinyOS \acl{OS}, BoX-MAC \cite{moss2008box} is a similar \ac{LPL} protocol referred to as LPL.

Receiver-initiated MAC protocols have also been proposed as an alternative approach to the transmitter/receiver handshake \cite{sun2008ri, dutta2010design}.
Here, the receiver periodically broadcasts a beacon, then listens for incoming data.
To send a packet, the sender listens to the channel for a beacon, then transmits the packet.
This approach firstly reduces the channel use compared to preamble sampling, thereby leaving more channel bandwidth available to the rest of the network.
Secondly, collisions can be handled more centrally by the receiver, which allows for a faster collision-resolution strategy.
This approach does incur a higher idle listening cost, however has been shown to be more energy efficient under certain traffic loads than sender-initiated alternatives.


