set terminal png #pdf dashed enhanced font "Helvetica,14" lw 3
#unset title
#set key center bottom horizontal

set xlabel "Channel Busy Probability p"

set ylabel "Duty Cycle D(p) in [%]"
set yrange [0:14]
set output "closed_form.png"

#set key autotitle columnheader
#set key top right
#set key samplen 3

plot "closed_form.txt" using 1:((4*$2)/10000) title 'CCR 4Hz - Closed Form' with lines, \
	"closed_form.txt" using 1:((8*$2)/10000) title 'CCR 8Hz - Closed Form' with lines, \
	"closed_form.txt" using 1:((16*$2)/10000) title 'CCR16Hz - Closed Form' with lines, \
	"closed_form.txt" using 1:((16*$3)/10000) title 'CCR16Hz - Monte Carlo' with lines




