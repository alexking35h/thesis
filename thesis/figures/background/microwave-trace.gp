set term postscript
set output 'microwave-trace.eps

set xrange[0:50]
unset title
set ylabel "RSSI (dBm)"
set xlabel "Time (ms)"

plot 'microwave-trace.dat' using ($0*0.0325):1 w l notitle

