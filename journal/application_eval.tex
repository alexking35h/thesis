% !TEX root = article.tex
\section{\ac{P-DCCA} Application}\label{sec:app_eval}

To evaluate the benefit of \ac{P-DCCA} in practice, we implemented it in a popular \ac{WSN} \ac{MAC} protocol: ContikiMAC. ContikiMAC with \ac{P-DCCA}  is then evaluated in a variety of testbed and practical deployments and the achievable network performance is evaluated and compared with plain ContikiMAC. The presented evaluation is specific to ContikiMAC; however, similar performance improvements are to be expected when applying \ac{P-DCCA} to other \ac{WSN} \ac{MAC} protocols.

To include  \ac{P-DCCA}  in a \ac{WSN} \ac{MAC} protocol it is necessary to replace the sender and receiver based \ac{CCA} mechanism with the \ac{P-DCCA} mechanism. It has to be taken into account that  \ac{P-DCCA}  requires additional time compared to the standard \ac{CCA} (see Section~\ref{sec:dcca}). The additional time required depends on the used transceiver hardware and in many cases this additional required time does not require modifications to the \ac{MAC} protocol. When transmitting messages the power modulation must be included.  


\subsection{\ac{P-DCCA} in ContikiMAC}

ContikiMAC~\cite{dunkels2011contikimac} resembles other \ac{WSN} \ac{MAC} protocols (see section \ref{sec:preliminary}) which use \ac{CCA} for receiving and transmitting. To save power, ContikiMAC nodes keep the radio in a powered-down sleep state, and periodically listen for incoming packets once every duty cycle. Transmitters continuously transmit a strobe sequence back-to-back to the recipient, listening in between each packet for an acknowledgement. The operation of ContikiMAC is depicted in Figure~\ref{fig:contikimac_operation}. 

To listen for incoming packets, each node in the network uses two \ac{CCA} spaced $0.5ms$ apart; a busy channel detection from either causes the radio to be left powered-on to receive the next subsequent packet from the transmitter. Likewise, senders must check the channel is free using six \ac{CCA} before transmitting, and one \ac{CCA} between packet strobes - a single busy channel detection will cause the transmission to stop. 

To reduce unnecessary transmissions when the receiver is asleep, \ac{PL} is an optimisation in ContikiMAC which learns the wake-up times of neighbouring nodes. Future transmissions are then delayed until just before the receiver is expected to wake.

\begin{figure*} 
 \begin{centering} 
 \includegraphics[width=\textwidth]{figs/contikimac_operation_wide.eps}
 \caption{Normal Operation of ContikiMAC:
         1) Transmit request is followed by Phase-Lock delay, and \ac{CCA} check before transmitting.
         2) Periodic Receiver \ac{CCA} check detects incoming packet, entering receive-sequence.
         3) After data received, \ac{ACK} transmitted, and packet strobes stop.}
 \label{fig:contikimac_operation}
 \end{centering} 
\end{figure*}

\ac{P-DCCA} allows us to improve ContikiMAC's behaviour in interference environments as it enables us to implement fine-grained reaction to different interferer types. We decide to augment ContikiMAC with the following simple \ac{DCCA} behaviour:
\begin{itemize}
 \item \textbf{Transmitter \ac{DCCA}:} Transmissions are only stopped in response to detecting 802.15.4 traffic, ignoring other channel interference.
 \item \textbf{Receiver \ac{DCCA}:} The radio is only powered on to receive data after detecting 802.15.4 traffic, ignoring other channel interference.
\end{itemize}
ContikiMAC senders persist with transmissions after detecting interference, motivated by the possibility that the target node may still correctly receive the packet over the
interference. This may be due to variation in the interference signal strength throughout the network, or due to the short channel occupation time of the interferer. Transmitters only back off if  802.15.4 traffic is detected within a \ac{CCA} before transmission. 

ContikiMAC receivers avoid listening to the channel for incoming packets unless 802.15.4 traffic has been detected - reducing false wake-ups and improving energy efficiency. While inconclusive results are ignored, the risk of ignoring valid 802.15.4 packets is eliminated in the design of ContikiMAC: the 0.5$ms$ spacing between \ac{CCA} checks ensures that at least one \ac{CCA} check will not fall at the end of a packet in the strobe sequence (see Figure~\ref{fig:contikimac_operation}).

In our evaluation we compare network performance of plain ContikiMAC and ContikiMAC with \ac{P-DCCA} extension. In all setups the same network and interference configuration is used for ContikiMAC and ContikiMAC with \ac{P-DCCA}. 


\subsection{Experiment 1: Single \ac{WSN} Transmitter}
In the first experiment, the link performance and energy efficiency of ContikiMAC is measured in controlled interference conditions. Two Tmote Sky are placed in opposite corners in an unused office, spaced $6m$ apart. The nodes are connected to a host computer over a USB connection, used for reprogramming and communication. In the same office, an IEEE 802.11g access point and station are placed in opposite corners, and are connected to the host computer over a wired connection. Interference is generated using the D-ITG traffic generation tool \cite{avallone2004d}, sending UDP traffic with varying intensity. The set traffic rates were achieved with present 802.15.4 traffic as the channel was not fully saturated. The experiment takes place during quiet office hours with reduced wireless background traffic in the building. WiFi channel 11 and IEEE 802.15.4 channel 22, which were found to be the quietest overlapping channels in the environment, are used for the experiment. 90-byte packets are sent at a rate of 4 packets per minute without retransmissions. The following performance metrics are recorded:

\begin{itemize}
 \item \textbf{\acf{PRR}:} \ac{PRR} is a reflection of the link performance, and is the ratio of successful packet transmissions to total attempts. 
 \item \textbf{\ac{RoT}:} \ac{RoT}  is used to measure the energy efficiency of ContikiMAC under interference. It is the time the transceiver is in an active state divided  by the number of successfully received packets. 
\end{itemize}

\ac{PRR} and \ac{RoT} are recorded at the receiver during the experiment. Each interference level is run for five minutes. The results are shown in Figure~\ref{fig:contikimac_eval}.

\begin{figure}[ht]
 \centering
 \subfigure[Packet Reception Rate] {
  \includegraphics[width=.75\textwidth]{figs/cmac_prr.eps}
  \label{fig:contikimac_eval_prr}
 }
 \subfigure[Radio-on time] {
  \includegraphics[width=.75\textwidth]{figs/cmac_rtime.eps}
  \label{fig:contikimac_eval_energy}
 }
 \caption{ContikiMAC with standard \ac{CCA} and \ac{P-DCCA}. 
          \ac{P-DCCA} significantly improves packet reception rate and energy efficiency under interference.}
 \label{fig:contikimac_eval}
\end{figure}

Figure~\ref{fig:contikimac_eval_prr} shows that the \ac{PRR} of ContikiMAC is markedly improved using \ac{P-DCCA}. Under the heaviest interference tested, standard ContikiMAC \ac{PRR} falls below 5\%, while the \ac{P-DCCA} variant achieves above 50\%. The graph shows that the improvement of \ac{P-DCCA} over standard ContikiMAC becomes greater as interference increases. This is because as interference increases, the probability of a collision with ContikiMAC \ac{CCA} increases. In case of standard ContikiMAC a collision results in an aborted transmission which leads to a  \ac{PRR} reduction. In case of ContikiMAC with \ac{P-DCCA} the transmission is carried out (unless it is the case of a false negative \ac{P-DCCA} classification) which is then often successful. 

In Figure~\ref{fig:contikimac_eval_energy}, the radio-on time per packet received increases proportionally with the interference rate. This is because as interference increases, \ac{PRR} falls - increasing the energy expenditure per packet. Also, false wake-ups increase, which increase idle listening. In both cases, $10ms$ is the minimum achieved on-time, this reflects the guard time when a sender begins transmitting packets, before the receiver is expected to wake up. As interference increases, standard ContikiMAC shows significantly worse performance, measuring $65ms$ under heavy interference, while the radio-on time of \ac{P-DCCA} rises to only $12ms$. 

This experiment has shown that a simple \ac{DCCA} policy on top of the \ac{P-DCCA}  mechanism significantly improves network performance. \ac{PRR} is improved while energy consumption, expressed via \ac{RoT} is reduced. For the highest measured interference level \ac{PRR} is improved ten-fold, while \ac{RoT}  is reduced by 82\%.

%% --------------------------
\subsection{Experiment 2: Multiple \ac{WSN} Transmitters}

The previous experiment affirmed the benefits of \ac{P-DCCA} in a single transmitter environment, where only one ContikiMAC node is transmitting. In this case, any interference detected is known to have originated from outside the network, such as from WiFi interference. However, in interference environments with multiple transmitters, \ac{CCA} collisions may be due to either interference, or another \ac{WSN} transmitter. In these cases, channel arbitration can be aided by \ac{P-DCCA}.
For example, upon collision with a WiFi signal, the more optimal approach in our implementation is to persist with the transmission. Conversely, collision with another ContikiMAC packet should result in the back off behaviour used within the \ac{WSN}.

In this experiment, an IEEE 802.11g network generates interference at a fixed rate of $37.5KB/s$.  Sensor nodes use the ContikiMAC \ac{CSMA} to handle retransmissions, initiating a random back-off after every unsuccessful transmission attempt. On each node, upon an acknowledgement being received or a timeout, a new transmission is initiated - attempting maximum throughput. The average throughput per node is used as evaluation metric. 

In each experiment, one, two, four and eight Tmote Sky nodes are used. As well as standard and \ac{P-DCCA} ContikiMAC, a third derivative is included: NO-CCA, where no \ac{CCA} checks are conducted either before, or during transmission. In a single transmitter use case, this would be advantageous, since the only source of interference is guaranteed to have originated from outside the network, and an aggressive policy will likely not have a negative impact on the network. This is not the case in a multi-transmitter use case however, since interference may be due to environmental noise or another device in the same network.
The results of this experimental run are shown in Figure~\ref{fig:contikimac_eval_multiple}.

\begin{figure}
 \begin{centering}
  \includegraphics[width=.75\textwidth]{figs/multi_nodes.eps}
  \caption{ContikiMAC Evaluation with multiple transmitters under WiFi interference.
    \ac{P-DCCA} provides the greatest throughput compared to standard ContikiMAC and NO-CCA.}
  \label{fig:contikimac_eval_multiple}
 \end{centering}
\end{figure}

The results show that in the case of one transmitter, NO-CCA slightly outperforms \ac{P-DCCA}. This is due to false positives in \ac{P-DCCA} - incorrectly detecting WiFi interference as 802.15.4 packets and backing off. With multiple transmitters however, \ac{P-DCCA} outperforms both NO-CCA and standard ContikiMAC, achieving better average throughput per node. This is due to collisions with other transmitters being avoided, but nodes persisting when faced with WiFi interference.

This experiment has shown that the \ac{P-DCCA}  mechanism significantly improves channel arbitration in an interference environment. For example, when using 4 concurrent transmitters \ac{P-DCCA}  ContikiMAC improves throughput per node by 36\% when compared with standard ContikiMAC.

%% -----------------------------
\subsection{Experiment 3: Testbed Deployment}

Implementing \ac{P-DCCA} with simple interferer-detection policies has been shown to reduce the energy costs and improve the \ac{PRR} for individual links. The performance of \ac{P-DCCA} in the context of a full network deployment (including routing protocol and multi-hop links), is evaluated next using a large scale \ac{WSN} testbed. 

%Links over greater distances than in the previous experiment may behave differently with \ac{P-DCCA}, due to the power variation used to transmit packets. This is tested in this experiment.

The WISEBED~\cite{coulson2012flexible} testbed at the University of L\"{u}beck is used. This testbed is located on the 2nd floor of an office building, and consists of 162 nodes arranged in clusters of three. Each node is connected to a host laptop, and the testbed is coordinated over an ethernet backend, which allows for node reprogramming. To aid network configuration, all of the nodes have pre-allocated \ac{MAC} addresses. For this experiment, only the TelosB nodes in the testbed are used.

All nodes are programmed with the \ac{RPL} routing protocol, which establishes an acyclic graph from a sink node to each node in the network. Each node then has a single parent, to forward data towards the sink, and a number of children, for downstream traffic. As  \ac{RPL} is used, the topology is not static and routing paths are subject to changes. The IPv6 protocol stack is used to handle packet forwarding and address assignment. The default Contiki \ac{CSMA} behaviour is used to handle retransmissions, with an exponentially increasing back-off and a maximum retry limit of three.

Within the 49-node network, eight nodes are configured as sources. These generate a 60-byte packet every 30 seconds, sent to a single sink node located at one end of the deployment. The remaining nodes are part of the network, forwarding packets as required to the sink. The wakeup frequency of all nodes in the network was set to 8$Hz$.

It was necessary to generate repeatable and reliable interference under controlled parameters in the testbed.
To achieve this, five of the TelosB nodes were programmed to simulate WiFi traffic, as described in JamLab~\cite{boano2011jamlab}.
These nodes used the test transmission mode of the CC2420, whereby psuedo-random data is transmitted continuously on the same IEEE 802.15.4 channel.
The nodes alternate between maximum and minimum transmission power, to simulate packetised IEEE 802.11g traffic.
The \emph{on} duration of the interference was 577$\mu s$, to simulate 1500-byte packet communication, including IEEE 802.11g RTS/CTS and acknowledgement.
The \emph{off} duration, reflecting the spacing between simulated IEEE 802.11g packets, was calculated randomly over a time window that is changed to throttle the degree of interference.
This approach does not simulate the IEEE 802.11g \ac{MAC} protocol, and does not coordinate between simulated interference.
As a consequence, the degree of interference (which is used as the independent variable in this experiment) is per-interferer, and is not the sum of all interference during the experiment.
This approach allows for an evaluation of \ac{P-DCCA} over a multi-hop network in the presence of an interference source.
In JamLab~\cite{boano2011jamlab}, the authors showed that this approach is able to achieve high accuracy of \ac{WSN} performance compared to the emulated interferer.
The arrangement of sink, sources, other nodes, and interferer nodes in the testbed are shown in Figure~\ref{fig:wisebed-deployment}.

\begin{figure}
 \begin{centering}
  \includegraphics[width=.75\columnwidth]{figs/wisebed-deployment.eps}
  \caption{54-node Wisebed Deployment in L\"{u}beck, consisting of IEEE 802.11 interferer, source and sink nodes.}
  \label{fig:wisebed-deployment}
 \end{centering}
\end{figure}

The source and sink nodes report each packet transmission and reception event respectively.
This is used to calculate the number of received packets at the sink: $P_{Received}$, and the number of packets sent from the $nth$ source node: $P_n$.
From this, the average \ac{PRR} from $N$ source nodes is calculated as:

$$
PRR_{Average} = \frac{P_{Received}}{\sum_{n=1}^{N}P_n}
$$

Also, all nodes in the network periodically reported the \ac{RoT} of each node, every ten seconds.
This is used to evaluate the energy efficiency of the network, in the same way as in the first experiment.
The average \ac{RoT} is calculated across all nodes in the network.
\ac{P-DCCA} and standard ContikiMAC variants are tested in 12 minute iterations, for each interference level.
The experiment is repeated four times. The results are shown in Figure~\ref{fig:testbed_experiment}.

\begin{figure}[ht]
 \centering
 \subfigure[Packet Reception Rate] {
  \includegraphics[width=.75\textwidth]{figs/testbed_prr.eps}
  \label{fig:testbed_prr}
 }
 \subfigure[Radio-on time] {
  \includegraphics[width=.75\textwidth]{figs/testbed_energy.eps}
  \label{fig:testbed_energy}
 }
 \caption{Energy efficiency and \ac{PRR} of ContikiMAC and \ac{P-DCCA} in a large-scale deployment. Results show \ac{P-DCCA} achieves lower energy consumption and higher packet delivery under interference.}
 \label{fig:testbed_experiment}
\end{figure}

The \ac{PRR} results (Figure~\ref{fig:testbed_prr})  are similar to the results obtained in the first experiment. The ContikiMAC \ac{P-DCCA} variant is less affected
by interference than standard ContikiMAC. In this experiment, however, \ac{P-DCCA} also suffers severe packet loss under high interference rates. This may be due to the cumulative reduction in \ac{PRR} which is felt across all links on a path - as opposed to the affect on an individual link as measured previously. Also, this experiment simulated interference originating from multiple IEEE 802.11 interferers, as opposed to a single, albeit higher power, interferer. Nevertheless,  \ac{P-DCCA} ContikiMAC  clearly outperforms standard ContikiMAC in all interference conditions. 

The energy consumption measured via \ac{RoT} (Figure~\ref{fig:testbed_energy}) shows similar trends to earlier experiments. The baseline under no interference is measured as 1.3$ms$. This is lower than the \ac{RoT} measured for a single link (as in figure \ref{fig:contikimac_eval_energy}), as this is averaged across all nodes in the network - including nodes that do not participate in packet forwarding. As interference increases, the \ac{RoT}  increases drastically with standard ContikiMAC. \ac{DCCA} mitigates this affect. This is due, in part, to the reduced rate of false wake-ups caused by collisions with interference, and also due to increased \ac{PRR} with \ac{P-DCCA}. As before, in this larger experiment, the energy efficiency of \ac{P-DCCA} is less impervious to interference than in the previous smaller experiments.

These results confirm that \ac{P-DCCA} benefits link quality and energy efficiency, on an individual link level, and also in large multi-hop networks. Importantly, the reduced transmission range stemming from \ac{P-DCCA} power modulation does not impair packet delivery. This is due to either link availability or quality being relatively unimpaired by this power modulation, or whose affects are negligible in large networks. Consequently, these results show that the drawbacks of employing \ac{P-DCCA} are far outweighed by the benefits, in this case. 

This experiment has shown that  the \ac{P-DCCA}  mechanism significantly improves network performance. \ac{PRR} is improved while energy consumption, expressed via \ac{RoT} is reduced. For the highest measured interference level \ac{PRR} is improved seven-fold while \ac{RoT}  is reduced by 85\%. Reduced communication range due to the power variation has no negative impact in a relatively dense network setting such as the WISEBED testbed. 

%% ------------------------------------
\subsection{Experiment 4: Office Deployment}

To evaluate \ac{P-DCCA} over longer durations in an uncontrolled, more realistic environment we used a small office deployment.  Two Tmote Sky nodes are placed at opposite ends in a busy office environment, with frequent WiFi interference from nearby laptops and smartphones. The transmitter attempted to transmit packets at a fixed rate of 4 packets per second.  Two variants are compared: standard ContikiMAC and ContikiMAC with \ac{P-DCCA}, each running on both nodes for five minute intervals. The experiment lasts over 24 hours, and as before, \ac{PRR} and \ac{RoT} are recorded. The results of this experiment are shown in Figure~\ref{fig:24h_experiment}.

\begin{figure}[ht]
 \centering
  \subfigure[Packet Reception Rate] {
   \includegraphics[width=.75\textwidth]{figs/24hr_prr.eps}
  }
  \subfigure[Radio-on time per packet received] {
   \includegraphics[width=.75\textwidth]{figs/24hr_energy.eps}
  }
 \centering
 \caption{PRR and Radio-on time of ContikiMAC with and without \ac{P-DCCA} in a busy office 
   environment. Results show that \ac{P-DCCA} improves network and energy efficiency 
   throughout the experiment.}
 \label{fig:24h_experiment}
\end{figure}

The results mirror earlier experiments: \ac{P-DCCA} improves link performance and node efficiency in ContikiMAC under local \ac{RF} interference.
At peak office hours during this experiment, \ac{PRR} was improved by up to 180\%, and energy consumption reduced by up to 40\%.



