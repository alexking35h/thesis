set term epscairo size 5,3
set output 'range_prr.eps'
set xlabel "Distance (m)"
set ylabel "Packet Reception Rate (%)"
set grid
set xrange[0:210]
set key top outside center horizontal
plot 'range_prr.tsv' using 1:($2/2880) title 'P-DCCA (5dB)' w l ls 1 lc rgb 'black' lw 3 dt 1,\
'range_prr.tsv' using 1:($3/2880) title 'Standard (0dB)' w l ls 1 lc rgb 'black' lw 3 dt 2

