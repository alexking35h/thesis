set term postscript
set output 'radio_on_time_contikimac_under_wifi.ps'
unset key
set xlabel "IEEE 802.11 Interference (KB/s)"
set ylabel "Radio-on time per packet received (ms)"
set grid
plot 'radio_on_time_contikimac_under_wifi.tsv' using 1:2 lw 2 w l

