% Overview of classification.
All of these mitigation strategies incur an overhead in terms of microprocessor, communication and energy resources.
For example, channel switching has a communication overhead to ensure all nodes are aware of the new channel, and FEC codes require additional packet payload and calculation.
Therefore, in environments subject to varying degrees of interference,
sensor networks can achieve greater energy efficiency by invoking these mechanisms only when required.

Similarly, the type of interference may determine the most optimal mitigation strategy.
For example, moving to an unaffected channel may be the best response under 802.11 interference, while selecting a random backoff to desynchronize with interferers may be most suited under microwave oven interference ~\cite{hermans2013sonic}.
Mitigation approaches may also be fine tuned, depending on characteristics of the interference.
For example, 802.11 b, g, and n, may each dictate optimal FEC parameters to balance packet resilience while minimizing packet payload.

Interference classification is often described as a precursor to interference mitigation \cite{hermans2013sonic,chowdhury2009interferer}, for these reasons.
When deployed, sensor nodes may measure and characterize the interference in the environment, and enable or fine tune these parameters as required.

% Passive & Active

A thorough review and taxonomy of classification mechanisms can be found in \cite{zacharias2014lightweight}.
Classification approaches can be classed firstly as passive, whereby the affects of interference on normal network operation - such as packet corruptions - are observed, and used to draw conclusions on the type of interference.
Secondly, active approaches sample the environmental interference alongside other node functions, either periodically or in response to some other stimuli, such as a sudden inrcease in packet loss.

In both cases, time- or spectral- components are measured to construct a feature set of interference signals.
This is then pased to a classification algorithm, which typically leverages prior known characteristics of possible interference sources, to identify the signal type.
For example, minimum packet spacing and maximum packet length, which are defined in the 802.11 standard \cite{ieee80211}.

% PASSIVE

% 'Dynamic Link adaptation based onc oexistenced fingerprint deteection' (nicholas '12)
Nicolas and Marot \cite{nicolas2012dynamic} 
propose a passive mechanism whereby corrupted packets are preserved in memory until the correct original is received after subsequent retransmissions.
These are then compared to identify bit error patterns, from which the type of interferer present is inferred.
Only 802.11b and Bluetooth interference is considered, disregarding 802.11g on the assertion that its more sensitive CCA is less suseptible to collisions.
Experiments in prior literature \cite{liang2010surviving}, and results in chapter \ref{chap:impact} contradict this.
Further, the authors do not explain the classification algorithm, and the evaluation is too superficial to draw firm conclusions on.

Hermans et al follow a similar approach in SoNIC \cite{hermans2013sonic} and \cite{hermans2012lightweight},
supplementing bit error patterns in corrupted packets, with RSSI samples recorded during packet transmission.
Interference signals appear in the latter as spikes in the RSSI trace.
These are both combined into a feature set, then passed as input to a classification algorithm to identify the most likely interference source.

Nicolas and Marot use a single coarse statistic to classify interference: the density function of bit errors in corrupted packets.
Conversely, Hermans et al gather finer measurements, such as LQI, error burst durations and RSSI features, as input to a decision tree classifier;
this approach presumably leads to a shorter classification time.

Both papers include an application evaluation of classification in practice: tailoring interference mitigation to a given interferer, verifying the use of classification mechanisms in this context.
These approaches require only that a mechanism is in place to recover corrupted packets, either in the form of automatic retransmissions or FEC; this is already in place with most WSN applications.
However, sufficient traffic load is required by these methods in order for a minimum number of corrupted packets to be passed to the classifier.
Below this, the classification algorithm may have too few corrupted packets to draw a reliable conclusion.
Similarly if the degree of interference exceeds packet recovery efforts, such as if retransmissions are unsuccessful, this approach may not work.
Testing and analysis of these traffic rates is not included in either paper.

% ACTIVE

% "A lightweight Class... - zacharias 2014" vs ZiSense.

Zheng et al present ZiSense, a WSN MAC protocol enhancement to improve transmitter/receiver rendezvous mechanisms in the presence of interference.
The authors supplement two MAC protocols with ZiSense. 
Firstly, LPL \cite{moss2007low} is able to distinguish 802.15.4 signals from other interference, hence avoiding false wakeups and improving energy inefficiency.
Secondly, probe packets corrupted by interference in LPP \cite{dutta2010design}  can still be detected, improving packet reception performance under such conditions.
Despite having different motivations to these other works, ZiSense is comparable as an active-sampling classification mechanism.

From the RSS trace, ZiSense identifies individual signals, and for each constructs a feature set consisting of \textbf{1) On-air time:} the duration of each signal, \textbf{2) Peak-to-Average-Power Ratio (PAPR)}: a measure of the shape of a signal, \textbf{3) Minimum Packet Interval (MPI):} the minimum interval between successive transmissions, and \textbf{Under Noise Floor (UNF):} a binary indication of RSS dips beneath the noise floor, characteristic of mcirowave oven interference.
(1) and (3) can be correlated for each interference source, for example the maximum value of (1) for an 802.11g device is 542$\mu s$.
The authors found that PAPR differs depending on the modulation technique: WiFi has a higher value than Bluetooth/ZigBee due to different modulation techniques.

IEEE 802.11b interference, which is not included in the original evaluation of ZiSense,
is found later in chapter \ref{chap:dcca} to cause a high rate of false positives in ZiSense - being miss-classified as 802.15.4 packets.
This is due to the DSSS modulation used in 802.11b - as opposed to OFDM in 802.11g, having  closer measurable PAPR and timing characteristics to 802.15.4.
While almost obsolete, rate selection algorithms in many 802.11 stations may fall back to this older revision under poor channel conditions, 
including those caused by interference from other non-802.11 devices.
This was found to be the case in chapter \ref{chap:impact}.
Further, the high degree of accuracy under 802.11g/n, microwave, and Bluetooth interference as reported in \cite{zheng2014zisense} could not be replicated in chapter \ref{chap:dcca};
however, a different method of measuring accuracy, and different testing conditions, were used.

Zacharias et al \cite{zacharias2014lightweight} propose a similar approach for the purposes of interference classification and mitigation, instead using Clear Channel Assessment (CCA).
A CCA is a true/false measurement, based on if the RSS exceeds the set threshold in hardware; this requires less RAM than RSSI samples, but is less descriptive.
\cite{zacharias2014lightweight} thus relies on measuring the periodicity of interference signals for classification.
For example, microwave ovens transmit a signal with a period defined by the electrical power source, and 802.11 Access Points periodically broadcast a beacon in order to be detected by other stations.

A similar mechanism called ZiFi is described by Zhou et al in \cite{zhou2010zifi} to detect WiFi signals, where low power 802.15.4 radio hardware is utilized to listen for and detect 802.11 AP beacons,
in order to conserve use of the main 802.11 radio.
Nodes record an RSS trace, then use an equivalent algorithm, termed Common Multiple Folding (CMF) - optimized for 16-bit embedded devices, to search for these periodic signals.
ZiFi is not designed for interference classification, and the evaluation does not measure accuracy in busy environments with other, non-802.11, interference sources.
Both of these works, \cite{zhou2010zifi} and \cite{zacharias2014lightweight}, show that the periodicity of signals can be used to differentiate, and classify, interference sources in an environment.
However, compared to more micro features, periodic features require longer to sample, and hence classify.

Comparing these approaches to ZiSense, the latter meaures more intricate signal features to create a more descriptive feature set for each interference signal.
By not relying on long term periodic features, ZiSense is able to classify signals faster as required by the papers motivation.
Further, ZiSense can distinguish between different interference sources based on dissimilar signal strengths, allowing for a more robust classification mechanism in busy environments.

WiSpot \cite{ansari2011wispot} is an active scanning approach that samples RSS across multiple 802.15.4 channels to search for spectral signatures matching known interference sources.
The authors propose a simple algorithm based on RSS magnitude to identify 802.11b/g signals, which may be overlapping.
The main drawback of this work is the use of a multi-radio node design, needed to sample contiguous channels simoultaneously.
This allows for a shorter classification time than single channel approaches - due to the reliance on spectral and not temporal features, at the expense however of atypical WSN hardware, incompatible with most other WSN designs.
Also, WiSpot is only evaluated to detect and classify 802.11b/g interference;
the spectral signature identification algorithm is likely too superficial to differentiate other interference, such as Bluetooth.

Chowdhury et al describe a similar approach in \cite{chowdhury2009interferer}, using instead only a single node to actively scanning multiple 802.15.4 channels.
This is more compatible with most WSN designs, however requires a much longer sampling duration to fully capture all interference sources.
The authors describe and evaluate this approach in the context described above, tailoring interference mitigation to microwave oven, and 802.11b interference.
The spectral classification mechanism is more scrutinuous compared to WiSpot:
contiguous channel RSS are taken as components into a vector, whose angular differences to a reference signal are used to classify signals.

The first criticism of this paper is the lack of implementation details, including the use of floating point trigonometric calculations on 16-bit MCU lacking an FPU.
Despite promising accuracy measurements, \cite{chowdhury2009interferer} includes only microwave oven and 802.11b interference, the latter of which is now effectively obsolete.
The authors evaluation is based on a custom, untested simulator, whose results may differ from real-world deployments.
Further, testing of this approach does not included multi-transmitter environments - overlapping or not, to which this classification mechanism may possibly be less effective.

%% Summary
The work on interference classification described above is difficult to compare, due to different design motivations, sampling and processing methods, and tradeoffs.
It is fruitless to compare the reported accuracy from each paper, as the testing conditions and methods of measuring accuracy are not equal.

Likewise, comparing the timing characteristics of these approaches is useless outside the context of other tradeoffs.
For example, WiSpot is designed to classify all interference in an environment, unlike ZiSense, which is only designed to be incorporated into a MAC protocol.
However it does provide an instructive dimension to consider for the purposes outlined in this thesis, and in mechanisms such as ZiSense.
That is, in order to incorporate into WSN MAC protocols.

Firstly, passive mechanisms trade energy efficiency for detection and classification latency.
These mechanisms inherently require the longest time in order to detect an interferer, and are a product of both traffic rates within the WSN, and of the interference source.

Secondly, spectral signal matching provides the shortest detection time only if specialized hardware is used \cite{ansari2011wispot}.
This is because a snapshot of channel activity is sufficient to identify interference sources, without the need for temporal features which require longer sample durations.
With standard, single radio hardware however (as in \cite{chowdhury2009interferer}), extremely long sample durations are necessary to capture all interference on each 802.15.4 channel,
as the channel-switch time is greater than the minimum packet length for most interferers.

Finally, the sampling duration for time-based approaches is dependent on the classification mechanism used.
Signal periodicity-based approaches (\cite{zhou2010zifi,zacharias2014lightweight}), require sufficient sampling durations in order to capture periodic signals.
For example, the default 802.11 AP beacon frequency is 10Hz: once every 100$ms$.
More scrutinous approaches that detect characteristics pertinent to each interferer source have far shorter classification and sampling durations;
ZiSense \cite{zheng2014zisense} requires 3200$\mu s$ - an order of magnitude shorter.

\ac{DCCA} is presented in chapter \ref{chap:dcca} an an enhancement to the standard 802.15.4 CCA mechanism, and is closer to ZiSense, than an interference classification mechanism.
Rather than classifying all interference in an environment, \ac{DCCA} is designed to differentiate between a clear channel, an 802.15.4 signal, and other interference.
As with ZiSense, detection and classification latency should be minized, motivating the design of \ac{DCCA} as an active, single channel sanning approach.
In order to further reduce this latency, transmissions are encoded with an orthogonal channel via Transmit Power Variation (TPV);
this additional encoding is more readily detected by receivers using \ac{DCCA}, as opposed to implicit features of the 802.15.4 PHY standard.

The estimation work in chapter \ref{chap:estimating} presents a tool to estimate WSN energy consumption and predict node lifetime, caused by false wakeups in a given environment.
The channel busy probability, obtained by sampling the CCA over time, is passed as input to calculate this energy consumption.
However, no interference classification is conducted, and the tool itself remains agnostic to any particular source of interference.

