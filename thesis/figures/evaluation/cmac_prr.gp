set terminal postscript size 7,4
set output 'cmac_prr.eps'

#set key horiz invert reverse center bottom outside
set yrange [0:0.8]
set xrange [37.5:225]
set ylabel "Packet Reception Rate (%)"
set xlabel "IEEE 802.11g interference (KB/s)"
set grid y

plot 'cmac_prr.dat' using 1:2 w lines t "ContikiMAC P-DCCA" lw 3 lc rgb "red" , \
'' using 1:3 w lines t "ContikiMAC Standard" lw 3 lc rgb "blue" lt 1

