set term epscairo size 5,3

set xlabel "Channel Busy Probability p"

set ylabel "Duty Cycle D(p) in [%]"
set yrange [0:4]
set output "exp1.eps"


#plot "exp1.txt" using 1:(((16*$3)/10000)/108) title 'CCR 16Hz - Measured' with lines, \
#	"exp1.txt" using 1:(((16*$4)/10000)/108) title 'CCR 16Hz - Monte Carlo' with lines

plot "exp1.txt" using 1:($3*100) title 'CCR 8Hz - Measured' with lines, \
	"exp1.txt" using 1:($4*100) title 'CCR 8Hz - Monte Carlo' with lines



