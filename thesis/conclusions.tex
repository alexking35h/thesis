\chapter{Conclusion}
\label{chap:conclusion}

The conclusions of this thesis are presented in this chapter.
Firstly, the premise - which is the effects of \ac{CTI} acting upon \ac{WSN} - is revisited from the perspective of the related work discussed in Chapter \ref{chap:related}.
Following this, the contributions of this thesis are summarised with regard to the original aims.
The scope for future work extending this work is then discussed, before some closing remarks which bring the chapter and thesis to a conclusion.

\section{Thesis Discussion}

Due to the advancement of hardware and software design, the ongoing realisation of the IoT, and broader demand, \acp{WSN} have become more prominent over the past decade.
Deployments have stretched beyond academic fields of interest, to also include industrial, office, and residential applications.
Combined with the proliferation of wireless technologies operating in the 2.4$Ghz$ ISM band, the issue of \ac{CTI} is now unavoidable for some \ac{WSN} designers.
Likewise, other technologies which share similarities with \ac{WSN}, such as home automation, vehicular sensor networks, and the IoT, face the same issues.

In this thesis, the costs of \ac{CTI} on \acp{WSN} were considered twofold: packet delivery and energy inefficiency.
Based on the 802.15.4 PHY protocol, \ac{LPL} protocols were considered as a specific subclass of \ac{WSN} \ac{MAC} protocols - common in applications favouring low energy consumption and data rate.
Here, \acp{CCA} are used for transmitting and receiving, enabling low duty cycles and long deployment lifetimes.
As discussed in Chapter \ref{chap:background}, the standard \ac{CCA} mechanism - \acl{ED} - is susceptible to other interference.
Previous works, discussed in Chapter \ref{chap:related}, have shown this may lead to suboptimal performance.

In Chapter \ref{chap:related}, previous studies and coexistence solutions relating to \ac{WSN} and other 802.15.4 networks were discussed.
Previous works were shown here to have modelled link quality under interference conditions, and energy consumption of \ac{WSN} in general terms.
However, there was shown to be no intersection of these domains: modelling energy consumption of \ac{WSN} under interference.
Consequently, predicting the lifetime of such a \ac{WSN} prior to deployment is currently difficult in interference conditions.

Then, current solutions to mitigate \ac{CTI} were classed based on strategy: drawn from avoidance, detection, and resilience.
It was concluded here that there is currently no \ac{WSN}-based detection approaches that are sufficiently accurate; 
do not rely on atypical hardware; do not significantly increase idle listening.

The two primary contributions of this thesis followed on from these observations, and are discussed in detail in the remainder of this section.

\subsection*{Methods for estimating \ac{WSN} \ac{MAC} protocol duty cycle for a node, based on the environmental interference.}

In Chapter \ref{chap:estimating}, tools were presented which enable \ac{WSN} designers to estimate the duty cycle of \ac{LPL} protocols, based on measurements of interference.
This chapter began with a discussion of interference measurement techniques available, which have previously been used for link reliability modelling and interference classification.
This section concluded that it is sufficient to measure $P_C$, the channel busy probability.

Following this, a closed form estimation of \ac{WSN} duty cycle was presented, based on the popular ContikiMAC protocol.
The complexity of this approach stemmed from the ContikiMAC state machine, through which numerous paths exist for the receiver to take.
This approach is largely inflexible to modifications of how ContikiMAC detects and responds to a busy channel.
Therefore, modifying this approach to account for changes to the \ac{MAC} protocol wakeup or channel sensing mechanism is inherently difficult.

These drawbacks motivated the development of a Monte-Carlo solver approach, which takes as input a LUA script representing the \ac{MAC} protocol wakeup sequence.
This approach more closely follows the actual implementation in software of ContikiMAC, and is more accommodating to \ac{MAC} protocol changes.
Both approaches were shown to predict closely the impact of $P_C$ on the duty cycle.

The accuracy of the solver was then evaluated under controlled interference conditions in a testbed, 
and uncontrolled conditions in realistic deployments.
For typical \ac{WSN} traffic rates, the solver was shown to be accurate.
While the ContikiMAC \ac{MAC} protocol was used in this chapter as an example,
the techniques presented are applicable to any \ac{WSN} \ac{MAC} protocol that relies on \ac{CCA} for synchronisation.

\subsection*{An extension to the IEEE 802.15.4 standard \acf{CCA} mechanism: \acf{DCCA}.}

\ac{DCCA} was presented in Chapter \ref{chap:dcca} as a conceptual extension to the standard \ac{CCA} mechanism.
As well as indicating the channel as busy/free, \ac{DCCA} can indicate the source of channel contention.
This can then be used to optimise collision response policies, and eliminate false wakeups in the receiving sequence.

Three methods of implementing \ac{DCCA} were considered.
\ac{MD-DCCA} was considered the most efficient, although not possible on typical \ac{WSN} hardware.
\ac{T-DCCA} and \ac{P-DCCA} were then considered as alternatives not reliant on atypical hardware.
The former was based on a similar approach in literature \cite{zheng2014zisense}, while the latter was developed in this thesis.
\ac{P-DCCA} works by varying the output transmission power, then detecting this characteristic feature at the receiver.

An application evaluation then followed, wherein \ac{P-DCCA} was implemented in ContikiMAC, and evaluated in terms of \ac{PRR} and radio on-time.
For a small, single-hop network, \ac{P-DCCA} was shown to improve packet delivery performance and energy efficiency, compared to standard ContikiMAC.
These results were mirrored in an office environment subject to realistic interference conditions.
The same experiment was then repeated on a large, 49-node \ac{WSN} testbed, where similar results were achieved.

In this chapter, \ac{DCCA} was shown firstly to be an effective approach to mitigating interference in heterogeneous network environments.
This is a novel concept to mitigating channel contention in \acp{WSN}:
even simple interference response policies were able to significantly improve performance of ContikiMAC under interference.
Regardless of the specific implementation, \ac{MAC} protocols are afforded the ability to arbitrate channel access more efficiently with other devices.
How to fully realise this potential remains an open research question.

Secondly, \ac{P-DCCA} was shown to be a feasible implementation of \ac{DCCA} on current \ac{WSN} hardware.
Compared to \ac{T-DCCA}, \ac{P-DCCA} was shown in section \ref{sec:pdcca_accuracy} to achieve better \acl{TP}- and \acl{FP}- accuracy, despite a shorter sampling duration.
This is closer to the standard 802.15.4 \ac{CCA} implementation, simplifying the process of incorporating \ac{DCCA} into existing \ac{MAC} protocols.
The tradeoffs of transmission power and range were quantified; these drawbacks were shown to be outweighed by the benefits of \ac{DCCA} in the application evaluation.

\section{Future work}

Several areas of future work have been identified that extend from the work in this thesis.
These range from more advanced \ac{CTI}-mitigation methods, to
potential uses of the transmit power variation mechanism beyond implementing \ac{DCCA}.

\subsection{Interferer-response policies}

In Chapter \ref{chap:dcca}, the potential benefits of \ac{DCCA} were demonstrated via simple interferer-response policies.
This was sufficient to demonstrate the use case of \ac{DCCA} in practice, and was able to improve packet delivery and energy efficiency of the \ac{WSN} in interference environments.
More comprehensive policies may, however, yield better performance, or target specific optimisations.

For example, the policies described in Section \ref{sec:dcca__app_eval} ignore non-\ac{WSN} devices before transmitting.
While this decreased the rate of \ac{CCA} collisions, and subsequently led to higher packet delivery, packet collisions may increase.
Therefore, a more robust approach could be to implement a 1-persistent \ac{CSMA} policy (similar to 802.11, see figure \ref{fig:interference__wifi-dcf})
This would ensure a collision with 802.11 is avoided, without an expensive retransmission interval.
Similarly, the wakeup process could be further improved by \ac{DCCA} by responding differently to packet corruption.
If a packet is detected originating from the \ac{WSN}, the receiver could extend the listen duration even after a corrupted packet is received. 
This would further improve packet delivery under interference.

It would be beneficial to explore the optimal response policies to various types of interference.
The same conclusion has been reached previously by interference classification studies \cite{hermans2013sonic}.

\subsection{Channel prioritisation via \ac{P-DCCA}}

In this thesis, \ac{P-DCCA} has been used to differentiate \ac{WSN} traffic from other interference.
However, in deployments where \ac{CTI} is not a concern, \ac{P-DCCA} may be used to differentiate traffic within a network.
This could be used to distinguish source, destination, or types of packet, within a \ac{DCCA} check.

For example, one application would be to implement distinct traffic priorities, in order to meet design requirements.
Downstream traffic may include software updates, network configurations, and node instructions.
If this traffic were valued higher than upstream traffic, such as sensor data, it would be advantageous 
for nodes to distinguish the two, and afford the former greater priority in channel contention.
This mechanism would be similar to the 802.11 DCF (see figure \ref{fig:interference__wifi-dcf}), wherein packet priorities are afforded by the \ac{IFS} duration.
Likewise, traffic priorities could be implemented on a single link, between nodes.
For unsuccessful packet transmissions, retransmissions could be afforded higher priority.
This would be beneficial in realtime applications, with strict deadlines for packet delivery.

This area of study is only applicable to the transmission power variation used in \ac{P-DCCA}, and not other \ac{DCCA} implementations.

\subsection{Orthogonal channel communication via Transmit power variation}

Transmit power variation is used in \ac{P-DCCA} in order for packets to be differentiated from other interference.
This simple modulation method could be extended to encapsulate additional information, in order to provide an orthogonal communication channel.
This could be implemented using available amplitude modulation techniques, such as On Off Keying (OOK), and Pulse Width Modulation (PWM).
Alternatively, a more specialised modulation technique could be developed.

This could be used firstly to expand the link bandwidth, without requiring a change to the hardware.
This could serve a specific purpose, such as forward error correction or security information.
Likewise, the receiving mechanism of this channel would not be bound by the same constraints of the 
802.15.4 PHY; namely, it is not necessary to receive and decode the preamble and \ac{SFD}.
This could be leveraged to communicate high priority information, without having to reserve capacity within the 802.15.4 channel.
An example of this would be broadcasting \ac{TDMA} schedules or security keys.
In both cases, a new node would be able to join an existing network by overhearing other network traffic.

This area of study would require research to extend the power variation technique, and also a receive mechanism.
In both cases, there would be a likely tradeoff between communication bandwidth, and error rate.

\subsection{Interference mitigation aware energy estimation}

The techniques presented in Chapter \ref{chap:estimating} to estimate energy consumption could be extended, to also model various \ac{CTI}-mitigation mechanisms.
For example, the idle listening of a \ac{WSN} node could be estimated and compared for various mitigation methods.
These could include \ac{P-DCCA}, \ac{T-DCCA}, and other approaches from literature \cite{sha2013energy}.
This would similarly be an intersection between energy estimation techniques and interference coexistence studies.

\ac{WSN} designers would benefit from being able to compare mitigation approaches before deployment.
Likewise, it would be possible to optimise parameters for each approach without exhaustive testing.
This would be able to account for the respective tradeoffs in each approach to suit the deployment environment.
For the case of \ac{P-DCCA} and \ac{T-DCCA}, evaluated in Chapter \ref{chap:dcca}, it would be trivial to extend these energy estimation techniques.
This would require firstly accounting for the change in \ac{CCA} duration; for example, in \ac{T-DCCA} this would be 2.9$ms$.
Secondly, the \ac{CCA} function would need to account for the respective \acl{FP} accuracy - as measured in Section \ref{sec:pdcca_accuracy}.

\subsection{Reactive \ac{MAC} protocol duty cycling}

In Chapter \ref{chap:estimating} it was shown that the idle listening time of a node can be accurately estimated from interference measurements in an environment.
This is useful to predict sensor network lifetimes before deployment, allowing for proactive measures to meet lifetime goals.

To complement this, reactive measures can be used in response to variable interference conditions.
To achieve this, the channel can be periodically sampled to measure the channel busy probability.
From this, the idle listening can be calculated, either from a closed-form estimation (as in Section \ref{sec:estimating__model}), or from a lookup table.
The wakeup frequency can then be adjusted on demand, to meet energy consumption requirements.
Consequently, \acp{WSN} would be able to ensure lifetime requirements are met, without having to commit to statically configured suboptimal network parameters.

\section{Concluding Remarks}

Advances in wireless communication have delivered innovations to consumer, academic, and research applications; from smartphones equipped with WiFi, to Bluetooth-enabled kettles.
This has led to an abundance of devices communicating in, among others, the 2.4$Ghz$ ISM frequency domain.
For \acp{WSN}, which are dependent on reliable, low power operation, this cost is incurred via packet loss, and energy inefficiency.
How to predict in advance and mitigate the affects of \ac{CTI} are therefore pressing questions amongst \ac{WSN} research.

In this thesis, it has been shown that the idle listening of a \ac{WSN} node can be accurately predicted in a known interference environment.
This enables network designers to optimise design parameters to meet lifetime goals, without extensive testing or development work prior.

Secondly, \ac{DCCA} has been developed as a novel extension to the standard 802.15.4 \ac{CCA}, to cater for more efficient \ac{MAC} protocol responses to interference.
This has been shown to improve link performance, and reduce the energy consumption of affected \acp{WSN}.
\ac{P-DCCA} was presented as one possible implementation of \ac{DCCA}, applicable to current \ac{WSN} hardware.
This work has carved a path for \ac{WSN} \ac{MAC} protocols to avoid the one-size-fits-all approach to collision response, in favour of a source-specific approach.
In a truly heterogeneous interference environment, this is likely to be a fundamental building block of \ac{CTI}-resilient \ac{WSN}.




