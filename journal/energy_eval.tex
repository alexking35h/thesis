% !TEX root = article.tex
\section{Energy Consumption Evaluation}
\label{sec:energy_evaluation}

In this section, the energy consumption of \ac{P-DCCA} is evaluated. In particular, we focus on idle listening energy consumption which is the greatest source of energy consumption in most \ac{WSN} deployments. A node must wake periodically to check for an incoming transmission using a \ac{CCA} mechanism which is the dominant energy cost. We analyse this idle listening cost analytically for \ac{P-DCCA} and compare it with the cost of \ac{T-DCCA} and normal \ac{CCA}.

\subsection{\ac{P-DCCA} Energy Consumption Model}
We assume that any transceiver activity within the \ac{P-DCCA} mechanism has the same energy consumption; energy consumption during transceiver startup and during \ac{RSSI} sampling is the same.
This is a realistic approximation for transceivers such as the CC2420 commonly used. Thus, energy consumption of a \ac{P-DCCA} check depends on the transceiver-on duration $T$, which we model.  

The behaviour of the \ac{P-DCCA} algorithm described depends on the interference encountered, which must be provided as input to this model.
The probability of finding the channel busy at any instant is represented as $p$;
when busy, the channel is then occupied by an interference signal with fixed duration $t$.
For example, when considering a WiFi interferer an intensity of $p$ and an average WiFi packet length of $t$ can be assumed.

The expected duration $T$ of a \ac{P-DCCA} check (shown in Equation~\ref{eq:energy0}) is the sum of
the transceiver start up time, $T_{st}$;
the duration of the first \ac{RSSI} sample, which is always taken;
and the time taken to carry out the sequence of $N_R$ \ac{RSSI} samples as described in Algorithm~\ref{alg:dcca}.
The latter stems from two possible outcomes. 
Firstly, $T_A$ models the case where the \ac{P-DCCA} check starts sooner than $N_R$ \ac{P-DCCA} samples before the end of the interference.
Otherwise, fewer samples are taken ($T_B$).


\begin{equation}
\label{eq:energy0}
T(t,p) = T_{st} + T_{RSSI} + p \cdot \big( T_A(t) + T_B(t) \big)
\end{equation}

$T_A$ is given in Equation~\ref{eq:energy1}, and is the product of the total duration of all \ac{RSSI} samples: $T_{RSSI} \cdot (N_R-1)$,
and the probability.



\begin{equation}
\label{eq:energy1}
T_A(t) = \Big(T_{RSSI} \cdot (N_R-1) \Big) \cdot \Big( \frac{t - ( N_R-1 ) \cdot T_{RSSI}}{t} \Big)
\end{equation}

The remaining interference duration modelled by $T_B$ has duration $t - T_{RSSI} \cdot (N_R - 2)$; this is considered as $(N_R-2)$ discrete intervals.
The probability of each is given by $T_{RSSI} / t$, and the duration of the subsequent $n$ samples is given by $n \cdot T_{RSSI}$.
Therefore, $T_B$ is the sum expected duration of these $(N_R-2)$ intervals.

\begin{equation}
\label{eq:energy2}
T_B = \sum_{n=1}^{N_R-2} n \cdot T_{RSSI} \cdot \Big( \frac{ T_{RSSI} }{ t } \Big)
\end{equation}

This model assumes firstly that $t \ge N \cdot T_{RSSI}$: that the duration of the interference signal is greater than the number of \ac{RSSI} checks in \ac{P-DCCA}.
This is not necessarily true for all types of interference, in which case $T$ represents an upper-bound.
Secondly, the model assumes that $p$ and $t$ are independent variables.

Knowing how much power the transceiver consumes when active and the number of \ac{DCCA} checks necessary (channel check rate) the idle energy consumption using \ac{P-DCCA} can be computed. The channel check rate and the number of \ac{P-DCCA}  checks necessary at each instance depends on the used \ac{MAC} protocol. 

\subsection{\ac{LPL} Energy Consumption}
We now assume TinyOS \ac{LPL} as \ac{WSN} \ac{MAC} protocol to evaluate idle listening energy consumption of  \ac{P-DCCA},  ZiSense as \ac{T-DCCA} and the classical \ac{CCA} without any \ac{DCCA} capability. We use \ac{LPL} as \ac{WSN} \ac{MAC} protocol as ZiSense was particularly designed for this specific protocol. 

TinyOS \ac{LPL} has an inter-packet spacing of $2.9ms$. Thus, for ZiSense a single \ac{CCA} check with a duration of $2.9ms$ is necessary to check for an incoming transmission. With the assumption of a minimum packet duration of at least 500$\mu s$, $6$ \ac{CCA} checks spaced 500$\mu s$ apart, over 3$ms$, are sufficient. This channel check behaviour is necessary for \ac{DCCA} and plain \ac{CCA} to check for incoming transmissions. 

This behaviour can be illustrated using Figure~\ref{fig:lpl_mac_protocols}. For ZiSense the receiver \ac{CCA} shown in Figure~\ref{fig:lpl_mac_protocols} is performed over a long period of  $2.9ms$. For \ac{DCCA} and plain \ac{CCA}  the receiver \ac{CCA} consists of $6$ \ac{CCA} checks spaced 500$\mu s$ apart, over 3$ms$.

ZiSense and plain \ac{CCA} has a constant energy cost per channel check. However, plain \ac{CCA} would not be able to infer the nature of the channel occupier and any observation of interference would lead to a false wake-up, contributing significantly to energy consumption; we do not consider this aspect here and include plain \ac{CCA} only as reference point. \ac{P-DCCA}  has a variable energy cost which depends on the present interference signal. 

\begin{figure}
 \centering
 \includegraphics[width=.75\linewidth]{figs/energy.eps}
 \caption{Energy consumption of \ac{P-DCCA} and \ac{T-DCCA} (based on ZiSense) under interference.
          \ac{P-DCCA} requires less idle listening time under all channel conditions.}
 \label{fig:energy_consumption}
\end{figure}

Based on this, the radio-on time per \ac{DCCA} check for \ac{P-DCCA}, ZiSense and plain \ac{CCA} are shown in Figure~\ref{fig:energy_consumption}, for interference conditions described by $p=0.1,0.25,0.5$ and variable duration $t$.

Plain \ac{CCA} has the same energy cost as \ac{P-DCCA} when interference approaches zero. \ac{P-DCCA} has an energy cost which is much lower than that of ZiSense. Given that  \ac{P-DCCA} was shown in Section~\ref{sec:accuracy_eval} to have at least similar detection accuracy as ZiSense \ac{P-DCCA} will provide better energy efficiency when considering \ac{DCCA} as receiver mechanism. 
