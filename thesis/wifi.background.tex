IEEE 802.11 was first released in 1997, to provide high speed LAN access to wireless devices.
It is typically the 'last hop' in a network topology, enabling network access to electronic devices requiring mobile network access, such as smartphones and laptops.
The brand name for IEEE 802.11, including later amendments and revisions, is $WiFi$, which is used interchangeably in this thesis.
Amendments to the protocol are provided as Wireless technology progresses, increasing data rates and feature set.
In the OSI model, these changes relate to the physical and data link layer, presenting the same interface to higher applications.
Both $WiFi$ and 802.11 are used interchangeably in this thesis to refer to the IEEE 802.11 standard.

An  802.11 network consists of a number of addressable nodes, referred to in the standard as stations (STA).
Each network consists of a \acf{BSS}, as the basic building block, defining an area within which stations can communicate.
An \acf{IBSS} is the simplest form of \ac{BSS}, consisting of two stations which can communicate directly. This is often known as an Ad-hoc network.
By contrast, an Infrastructure \ac{BSS} includes an \acf{AP}, which may provide access to a wider network.
\acp{AP} periodically broadcast their presence to nodes within the \ac{BSS} via beacons. Thus, to join an infrastructure \ac{BSS}, nodes must receive a beacon.

Released in 1999 as the second 802.11 standard, 802.11b was the first to be widely adopted and most current WiFi devices still maintain backwards compatibility.
802.11b channel bandwidth is 22Mhz, and the minimum transmission power is 0$dBm$.
This standard introduced High Rate \ac{DSSS}, which increased the maximum data rate from 2Mbps in the original 1997 standard to 11Mbps.

Subsequently in 2003, 802.11g was released, and incorporated into the  802.11 standard in 2007 under clause 19.
Using the \ac{OFDM} modulation, transmit rates are provided upto 54Mbps.
In deployments where 802.11g networks must coexist with 802.11b devices, backwards compatibility is supported.
Here, data packets are transmitted with an 802.11b-compatible \ac{DSSS} packet header, therefore reducing the maximum data rate.
This revision also incorporated more extensive security features into the standard.

802.11n is the most recent amendment to the 802.11 standard in the 2.4Ghz domain, and is incorporated into the  802.11 standard in 2012, under clause 20.
The same \ac{OFDM} modulation is used as in 802.11g, however by affording channel bandwidths upto 40Mhz, and support for multiple spacial streams simultaneously, 802.11n can support data rates upto 600Mbps.
The larger bandwidth therefore presents a wider interference footprint, which must coexist with other 2.4Ghz devices.

The  802.11 PHY provides an interface to configure the rate of the underlying modulation used to transmit packets.
Rate selection algorithms then vary between device vendors.
In some cases,  802.11 stations may respond to packet loss by reducing the rate, to improve performance under low SNR.
Other instances may respond by increasing the rate, to reduce the on-air time for each packet, and avoid future collisions.
A review of  802.11 rate selection algorithms is beyond the scope of this discussion.

Across all revisions, the MAC protocol remains mostly unchanged. 
802.11 uses the \acf{DCF} to mediate channel contention between multiple 802.11 devices.
Before being able to use the channel, the protocol requires that the channel is idle for a minimum period called \acf{IFS}.
Otherwise, the node must enter the backoff procedure, where the node waits until this condition has been met.
After which, each node picks a random slot to begin transmitting; the node picking the soonest slot number wins and begins transmitting, while the other nodes repeat the procedure.
An Acknowledgement is transmitted after each data frame.
This is shown in figure \ref{fig:interference__wifi-dcf}.

\begin{figure}
 \centering
 \includegraphics[width=.7\textwidth]{figures/background/WiFi-DCF.eps}
 \caption{Simplified model of  802.11 \ac{DCF} function, showing contention between stations 2 and 3, after station 1 has finished using the channel. 1) Both stations wait for the \ac{DIFS} period after station 1 has finished. 2) Both stations randomly select a slot number, station 3 selects a low slot number and begins transmitting. 3) Station 2 waits for the \ac{DIFS} period and the backoff period before being able to transmit}
 \label{fig:interference__wifi-dcf}
\end{figure}

By defining the \ac{IFS}, nodes can be afforded different priorities, shorter \ac{IFS} granting higher priority.
\acf{SIFS} is used for higher priority packets, including acknowledgements, while
\acf{DIFS} is used for all other packets.

The DCF requires a \ac{CCA} method to detect activity on the channel, to determine if another device may be transmitting;
this \ac{CCA} method affects the coexistence with other neighbouring devices.
The standard supports two mechanisms to provide \ac{CCA}: \acf{ED}.
which can detect any signal above a preset threshold, and \acf{CS}, which can only detect signals of the same modulation.
In  802.11b, g, and n, \ac{ED} \ac{CCA} is optional, and is not required in any regulatory domains.
However, it's implementation has become more common in later compliant devices.

\ac{VCS} is provided by the MAC layer using the \ac{NAV}
Here, all packets include a duration field, indicating the duration of the packet.
Upon detecting a packet, a station marks the channel busy until the duration field has expired.
Request-To-Send/Clear-To-Send (RTS/CTS) packets are also defined in the standard; if implemented, the duration field in both records the duration of the entire DATA/ACK exchange.
The use of RTS/CTS is configurable, and implementation dependent, and is recommended where improved \ac{NAV} protection is needed (see hidden node problem).
