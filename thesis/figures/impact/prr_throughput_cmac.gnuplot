set term postscript
set output 'contikimac_throughput_under_wifi.ps'
set xlabel "IEEE 802.11g WiFi interference (KB/s)"
set ylabel "ContikiMAC Goodput (B/s)"
set grid
set xtics 5
unset key
plot 'prr_throughput_cmac.tsv' every ::1 using 1:5 w l lt 1 lw 2

set term postscript
set output 'contikimac_prr_under_wifi.ps'
set xlabel "IEEE 802.11g WiFi interference (KB/s)"
set ylabel "Packets (%)"
set xtics 5
set grid
set key bottom outside center horizontal
plot 'prr_throughput_cmac.tsv' every ::1 using 1:2 title "Received" w l lt 1 lw 2 lc rgb 'blue',\
'' every ::1 using 1:3 title "No Acknowlodgement" w l lt 1 lw 2 lc rgb '#20B2AA',\
'' every ::1 using 1:4 title "Collision" w l lw 2 lt 1 lc rgb 'orange'


