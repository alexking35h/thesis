set terminal postscript #png #pdf dashed enhanced font "Helvetica,10" lw 3

set xlabel "Channel Busy Probability p"

set ylabel "Duty Cycle D(p) in [%]"
set yrange [0:4]
set output "estimation_controlled.eps


#plot "estimation_controlled.txt" using 1:(((16*$3)/10000)/108) title 'CCR 16Hz - Measured' with lines, \
#	"estimation_controlled.txt" using 1:(((16*$4)/10000)/108) title 'CCR 16Hz - Monte Carlo' with lines

plot "estimation_controlled.txt" using 1:($3*100) title 'CCR 8Hz - Measured' with lines, \
	"estimation_controlled.txt" using 1:($4*100) title 'CCR 8Hz - Monte Carlo' with lines



