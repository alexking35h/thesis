In deployments where frequency avoidance is not possible, improving the detection of heterogeneous network technologies can ensure fair channel usage, and mitigate the effects of CTI.
Homogeneous detection is an existing component in wireless protocol design, necessary to reduce collisions in contentious channel conditions.
For example, 802.11 must listen to the channel before transmitting, to reduce the probability of multiple nodes interfering simultaneously.
Such protocols often cater for heterogeneous networks also, however differences in their implementation prevent fair channel access.
Prior work has found this to be the case with 802.15.4 and 802.11 networks: the latter is unable to detect the former, leading to collisions and preventing efficient channel arbitration.

Detection mechanisms also play an important role in LPL-style MAC protocols in WSN. Here, the channel state is used to infer if another node is transmitting, to decide if the radio should 'wake up' or conserve energy by returning to sleep.
However, this mechanism can be unintentionally triggered by interference on the same channel, leading to a \textit{false wakeup}.
This reduces energy efficiency and, in battery-powered WSN, network lifetime.
From this, a logical solution is therefore to bolster these detection mechanisms' sensitivity to heterogeneous devices. If the WiFi MAC protocol, for example, could detect 802.15.4 signals, transmissions would be deferred until the channel becomes free again, avoiding a collisions.

In the following discussion, previous literature is classed based
upon where detection is improved - either the interferer, or the WSN.
Most interferer solutions have almost exclusively focused on 802.11.
This classification does not dictate where any modification takes place. For example, making WSN transmissions more visible to 802.11 is considered an interferer-detection mechanism, even if the modification resides on the WSN.

% Avoiding collisions between ieee 802.11 and ieee 802.15.4 through coexistence aware clear channel assessment.
 % 
 % Analytical evaluation of 802.11 affect on 802.15.4: assumption, not able to detect zigbee, then validated.
 % Arrival of 802.11 packets modelled as an exponential process.
 % Find that:
 %     1) duration of wifi packet has little impact.
 %     2) smaller zigbee packets upto 8x better off.
 %     3) WiFi rate affects zigbee a lot.
 % 
 % Validate model. Results accurate for low wifi
 % Argument: Wifi cannot detect zigbee, even if ED CCA is used.
 % 
 % CACCA is based on a spectral sensing engine: mesaures the spectral power across a wide bandwidth quickly.
 %  - use in CCA, very short.
 % 
 % Then evaluated in model:
 %  - no changes to zb cca, except timing.
 %  - CCA
 % 
 % Findings:
 %  - Compare, based on previous analytical model, zigbee cacca, wifi cacca, and both.
 %  - Find: zigbee-CCA = 24% PER reduction in PL
 %  - WiFi CCA = 75% PER reduction in PL
 %  - Both: 99.6% PL drop.
 %  - Energy efficient.

Tytgat et al study the asymmetry in ZigBee and WiFi CCA mechanisms in \cite{tytgat2012avoiding}.
The effect of WiFi interference on ZigBee is modelled, on the assumption that WiFi CCA cannot detect ZigBee transmissions.
As with carrier sense CCA in WiFi, Tytgat et al also find it to be the case even with energy detection CCA.
This is due to the difference in bandwidths. 
802.15.4, having a bandwidth of 2$Mhz$, has 9.6-$dBm$ higher sensitivity to WiFi, that has a bandwidth of 20$Mhz$, than vice-versa.

Coexistence Aware CCA (CACCA) is then proposed to supplement WiFi and ZigBee CCA with a sensing engine, specifically to detect each others' signals.
The CACCA duration is modelled around 802.11g CCA of 4$\mu s$.
No signal processing is done by the sensing engine, however the authors assume that sufficient accuracy is possible by measuring signal strength alone.
The prior model is then adapted to include ZigBee-only CACCA, WiFi-only CACCA, and both.

Under 100$kb/s$ WiFi interference, the model then predicts that WiFi-only CACCA may reduce ZigBee packet loss by 75\%.
Conversely, ZigBee-only CACCA results in 24\% reduction, while both results in 99.6\%.
WiFi-only CACCA is also shown to decouple the WiFi traffic load from ZigBee packet loss, otherwise correlated in the original model.
Tytgat et al here only model the timing interaction of WiFi and ZigBee, with and without CACCA.
The sensitivity of CACCA is not modelled which prevents analysis of the transmission power asymmetry between WiFi and ZigBee. 
Nonetheless, their work affirms that WiFi is the primary culprit in WiFi-ZigBee CTI.
Following, in the case of 802.11, interferer-side solutions are likely to yield the greatest reduction of 802.15.4 packet loss in joint deployments.
