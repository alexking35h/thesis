%% Tytgat: CACCA

Valck et al present an implementation of WiFi-only CACCA on an \ac{SDR} platform in \cite{de2013coexistence}.
The authors design CACCA to be backward-compatible with the existing 802.11 standard, constraining the CACCA duration to 4$\mu s$.
Two methods are studied to implement CACCA: energy detection (ED), and matched filter (MF).
ED measures the energy on each overlapping 802.15.4 channel, and MF also filters each channel for the O-QPSK modulation.

CACCA is then implemented on a WARP \ac{SDR} platform. To evaluate CACCA, the 802.11 MAC protocol is mimicked in order to simulate coexistence.
For the evaluation, a shielded RF enclosure is used, and the signal attenuation is manually adjusted between the WiFi interferer, and two ZigBee nodes.
Measuring CACCA sensitivity, ED is shown to retain 90\% accuracy for signals above -79$dBm$, while MF reduces this to -83$dBm$.
By contrast, the 802.15.4 standard requires a sensitivity of -85$dBm$, which the authors state cannot be achieved within the timing constraint.

The second experiment includes an 802.11 interferer, with and without CACCA, alongside two ZigBee nodes transmitting at maximum capacity.
Within those sensitivity ranges, CACCA is shown to limit packet loss to less than 10\%, wherein otherwise total packet loss is experienced.
This evaluation compared against an 802.11 benchmark that did not have ED CCA.
More useful would have been to measure against an ED CCA-equipped WiFi interferer, which is supported by the standard.
This approach requires extensive modification to the design of WiFi hardware.
Given the guarded nature and complexity of 802.11 firmware; the number and ubiquity of WiFi networks already deployed, this approach cannot practically alleviate CTI in the immediate future.

% Hou and Tang

By contrast, Hou et al \cite{hou2009minimizing} and Tang et al \cite{wang2011wicop} modify WSN devices:
leveraging the existing detection mechanism in WiFi, improving the visibility of 802.15.4 communications.
In both cases, this is achieved via the sink node, which is responsible for signalling to nearby WiFi devices during ZigBee packet exchanges.
Hou et al build on the 802.11 \ac{VCS} MAC component, which uses RTS/CTS packets to reserve the channel for the duration of the packet exchange.
To utilise this for the protection of 802.15.4 packets, an 802.11 CTS frame is broadcast by the sink, listing a duration sufficient for the following ZigBee exchange.
Upon hearing this CTS frame, WiFi devices will defer until the channel becomes free again after the ZigBee exchange.

Similarly, Wang et al propose WiCop, and study two signalling approaches: Fake-PHY headers, and \ac{DSSS}-nulling.
The former broadcasts 802.11 packet headers, whose length field is sufficient to cover the ZigBee exchange.
This is similar to the \ac{VCS} mechanism described above, but is a more explicit deviation from the 802.11 standard.
\ac{DSSS}-nulling instead continuously transmits 802.11 PHY headers, jamming the channel, throughout the ZigBee packet exchange.
The signal bandwidth is reduced from 22$Mhz$ to 8$Mhz$, in order to provide sufficient space on the channel for ZigBee, while still remaining visible to WiFi interferers.
This approach guards against opportunistic WiFi devices that may ignore Fake-Headers and CTS approaches.

In both papers, these methods are found to be capable of reserving sufficient channel capacity to meet the QoS requirements of the WSN.
Evaluated under WiFi interference, packet loss is reduced to below 3\% and 2\% respectively in \cite{hou2009minimizing} and WiCop \cite{wang2011wicop}.
Both papers envisage a medical sensing application, and assume a centralised topology around the sink node.

%% Cooperative Carrier signaling: Zhang and Shin.

These approaches require atypical hardware solutions, beyond the remit of off-the-shelf WSN.
To avoid this pitfall, Zhang and Shin present a signalling approach based on standard 802.15.4 hardware. 
During the ZigBee packet exchange between two nodes, a third node - designated the signaller - emits a busy tone on an adjacent channel.
The signaller may have a greater transmission power than standard ZigBee nodes, but otherwise conforms to the design of 802.15.4 hardware, and is detectable by WiFi devices.
The busy tone covers the CCA check, packet transmission, and acknowledgement of the ZigBee \ac{CSMA} protocol.
Therefore, the issues of asymmetric interference range and Tx/Rx switch found in \cite{liang2010surviving}, are mitigated.
This approach is termed Cooperative Carrier Signalling (CCS), and is evaluated on the MICAz and USRP \ac{SDR} platforms \cite{zhang2013cooperative}.

In CCS, packet transmissions are preceded by a RTS/CTS exchange which includes the packet length.
Upon overhearing this exchange, the signaller switches to an adjacent channel - still under the same WiFi channel, and emits the busy tone.
Therefore, WiFi nodes are able to detect and avoid ongoing ZigBee communication.
CCS is evaluated in a testbed which includes co-located ZigBee and WiFi networks. On a single link, CCS is shown to reduce ZigBee packet loss by upto 90\%.

CCS is also evaluated for ZigBee in \ac{TDMA} mode, where similar improvements are shown.
This approach does not require any hardware modifications outside either standard, and the authors describe more generally how it may alleviate coexistence between arbitrary networks.
The overhead of coordinating with the signaller does incur a cost in energy consumption of at least 10\% above standard ZigBee.
This is detrimental in WSN applications where energy efficiency is paramount.
Further, Zhang and Shin recommend the signaller be mains powered and capable of higher transmission power - not possible in many WSN applications.

%% Save for summary!
%In both cases, a centralized implementation is adopted based on a single signaler covering the entire network.
%This topology is suitable for the medical applications both envisage, but less so for more typical WSN applications.

